<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Change Email</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns">
                <div class="docs section-container accordion" data-section="" data-options="deep_linking: false" style="">
                    <section class="section active" style="padding-top: 48px;">
                        <p class="title"><a href="#"><? echo $_SESSION['UserName']; ?> Settings</a></p>
                        <div class="content" style="">
                            <ul class="side-nav">
                                <li><a class="" href="/settings/">Summary</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/settings/saved">My Saved Settings</a></li>
                                <li class="divider"></li>
                                <li><a class="active" href="/settings/email">Change Email</a></li>
                                <li><a class="" href="/settings/password">Change Password</a></li>
                                <li><a class="" href="/settings/sessions">View Sessions</a></li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
            <div class="large-9 columns">
                <div class="row">
                    <div class="large-12 columns">
                        <p class="indent">
                            Please use an email address which you have access to, we will send a verification link 
                            to the new address. This link will only be valid for 15 minutes.
                        </p>
                        <hr />
                    </div>
                </div>
                
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>
                <div class="row">
                    <div class="large-8 large-centered columns">
                        <form id="SettingForm" class="custom" method="post" action="/php-bin/settings.php">
                            <fieldset>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Email:</label>
                                        <input tabindex="1" id="Email" name="Email" 
                                               placeholder="Current: <? echo $_SESSION['Email']; ?>" 
                                               type="email" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-4 large-offset-8 columns">
                                        <input id="Command" Name="Command" value="email" type="hidden">
                                        <input id="Submit" tabindex="2" type="submit" 
                                               class="button prefix" value="Change Email">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>    
            </div>
        </div>    
