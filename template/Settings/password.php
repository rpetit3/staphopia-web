<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Change Password</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns">
                <div class="docs section-container accordion" data-section="" data-options="deep_linking: false" style="">
                    <section class="section active" style="padding-top: 48px;">
                        <p class="title"><a href="#"><? echo $_SESSION['UserName']; ?> Settings</a></p>
                        <div class="content" style="">
                            <ul class="side-nav">
                                <li><a class="" href="/settings/">Summary</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/settings/saved">My Saved Settings</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/settings/email">Change Email</a></li>
                                <li><a class="active" href="/settings/password">Change Password</a></li>
                                <li><a class="" href="/settings/sessions">View Sessions</a></li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
            <div class="large-9 columns">
                <div class="row">
                    <div class="large-12 columns">
                        <p class="indent">
                            Remember passwords require atleast one upper case character, one lower case character and 
                            either a number or special character.  It must also have a length of atleast 8 chracters.
                        </p>
                        <hr />
                    </div>
                </div>
                
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>

                <div class="row">
                    <div class="large-8 large-centered columns">
                        <form id="SettingForm" class="custom" method="post" action="/php-bin/settings.php">
                            <fieldset>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Original Password:</label>
                                        <input tabindex="1" id="Original" name="Original" placeholder="********" 
                                               type="password" value="" required 
                                               pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>New Password:</label>
                                        <input tabindex="2" id="Password" name="Password" placeholder="********" 
                                               type="password" value="" required 
                                               pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Verify New Password:</label>
                                        <input tabindex="3" id="Password1" name="Password1" placeholder="********" 
                                               type="password" value="" required 
                                               pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-5 large-offset-7 columns">
                                        <input id="Command" Name="Command" value="password" type="hidden">
                                        <input id="Submit" tabindex="4" type="submit" 
                                               class="button prefix" value="Change Password">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>    
            </div>
        </div>    
