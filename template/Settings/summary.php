<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Account Summary</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns">
                <div class="docs section-container accordion" data-section="" data-options="deep_linking: false" style="">
                    <section class="section active" style="padding-top: 48px;">
                        <p class="title"><a href="#"><? echo $_SESSION['UserName']; ?> Settings</a></p>
                        <div class="content" style="">
                            <ul class="side-nav">
                                <li><a class="active" href="/settings/">Summary</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/settings/saved">My Saved Settings</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/settings/email">Change Email</a></li>
                                <li><a class="" href="/settings/password">Change Password</a></li>
                                <li><a class="" href="/settings/sessions">View Sessions</a></li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
            <div class="large-9 columns">
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>
                <div class="row">
                    <div class="large-12 large-centered columns">
                        
                        <hr />
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 large-centered columns">

                    </div>
                </div>    
            </div>
        </div>    
