<? $sessions = $settings->ViewSessions(); ?>

<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Current Saved Sessions</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns">
                <div class="docs section-container accordion" data-section="" data-options="deep_linking: false" style="">
                    <section class="section active" style="padding-top: 48px;">
                        <p class="title"><a href="#"><? echo $_SESSION['UserName']; ?> Settings</a></p>
                        <div class="content" style="">
                            <ul class="side-nav">
                                <li><a class="" href="/settings/">Summary</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/settings/saved">My Saved Settings</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/settings/email">Change Email</a></li>
                                <li><a class="" href="/settings/password">Change Password</a></li>
                                <li><a class="active" href="/settings/sessions">View Sessions</a></li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
            <div class="large-9 columns">
                <div class="row">
                    <div class="large-12 columns">
                        <p class="indent">
                            This is a current list sessions in which you've used the 'keep me logged in' function.
                            You can clear selected sessions, forcing a log in on the next visit.  This can be helpful if
                            you chose to stay logged in on a public computer and forget to logout, or notice a suspicious session.
                        </p>
                        <hr />
                    </div>
                </div>
                
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>
                <div class="row">
                    <div class="large-12 large-centered columns">
                        <? if (count($sessions) > 0 ) : ?>
                        <form id="SettingForm" class="" method="post" action="/php-bin/settings.php">
                            <div class="row">
                                <div class="large-12 large-centered columns">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th style="width: 5em;"></th>
                                            <th style="width: 20em;">IP Address</th>
                                            <th style="width: 25em;">Location</th>
                                            <th style="width: 25em;">Creation Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <? foreach ($sessions as $session) : ?>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" id="Tokens" name="Tokens[]" 
                                                               value="<? echo $session['SessionToken']; ?>">
                                                    </td>
                                                    <td><? echo $session['IPv4']; ?></td>
                                                    <td><? echo $session['Location']; ?></td>
                                                    <td><? echo $session['CreateDate']; ?></td>
                                                </tr>
                                        <? endforeach; ?>  
                                        </tbody>
                                    </table> 
                                </div>
                            </div>        
                            <div class="row">
                                <div class="large-3 large-offset-9 columns">
                                    <input id="Command" Name="Command" value="sessions" type="hidden">
                                    <input id="Submit" type="submit" 
                                           class="button prefix" value="Delete Session">
                                </div>
                            </div> 
                        </form>
                        
                        <? else : ?>
                        <div class="panel callout">
                            <p class="center">
                                You do not have any stored sessions.
                            </p>
                        </div>
                        <? endif; ?>
                    </div>
                </div>    
            </div>
        </div>    
