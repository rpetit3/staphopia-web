<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center">Staphopia Groups</h4>
                <p class="indent">
                    The group feature allows you to selectively share access of your data to other users.  You create
                    a group and begin to invite fellow users.  Users can be invited as administrators, curators or just 
                    members.  At the moment groups will be by invite only, and not viewable by the public.
                </p>
                <hr class="shift-up"/>
            </div>
        </div>
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-10 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>
        
        <div class="row">
                <div class="large-10 large-centered columns">
                <p></p>
                <div class="row">
                    <div class="large-2 large columns">
                        &nbsp;
                    </div>
                    <div class="large-8 large columns">
                        <h6 class="center">Your Groups</h6>
                    </div>
                    <div class="large-2 large columns right">
                        <a href="/groups/create/" title="Create a new Group.">
                            <i class="foundicon-plus"></i>
                        </a>
                    </div>
                </div>
                <p></p>
                <hr class="shift-up"/>
            </div>
        </div>
        

        
        <div class="row">
            <div class="large-10 large-centered columns">
                
                <? if ($groups->HasGroups) : ?>
                
                    <? foreach ($groups->groups as $group) : ?>
                    
                        <div class="row">
                            <div class="large-8 columns">
                                <a href="/groups/view/<? echo $group['GroupID'] ?>" title="View group info">
                                    <? echo $group['Name']; ?>
                                </a>
                            </div>
                            <div class="large-4 columns right">
                                <? echo $permission[$group['Permission']]; ?>
                            </div>
                        </div>
                       <div class="row">
                            <div class="large-10 large-centered columns italic">
                                <p><? echo $group['Title']; ?></p>
                            </div>
                        </div>
                        <hr />
                        
                    <? endforeach; ?>

                <? else : ?>
                    <br />
                    Hmmm... not in a group? <a href="/groups/create/" title="Create a new group.">Start your own!</a>
                    <br />
                <? endif; ?>
                
            </div>
        </div>   