<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center">Remove a group.</h4>
                <p class="indent">
                    There is no turning back, if you delete a group it is gone.  Other members will no longer be able 
                    to access the group.  All shared connections will be lost.  Your data will be untouched, but members 
                    will no longer have access to it.
                </p>
                <hr class="shift-up"/>
            </div>
        </div>        
        <div class="row">
            <div class="large-8 large-centered columns">
                <form id="GroupForm" class="custom"  method="post" action="/php-bin/groups.php">
                    <fieldset>
                        <div class="row">
                            <div class="large-12 columns center">
                                <label>You are about to remove the group "<? echo $groups->GroupName( $_GET['f'] ); ?>"</label>
                                <br />
                                <br />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns center">
                                <label>Last chance... No turning back...</label>
                                <br />
                                <br />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 large-centered columns">
                                <input id="GroupID" Name="GroupID" value="<? echo $_GET['f']; ?>" type="hidden">
                                <input id="Command" Name="Command" value="destroy" type="hidden">
                                <input id="Submit" tabindex="4" type="submit" 
                                       class="button prefix" value="Remove Group">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>