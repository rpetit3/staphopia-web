<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center">Invite New Member To Group</h4>
                <p class="indent">
                    Use this form to invite new members to one of your groups. Only group owners and group 
                    administrators can invite new members.
                </p>
                <hr class="shift-up"/>
            </div>
        </div>
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-10 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>

        <? if ($groups->HasGroups && $groups->CanInvite) : ?>
        
        <div class="row">
            <div class="large-8 large-centered columns">
                <form id="GroupForm" class="custom"  method="post" action="/php-bin/groups.php">
                    <fieldset>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Group Name:</label>
                                
                                <select name="GroupID" tabindex="22">
                                    <option value="">Select A Group</option>
                                    <? foreach($groups->groups as $group) : ?>
                                        <? if ($group['Permission'] >= 2) : ?>
                                            <? if ($group['GroupID'] == $_GET['f']) : ?>
                                                <option value="<? echo $group['GroupID']; ?>" selected="selected"><? echo $group['Name']; ?></option>
                                            <? else : ?>
                                                <option value="<? echo $group['GroupID']; ?>"><? echo $group['Name']; ?></option>
                                            <? endif;?>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>First Name:</label>
                                <input tabindex="1" id="FirstName" name="FirstName" placeholder="John" 
                                       type="text" value="" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Last Name:</label>
                                <input tabindex="2" id="LastName" name="LastName" placeholder="Doe" 
                                       type="text" value="" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Email:</label>
                                <input tabindex="3" id="Email" name="Email" placeholder="jdoe@lost.com" 
                                       type="email" value="" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 large-offset-9 columns">
                                <input id="Command" Name="Command" value="invite" type="hidden">
                                <input id="Submit" tabindex="4" type="submit" 
                                       class="button prefix" value="Invite">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        
        <? else : ?>
        <div class="row">
            <div class="large-8 large-centered columns center">
                <br />
                You do not have any groups for which you can invite new members.
                <br />
            </div>
        </div>
        <? endif; ?>