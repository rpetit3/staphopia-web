<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center">Modify user's permissions</h4>
                <p class="indent">
                    Use this form to change the permission level of a user.  There are four levels, Member, Curator, 
                    Administrator, and Owner.  Members can view all data shared by the group.  Curators can edit the 
                    meta data of shared data.  Administrators can edit meta data as well as invite new users, remove 
                    users, etc... Finally, Owners have full control of the group. You can only modify permissions of 
                    a user if their permission level is lower then yourself.
                </p>
                <hr class="shift-up"/>
            </div>
        </div>
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-10 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>

        <? if ($groups->HasRemovables) : ?>
        
        <div class="row">
            <div class="large-8 large-centered columns">
                <form id="GroupForm" class="custom"  method="post" action="/php-bin/groups.php">
                    <fieldset>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Select user from <? echo $groups->GroupName( $_GET['f'] ); ?>:</label>
                                
                                <select name="UserID" tabindex="22">
                                    <option value="" selected="selected" >Select A User</option>
                                    <? foreach($groups->Info['Members'] as $member) : ?>
                                        <? if ($member['Permission'] < $groups->UserGroup( $_SESSION['UserID'], $_GET['f'] )) : ?>
                                            <option value="<? echo $member['UserID']; ?>">
                                                <? echo $member['FirstName'] .' '. $member['LastName'] .' ('. $permission[ $member['Permission'] ] .')'; ?>
                                            </option>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Select permission level:</label>
                                
                                <select name="Permission" tabindex="22">
                                    <option value="" selected="selected" >Select A Permission Level</option>
                                    <option value="0">Member</option>
                                    <option value="1">Curator</option>
                                    <option value="2">Administrator</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 large-offset-9 columns">
                                <input id="GroupID" Name="GroupID" value="<? echo $_GET['f']; ?>" type="hidden">
                                <input id="Command" Name="Command" value="permission" type="hidden">
                                <input id="Submit" tabindex="4" type="submit" 
                                       class="button prefix" value="Update User">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        
        <? else : ?>
        <div class="row">
            <div class="large-8 large-centered columns center">
                <br />
                There are not any users that can have permissions altered from the group "<? echo $groups->GroupName( $_GET['f'] ); ?>".
                <br />
            </div>
        </div>
        <? endif; ?>