<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center">Leave a Group</h4>
                <p class="indent">
                    Use this form to leave a group.  Once you leave a group, you will no longer have acess to 
                    private data shared within the group.  You cannot leave a group which you created.
                </p>
                <hr class="shift-up"/>
            </div>
        </div>
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-10 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>

        <? if ($groups->HasGroups) : ?>
        
        <div class="row">
            <div class="large-8 large-centered columns">
                <form id="GroupForm" class="custom"  method="post" action="/php-bin/groups.php">
                    <fieldset>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Group Name:</label>
                                
                                <select name="GroupID" tabindex="22">
                                    <option value="" >Select A Group</option>
                                    <? foreach($groups->groups as $group) : ?>
                                        <? if ($group['Permission'] <= 3) : ?>
                                        
                                            <? if ($group['GroupID'] == $_GET['f']) : ?>
                                                <option selected="selected" value="<? echo $group['GroupID']; ?>"><? echo $group['Name']; ?></option>
                                            <? else : ?>
                                                <option value="<? echo $group['GroupID']; ?>"><? echo $group['Name']; ?></option>
                                            <? endif; ?>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 large-offset-9 columns">
                                <input id="UserID" Name="UserID" value="<? echo $_SESSION['UserID']; ?>" type="hidden">
                                <input id="Command" Name="Command" value="leave" type="hidden">
                                <input id="Submit" tabindex="4" type="submit" 
                                       class="button prefix" value="Leave Group">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        
        <? else : ?>
        <div class="row">
            <div class="large-8 large-centered columns center">
                <br />
                You do not have any groups to leave.
                <br />
            </div>
        </div>
        <? endif; ?>