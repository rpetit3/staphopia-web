<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center"><? echo $groups->Info['Name']; ?></h4>
                <p class="indent"><? echo $groups->Info['Title']; ?></p>
                <hr class="shift-up"/>
            </div>
        </div>
        
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-10 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>

        <div class="row">
            <div class="large-10 large-centered columns right">
                <? if ($groups->CanInvite) : ?>
                    <a href="/groups/invite/<? echo $_GET['f']; ?>" title="Invite a new member to the group.">
                        <i class="foundicon-mail"></i>
                    </a>
                    <a href="/groups/permission/<? echo $_GET['f']; ?>" title="Change member permissions.">
                        <i class="foundicon-tools"></i>
                    </a>
                    <a href="/groups/remove/<? echo $_GET['f']; ?>" title="Remove a member from the group.">
                        <i class="foundicon-remove"></i>
                    </a>
                    
                    <? if ($groups->IsOwner) : ?>
                    
                    <a href="/groups/destroy/<? echo $_GET['f']; ?>" title="Delete the group.">
                        <i class="foundicon-trash"></i>
                    </a>
                    
                    <? else : ?>
                    
                    <a href="/groups/leave/<? echo $_GET['f']; ?>" title="Leave the group.">
                        <i class="foundicon-trash"></i>
                    </a>
                    
                    <? endif; ?>
                    
                <? else : ?>
                
                    <a href="/groups/leave/<? echo $_GET['f']; ?>" title="Leave the group.">
                        <i class="foundicon-trash"></i>
                    </a>
                    
                <? endif; ?>
            </div>
        </div>
        
        <div class="row">
            <div class="large-10 large-centered columns">
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Group Member</th>
                            <th>Status</th>
                            <th>Genome Count</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <? foreach ($groups->Info['Members'] as $member) : ?>
                            <tr>
                                <td><? echo $member['FirstName'] .' '. $member['LastName']; ?></td>
                                <td><? echo $permission[ $member['Permission'] ]; ?></td>
                                <td><? echo $groups->GenomeCount( $member['UserID'] ); ?></td>
                                <td>
                                    <a href="/genomes/<? echo $member['UserName']; ?>" titel="View genomes">
                                        View Genomes
                                    </a>
                                </td>
                            </tr>
                        <? endforeach;?>.
                    </tbody>
                </table>
            </div>
        </div>
