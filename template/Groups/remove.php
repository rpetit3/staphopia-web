<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center">Remove user from a Group</h4>
                <p class="indent">
                    Use this form to remove a user from the group.  You can only users with lower privileges than
                    yourself.
                </p>
                <hr class="shift-up"/>
            </div>
        </div>
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-10 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>

        <? if ($groups->HasRemovables) : ?>
        
        <div class="row">
            <div class="large-8 large-centered columns">
                <form id="GroupForm" class="custom"  method="post" action="/php-bin/groups.php">
                    <fieldset>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Select user to remove from <? echo $groups->GroupName( $_GET['f'] ); ?>:</label>
                                
                                <select name="UserID" tabindex="22">
                                    <option value="" selected="selected" >Select A User</option>
                                    <? foreach($groups->Info['Members'] as $member) : ?>
                                        <? if ($member['Permission'] < $groups->UserGroup( $_SESSION['UserID'], $_GET['f'] )) : ?>
                                            <option value="<? echo $member['UserID']; ?>">
                                                <? echo $member['FirstName'] .' '. $member['LastName'] .' ('. $permission[ $member['Permission'] ] .')'; ?>
                                            </option>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 large-offset-9 columns">
                                <input id="GroupID" Name="GroupID" value="<? echo $_GET['f']; ?>" type="hidden">
                                <input id="Command" Name="Command" value="remove" type="hidden">
                                <input id="Submit" tabindex="4" type="submit" 
                                       class="button prefix" value="Remove User">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        
        <? else : ?>
        <div class="row">
            <div class="large-8 large-centered columns center">
                <br />
                There are not any users that can be removed from the group "<? echo $groups->GroupName( $_GET['f'] ); ?>".
                <br />
            </div>
        </div>
        <? endif; ?>