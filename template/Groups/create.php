<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center">Create A New Group</h4>
                <p class="indent">
                    Create a new group allow other members to view your data.  This group will only be veiwable to 
                    users you invite.
                </p>
                <hr class="shift-up"/>
            </div>
        </div>
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-10 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>
        
        <div class="row">
            <div class="large-6 large-centered columns">
                <form id="GroupForm" class="" method="post" action="/php-bin/groups.php">
                    <fieldset>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Group Name:</label>
                                <input tabindex="1" id="GroupName" name="GroupName" placeholder="Tricia McMillan's Lab" 
                                       type="text" value="" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Group Message:</label>
                                <input tabindex="2" id="GroupMessage" name="GroupMessage" placeholder="All data sequenced in Dr. McMillan's lab." 
                                       type="text" value="" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-4 large-offset-8 columns">
                                <input id="Command" Name="Command" value="create" type="hidden">
                                <input id="Submit" tabindex="4" type="submit" 
                                       class="button prefix" value="Create Group">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>   