<nav class="top-bar">
            <ul class="title-area">
                <!-- Title Area -->
                <li class="name">
                    <h1><a href="/">Staphopia&trade;<sub>Beta</sub></a></h1>
                </li>
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>

            <section class="top-bar-section">
                <!-- Left Nav Section -->
                <ul class="left">
                    <li class="divider"></li>
                    <li><a href="/">Home</a></li>
                    <li class="divider"></li>
                    <? if (!$_SESSION['IsLive']): ?>
                    <li><a href="/submit/">Submit Genome</a></li>
                    <li class="divider"></li>
                    <li><a href="/top10/">Top 10</a></li>
                    <li class="divider"></li>
                    <? endif; ?>
                    <li><a href="/genomes/">Genomes</a></li>
                    <li class="divider"></li>
                    <? if (!$_SESSION['IsLive']): ?>
                    <li><a href="/groups/">Groups</a></li>
                    <li class="divider"></li>
                    <? endif; ?>
                    <li><a href="/contact/">Contact</a></li>
                    <li class="divider"></li>    
                    <li><a href="/issue/">Report Issue</a></li>
                    <li class="divider"></li>                           
                </ul>

                <!-- Right Nav Section -->
                <ul class="right">
                    <li class="divider"></li>
                    <li class="has-dropdown"><a href="#"><? echo $_SESSION['UserName']; ?></a>
                        <ul class="dropdown">
                            <li><a href="/genomes/<? echo $_SESSION['UserName']; ?>">My Genomes</a></li>
                            <? if (!$_SESSION['IsLive']): ?>
                            <li><a href="/groups/summary">My Groups</a></li>
                            <? endif; ?>
                            <li><a href="/settings/">Acount Settings</a></li>
                            <? if ($_SESSION['IsAdmin']) : ?>
                            <li><a href="/admin/">Admin Tools</a></li>
                            <? endif; ?>
                            <li><a href="/php-bin/logout/">Log Out</a></li>
                        </ul>
                    </li>
                    <li class="divider"></li>
                </ul>
            </section>
        </nav>

