<?php 
    $_SESSION['IsLive'] = True;
    if (isset($_SESSION['IsLockedOut'])) {
        include ('/var/www/staphopia/template/top-nav-locked.php');
    } else if (isset($_SESSION['IsLoggedIn'])) {
        include ('/var/www/staphopia/template/top-nav-private.php');
    } else if ($_SESSION['IsLive']) {
        include ('/var/www/staphopia/template/top-nav-live.php');
    } else {
        include ('/var/www/staphopia/template/top-nav-public.php');
    }

?>
