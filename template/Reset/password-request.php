<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Password Reset Request</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-9 large-centered columns">
                <div class="row">
                    <div class="large-12 columns">
                        <p class="indent">
                            Please use an email address which you have access to, we will send a password reset 
                             request link the address. This link will only be valid for 15 minutes.
                        </p>
                        <hr />
                    </div>
                </div>
                
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>
                <div class="row">
                    <div class="large-8 large-centered columns">
                        <form id="ResetForm" class="" method="post" action="/php-bin/reset.php">
                            <fieldset>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Email:</label>
                                        <input tabindex="1" id="Email" name="Email" 
                                               placeholder="myemail@mydomain.com" 
                                               type="email" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-4 large-offset-8 columns">
                                        <input id="Type" Name="Type" value="request" type="hidden">
                                        <input id="Command" Name="Command" value="password" type="hidden">
                                        <input id="Submit" tabindex="2" type="submit" 
                                               class="button prefix" value="Request Reset">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>    
            </div>
        </div>    
