<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Reset Password</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-9 large-centered columns">
                <div class="row">
                    <div class="large-12 columns">
                        <p class="indent">
                            Remember passwords require atleast one upper case character, one lower case character and 
                            either a number or special character.  It must also have a length of atleast 8 chracters.
                        </p>
                        <hr />
                    </div>
                </div>
                
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>

                <div class="row">
                    <div class="large-8 large-centered columns">
                        <form id="ResetForm" class="" method="post" action="/php-bin/reset.php">
                            <fieldset>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Password:</label>
                                        <input tabindex="2" id="Password" name="Password" placeholder="********" 
                                               type="password" value="" required 
                                               pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Verify Password:</label>
                                        <input tabindex="3" id="Password1" name="Password1" placeholder="********" 
                                               type="password" value="" required 
                                               pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-5 large-offset-7 columns">
                                        <input id="CodeID" Name="CodeID" value="<? echo $_GET['f']; ?>" type="hidden">
                                        <input id="Type" Name="Type" value="reset" type="hidden">
                                        <input id="Command" Name="Command" value="password" type="hidden">
                                        <input id="Submit" tabindex="4" type="submit" 
                                               class="button prefix" value="Reset Password">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>    
            </div>
        </div>    
