<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Delete User Account</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns">
                <div class="docs section-container accordion" data-section="" data-options="deep_linking: false" style="">
                    <section class="section active" style="padding-top: 48px;">
                        <p class="title"><a href="#">Admin Tools</a></p>
                        <div class="content" style="">
                            <ul class="side-nav">
                                <li><a class="" href="/admin/">Summary</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/admin/invite">Invite User</a></li>
                                <li><a class="" href="/admin/lock">Lock User</a></li>
                                <li><a class="" href="/admin/unlock">Unlock User</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/admin/grant">Grant Admin Status</a></li>
                                <? if ($_SESSION['UserName'] == 'rpetit') : ?>
                                <li><a class="" href="/admin/revoke">Revoke Admin Status</a></li>
                                <li class="divider"></li>
                                <li><a class="active" href="/admin/delete">Delete User</a></li>
                                <? endif; ?>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
            <div class="large-9 columns">
                <div class="row">
                    <div class="large-12 columns">
                        <p class="center" style="font-weight: bold;">***WARNING*** ***WARNING*** ***WARNING***</p>
                        <p class="indent">
                            There is no turning back when deleting a user's account.  Everything associated
                            with the user will be lost!  This includes all runs and associated data (MLST, SCCmec etc...).
                            Think long and hard before deleting a user account! 
                        </p>
                        <p class="center" style="font-weight: bold;">***WARNING*** ***WARNING*** ***WARNING***</p>
                        <hr />
                    </div>
                </div>
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <p class="center">Fields must be exact matches as the database, to prevent accidental deletion.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="large-8 large-centered columns">
                        <form id="AdminForm" class="" method="post" action="/php-bin/admin.php">
                            <fieldset>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>User Name:</label>
                                        <input tabindex="1" id="UserName" name="UserName" placeholder="adent" 
                                               type="text" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>First Name:</label>
                                        <input tabindex="2" id="FirstName" name="FirstName" placeholder="John" 
                                               type="text" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Last Name:</label>
                                        <input tabindex="3" id="LastName" name="LastName" placeholder="Doe" 
                                               type="text" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Email:</label>
                                        <input tabindex="4" id="Email" name="Email" placeholder="jdoe@lost.com" 
                                               type="email" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>User ID:</label>
                                        <input tabindex="5" id="UserID" name="UserID" placeholder="123456" 
                                               type="text" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-4 large-offset-8 columns">
                                        <input id="Command" Name="Command" value="delete" type="hidden">
                                        <input id="Submit" tabindex="6" type="submit" 
                                               class="button prefix" value="Delete User">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
