<? $locked = count($admin->LockedUsers()); ?>
<? $normal = count($admin->NormalUsers()); ?>
<? $users = $admin->AllAdmins(); ?>
<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Admin Summary</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns">
                <div class="docs section-container accordion" data-section="" data-options="deep_linking: false" style="">
                    <section class="section active" style="padding-top: 48px;">
                        <p class="title"><a href="#">Admin Tools</a></p>
                        <div class="content" style="">
                            <ul class="side-nav">
                                <li><a class="active" href="/admin/">Summary</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/admin/invite">Invite User</a></li>
                                <li><a class="" href="/admin/lock">Lock User</a></li>
                                <li><a class="" href="/admin/unlock">Unlock User</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/admin/grant">Grant Admin Status</a></li>
                                <? if ($_SESSION['UserName'] == 'rpetit') : ?>
                                <li><a class="" href="/admin/revoke">Revoke Admin Status</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/admin/delete">Delete User</a></li>
                                <? endif; ?>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
            <div class="large-9 columns">
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>
                <div class="row">
                    <div class="large-12 large-centered columns">
                        Server stats here?
                        <hr />
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 large-centered columns">
                        <? if (count($users) > 0 ) : ?>
                        <h5 class="center">Current Admins</h5>
                        <table>
                            <thead>
                            <tr>
                                <th style="width: 13em;">User Name</th>
                                <th style="width: 13em;">First Name</th>
                                <th style="width: 13em;">Last Name</th>
                                <th style="width: 20em;">Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            <? foreach ($users as $user) : ?>
                                    <tr>
                                        <td><? echo $user['UserName']; ?></td>
                                        <td><? echo $user['FirstName']; ?></td>
                                        <td><? echo $user['LastName']; ?></td>
                                        <td><? echo $user['Email']; ?></td>
                                    </tr>
                            <? endforeach; ?>  
                            </tbody>
                        </table>
                        <? else : ?>
                        <div class="panel callout">
                            <p class="center">
                                There was a problem getting admins!
                            </p>
                        </div>
                        <? endif; ?>
                    </div>
                </div>    
            </div>
        </div>    
