<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Invite New User</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns">
                <div class="docs section-container accordion" data-section="" data-options="deep_linking: false" style="">
                    <section class="section active" style="padding-top: 48px;">
                        <p class="title"><a href="#">Admin Tools</a></p>
                        <div class="content" style="">
                            <ul class="side-nav">
                                <li><a class="" href="/admin/">Summary</a></li>
                                <li class="divider"></li>
                                <li><a class="active" href="/admin/invite">Invite User</a></li>
                                <li><a class="" href="/admin/lock">Lock User</a></li>
                                <li><a class="" href="/admin/unlock">Unlock User</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/admin/grant">Grant Admin Status</a></li>
                                <? if ($_SESSION['UserName'] == 'rpetit') : ?>
                                <li><a class="" href="/admin/revoke">Revoke Admin Status</a></li>
                                <li class="divider"></li>
                                <li><a class="" href="/admin/delete">Delete User</a></li>
                                <? endif; ?>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
            <div class="large-9 columns">
                <div class="row">
                    <div class="large-12 columns">
                        <p class="center">Invite new user to become a Beta-Tester.</p>
                        <hr />
                    </div>
                </div>
                <? if (isset($_SESSION['ErrorMessage'])) : ?>
                
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="panel callout">
                            <p class="center">
                            <? 
                                echo $_SESSION['ErrorMessage']; 
                                unset($_SESSION['ErrorMessage']);
                            ?>
                            
                            </p>
                        </div>
                    </div>
                </div>
                
                <? endif; ?>
                <div class="row">
                    <div class="large-8 large-centered columns">
                        <form id="AdminForm" class="" method="post" action="/php-bin/admin.php">
                            <fieldset>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>First Name:</label>
                                        <input tabindex="1" id="FirstName" name="FirstName" placeholder="John" 
                                               type="text" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Last Name:</label>
                                        <input tabindex="2" id="LastName" name="LastName" placeholder="Doe" 
                                               type="text" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <label>Email:</label>
                                        <input tabindex="3" id="Email" name="Email" placeholder="jdoe@lost.com" 
                                               type="email" value="" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-3 large-offset-9 columns">
                                        <input id="Command" Name="Command" value="invite" type="hidden">
                                        <input id="Submit" tabindex="4" type="submit" 
                                               class="button prefix" value="Invite">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
