<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center"><? echo $status->title;  ?></h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="large-10 large-centered columns center">
            <? if (!$status->begun) : ?>
                Your job is still in queue waiting to begin.
            <? else : ?>
                <? if (!$status->valid) : ?>
                    The input FASTQ did not pass our validation step.  Please verify your FASTQ is 
                    in proper format.
                
                <? else : ?> 
                    <? if ($status->complete) : ?>
                        We have finished processing your Sample. View it here: <? echo $status->link; ?>
                    
                    <? else : ?> 
                        Your Sample is being processed, it should be done shortly.
                        
                    <? endif; ?>
                <? endif; ?>
            <? endif; ?>
            <hr />
            </div>
        </div>
        <div class="row">
            <div class="large-5 large-centered columns">
                <ul id="status" style="list-style-type: none;">
                    <li><i class="<? echo $status->state['EC2']; ?>"></i>Launching Amazon EC2 Instance</li>
                    <li><i class="<? echo $status->state['Valid']; ?>"></i>Validate Input FASTQ</li>
                    <li><i class="<? echo $status->state['Filter']; ?>"></i>Filter Low Quality Reads</li>
                    <li><i class="<? echo $status->state['Assembly']; ?>"></i>Assemble FASTQ</li>
                    <li><i class="<? echo $status->state['MLST']; ?>"></i>Determine Sequence Type (MLST) of Sample</li>
                    <li><i class="<? echo $status->state['SCCmec']; ?>"></i>Determine SCCmec Type of Sample</li>
                    <li>
                        <i class="<? echo $status->state['Resistance']; ?>"></i>Predict Resistance Phenotype of Sample
                    </li>
                    <li>
                        <i class="<? echo $status->state['Virulence']; ?>"></i>Predict Virulence Factors of Sample
                    </li>
                </ul>
            </div>
        </div>
