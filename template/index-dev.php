        <div class="row">
            <div class="small-12 large-8 large-centered columns">
                <? if (isset($_SESSION['IsLoggedIn'])) : ?>
                    <p></p>
                    <p>Hello Beta-Testers! Here's a list of some things we would like for you to test out.</p>
                    <p></p>
                    <p class="indent">- Try the 'keep me logged in' function.</p>
                    <p class="indent">- Try many unsucceful login atempts and see what happens!</p>
                    <p class="indent">- Try logging into your account unsucessfully.</p>
                    <p></p>
                    <p>If you run into any problems, please use the 
                        <a href="/issue/" title="Report an issue">Report Issue</a> 
                        feature. Thank you very much for your help!</p>
                <? else: ?>
                <p></p>
                <p class="indent">
                    You have reached the development version of Staphopia.  This is site is not currently open to the  
                    public.  Please click the link below to be redirected to the public version of Staphopia.
                </p>
                <p>
                    <h3 class="center">
                        <a href="http://www.staphopia.com" title="View the public version of Staphopia">Staphopia</a>
                    </h3>
                </p> 
                <? endif; ?>
            </div>
        </div>