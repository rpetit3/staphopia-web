            <div class="mlst-outline info">
                <div class="row">
                    <div class="small-8 small-centered columns">
                        <div class="row">
                            <div class="small-5 columns">
                                <div class="panel no-border summary-box">
                                    <h5 class="italic">MLST:</h5>
                                    <h4 class="centered"><? echo ($mlst->stats[0]['SequenceType'] == 0 ? '-' : $mlst->stats[0]['SequenceType']); ?></h4>
                                </div>
                            </div>
                            <div class="small-5 columns">
                                <div class="panel no-border summary-box">
                                    <h5 class="italic">Clonal Complex:</h5>
                                    <h4 class="centered"><? echo ($mlst->stats[0]['ClonalComplex'] == 0 ? '-' : $mlst->stats[0]['ClonalComplex']); ?></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="small-9 small-centered columns">
                        <hr />
                        <div class="panel no-border">
                            <h5 class="center">Locus Hits</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="small-9 small-centered columns">
                        <table id="lociTable">
                            <thead>
                                <tr>
                                    <td>Locus</td>
                                    <td>Allele</td>
                                    <td>Assembly Score</td>
                                    <td>Mapping Score</td>
                                    <td>Calling Method</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>arcC</td>
                                    <td><? echo ($mlst->stats[0]['arcc_id'] == 0 ? '-' : $mlst->stats[0]['arcc_id'] ); ?></td>
                                    <td><? echo $mlst->stats[0]['arcc_aScore']; ?></td>
                                    <td><? echo $mlst->stats[0]['arcc_mScore']; ?></td>
                                    <td><? echo $mlst->method[$mlst->stats[0]['arcc_status']]; ?></td>
                                </tr>
                                <tr>
                                    <td>aroE</td>
                                    <td><? echo ($mlst->stats[0]['aroe_id'] == 0 ? '-' : $mlst->stats[0]['aroe_id'] ); ?></td>
                                    <td><? echo $mlst->stats[0]['aroe_aScore']; ?></td>
                                    <td><? echo $mlst->stats[0]['aroe_mScore']; ?></td>
                                    <td><? echo $mlst->method[$mlst->stats[0]['aroe_status']]; ?></td>
                                </tr>
                                <tr>
                                    <td>glpF</td>
                                    <td><? echo ($mlst->stats[0]['glpf_id'] == 0 ? '-' : $mlst->stats[0]['glpf_id'] ); ?></td>
                                    <td><? echo $mlst->stats[0]['glpf_aScore']; ?></td>
                                    <td><? echo $mlst->stats[0]['glpf_mScore']; ?></td>
                                    <td><? echo $mlst->method[$mlst->stats[0]['glpf_status']]; ?></td>
                                </tr>
                                <tr>
                                    <td>gmk_</td>
                                    <td><? echo ($mlst->stats[0]['gmk__id'] == 0 ? '-' : $mlst->stats[0]['gmk__id'] ); ?></td>
                                    <td><? echo $mlst->stats[0]['gmk__aScore']; ?></td>
                                    <td><? echo $mlst->stats[0]['gmk__mScore']; ?></td>
                                    <td><? echo $mlst->method[$mlst->stats[0]['gmk__status']]; ?></td>
                                </tr>
                                <tr>
                                    <td>pta_</td>
                                    <td><? echo ($mlst->stats[0]['pta__id'] == 0 ? '-' : $mlst->stats[0]['pta__id'] ); ?></td>
                                    <td><? echo $mlst->stats[0]['pta__aScore']; ?></td>
                                    <td><? echo $mlst->stats[0]['pta__mScore']; ?></td>
                                    <td><? echo $mlst->method[$mlst->stats[0]['pta__status']]; ?></td>
                                </tr>
                                <tr>
                                    <td>tpi_</td>
                                    <td><? echo ($mlst->stats[0]['tpi__id'] == 0 ? '-' : $mlst->stats[0]['tpi__id'] ); ?></td>
                                    <td><? echo $mlst->stats[0]['tpi__aScore']; ?></td>
                                    <td><? echo $mlst->stats[0]['tpi__mScore']; ?></td>
                                    <td><? echo $mlst->method[$mlst->stats[0]['tpi__status']]; ?></td>
                                </tr>
                                <tr>
                                    <td>yqiL</td>
                                    <td><? echo ($mlst->stats[0]['yqil_id'] == 0 ? '-' : $mlst->stats[0]['yqil_id'] ); ?></td>
                                    <td><? echo $mlst->stats[0]['yqil_aScore']; ?></td>
                                    <td><? echo $mlst->stats[0]['yqil_mScore']; ?></td>
                                    <td><? echo $mlst->method[$mlst->stats[0]['yqil_status']]; ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="font-size: 10px;font-style: italic;text-align: right;">
                            *An assembly score of 0, and a small mapping score indicate high confidence.
                        </p>
                    </div>
                </div>
            </div>
