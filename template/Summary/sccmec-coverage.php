                <div class="sccmec-coverage info">
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <div class="panel no-border">
                                <? echo $sccmec->images; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <div class="panel no-border">
                                <p></p>
                                <h5 class="center">Select A SCCmec Type To View</h5>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <ul class="large-block-grid-3">
                                <li style="min-width: 20em;">
                                    <ul class="side-nav sccmecCov">
                                        <li>
                                            <ul class="inline-list">
                                                <li></li>
                                                <li>Coverage (x)</li>
                                                <li>Aligned %</li></li>
                                            </ul>
                                        </li>
                                        <? echo $sccmec->coverage['lists'][0]; ?>
                                    </ul>
                                </li>
                                <li style="min-width: 20em;">
                                    <ul class="side-nav sccmecCov">
                                        <li>
                                            <ul class="inline-list">
                                                <li></li>
                                                <li>Coverage (x)</li>
                                                <li>Aligned %</li></li>
                                            </ul>
                                        </li>
                                        <? echo $sccmec->coverage['lists'][1]; ?>
                                    </ul>
                                </li>
                                <li style="min-width: 20em;">
                                    <ul class="side-nav sccmecCov">
                                        <li>
                                            <ul class="inline-list">
                                                <li></li>
                                                <li>Coverage (x)</li>
                                                <li>Aligned %</li></li>
                                            </ul>
                                        </li>
                                        <? echo $sccmec->coverage['lists'][2]; ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>            
                    </div>
                </div>
