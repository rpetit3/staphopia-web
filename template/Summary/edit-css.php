    <style>
        .required {
            font-weight: bold;
            font-style: italic;
        }
        div.callout {
            padding: 0.5em !important;
        }
        img.loading {
            display: none;
            margin-left: auto;
            margin-right: auto;
        }
        .ui-widget-content {
            width: 410px !important;
        }
        .ui-autocomplete {
          padding: 0;
          list-style: none;
          background-color: #fff;
          width: 218px;
          border: 1px solid #B0BECA;
          max-height: 350px;
          overflow-y: scroll;
        }
        .ui-autocomplete .ui-menu-item a {
          border-top: 1px solid #B0BECA;
          display: block;
          padding: 4px 6px;
          color: #353D44;
          cursor: pointer;
        }
        .ui-autocomplete .ui-menu-item:first-child a {
          border-top: none;
        }
        .ui-autocomplete .ui-menu-item a.ui-state-hover {
          background-color: #D5E5F4;
          color: #161A1C;
        }
        #HideDropDown div.dropdown {
            display: none;
        }
        
        #IsolationCountry {
            display: block !important;
        }
        
        span.GeoNames {
            font-size: 0.7em;
            float: right;
            bottom: 0px;
            line-height: 2.32em;
        }
        
        .loading {
            display: inline !important;
        }
        
        .mic {
            font-size: 0.7em;
            padding-top: 1em;
            text-align: right;
            font-style: italic;
            font-weight: bold;
        }
    </style>