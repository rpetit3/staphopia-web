<?
    require_once ('/var/www/staphopia/lib/HTML/Edit.php');
    $edit = new Edit($_SESSION['SampleTag'], '', '');
    $edit->GetHistory();

?>

        <div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center"><? echo $summary->seqname; ?> History</h4>
                <p class="indent">
                    A history of all associated meta-data changes.
                </p>
                <hr />
            </div>
        </div>

        <div class="row">
            <div class="small-12 small-centered columns">
                <table id="HistoryTable" class="HistoryTable">
                    <thead>
                        <tr>
                            <td>User</td>
                            <td>Category</td>
                            <td>Date</td>
                            <td>Change From</td>
                            <td>Change To</td>
                        </tr>
                    </thead>
                    <tbody>

                <? foreach ($edit->history as $record) : ?>
                        <tr>
                            <td><? echo $record['UserName']; ?></td>
                            <td><? echo preg_replace('/(?<!\ )[A-Z]/', ' $0', $record['Tag']); ?></td>
                            <td>
                            
                            <? echo date('F j, Y',strtotime($record['Date'])); ?></td>
                            <td>
                                <? if (empty($record['From']) || preg_match('/^DNE$/', $record['From'])) : ?>
                                    -
                                <? else : ?>
                                    <? echo $record['From']; ?>
                                <? endif; ?>
                            </td>
                            <td>
                                <? if (empty($record['To']) || preg_match('/^DNE$/', $record['To'])) : ?>
                                    -
                                <? else : ?>
                                    <? echo $record['To']; ?>
                                <? endif; ?>
                            </td>
                        </tr>
                <? endforeach; ?>



                    </tbody>
                </table>
            </div>
        </div>
