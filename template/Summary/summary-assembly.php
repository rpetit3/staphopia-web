            <div class="summary-assembly info">
                <div class="row">
                    <div class="small-10 small-centered large-8 large-centered columns">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="width: 50%;" >Program:</td>
                                    <td style="width: 50%;" ><? echo $summary->program[$assembly->stats[0]['ProgramID']]['Name']; ?></td>
                                </tr>
                                <tr>
                                    <td>Version:</td>
                                    <td><? echo $summary->program[$assembly->stats[0]['ProgramID']]['Version']; ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Total Size:</td>
                                    <td><? echo number_format($assembly->stats[0]['TotalSize']); ?> bp</td>
                                </tr>
                                <tr>
                                    <td>Total Contigs:</td>
                                    <td><? echo number_format($assembly->stats[0]['TotalContigs']); ?></td>
                                </tr>
                                <tr>
                                    <td>N50:</td>
                                    <td><? echo number_format($assembly->stats[0]['N50']); ?> bp</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Smallest Contig:</td>
                                    <td><? echo number_format($assembly->stats[0]['MinContig']); ?> bp</td>
                                </tr>
                                <tr>
                                    <td>Largest Contig:</td>
                                    <td><? echo number_format($assembly->stats[0]['MaxContig']); ?> bp</td>
                                </tr>
                                <tr>
                                    <td>Mean Contig:</td>
                                    <td><? echo number_format($assembly->stats[0]['Mean']); ?> bp</td>
                                </tr>
                                <tr>
                                    <td>Median Contig:</td>
                                    <td><? echo number_format($assembly->stats[0]['Median']); ?> bp</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Contigs >= 500bp:</td>
                                    <td><? echo number_format($assembly->stats[0]['Greater500']); ?></td>
                                </tr>
                                <tr>
                                    <td>Contigs >= 1kbp:</td>
                                    <td><? echo number_format($assembly->stats[0]['Greater1K']); ?></td>
                                </tr>
                                <tr>
                                    <td>Contigs >= 10kbp:</td>
                                    <td><? echo number_format($assembly->stats[0]['Greater10K']); ?></td>
                                </tr>
                                <tr>
                                    <td>Contigs >= 100kbp:</td>
                                    <td><? echo number_format($assembly->stats[0]['Greater100K']); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
