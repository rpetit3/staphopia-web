        <!-- Categories -->
        <div class="row">
            <div class="small-12 small-centered large-5 large-centered columns category">
                <p></p>
                <dl class="sub-nav selected">
                    <dd class="active summary"><a href="#" onclick="subCategory('summary', 0)">Summary</a></dd>
                    <dd class="mlst"><a href="#" onclick="subCategory('mlst', 5)">MLST</a></dd>
                    
                    <? if ($summary->fullView) : ?>
                    
                    <dd class="sccmec"><a href="#" onclick="subCategory('sccmec', 7)">SCCmec</a></dd>
                    <dd class="resistance"><a href="#" onclick="subCategory('resistance', 11)">Resistance</a></dd>
                    <dd class="virulence"><a href="#" onclick="subCategory('virulence', 12)">Virulence</a></dd>   
                                
                    <? endif; ?>

                </dl>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <hr class="shift-up" />
            </div>
        </div>
        <div class="row">    
            <div class="small-12 small-centered large-5 large-centered columns sub-category">
                <dl class="sub-nav summary selected">
                    <dd class="active outline">
                        <a href="#" onclick="showResults('outline','summary', 0);">Outline</a>
                    </dd>
                    <dd class="meta-data">
                        <a href="#" onclick="showResults('meta-data','summary', 1);">Meta-Data</a>
                    </dd>
                    <dd class="phenotype">
                        <a href="#" onclick="showResults('phenotype','summary', 2);">Phenotype</a>
                    </dd>
                    <dd class="sequence">
                        <a href="#" onclick="showResults('sequence','summary', 3);">Sequence</a>
                    </dd>
                    <dd class="assembly">
                        <a href="#" onclick="showResults('assembly','summary', 4);">Assembly</a>
                    </dd>
                </dl>
                <dl class="sub-nav mlst">
                    <dd class="active outline">
                        <a href="#" onclick="showResults('outline','mlst', 5);">Outline</a>
                    </dd>
                    <dd class="related">
                        <a href="#" onclick="showResults('related','mlst', 6);">Related Samples</a>
                    </dd>
                </dl>
                
                <? if ($summary->fullView) : ?>
                
                <dl class="sub-nav sccmec">
                    <dd class="active outline" onclick="showResults('outline','sccmec', 7);">
                        <a href="#">Outline</a>
                    </dd>
                    <dd class="coverage">
                        <a href="#" onclick="showResults('coverage','sccmec', 8);">Cassette Coverage</a>
                    </dd>
                    <dd class="protein">
                        <a href="#" onclick="showResults('protein','sccmec', 9);">Protein Hits</a>
                    </dd>
                    <dd class="primer">
                        <a href="#" onclick="showResults('primer','sccmec', 10);">Primer Hits</a>
                    </dd>
                </dl>
                <dl class="sub-nav resistance">
                    <dd class="active outline" onclick="showResults('outline','resistance', 11);">
                        <a href="#">Outline</a>
                    </dd>
                    <dd class="protein">
                        <a href="#" onclick="showResults('protein','resistance', 9);">Protein Hits</a></a>
                    </dd>
                </dl>
                <dl class="sub-nav virulence">
                    <dd class="active outline" onclick="showResults('outline','virulence', 12);">
                        <a href="#">Outline</a>
                    </dd>
                    <dd class="protein">
                        <a href="#" onclick="showResults('protein','virulence', 9);">Protein Hits</a>
                    </dd>
                </dl>
                    
                <? endif; ?>
                
            </div>
        </div>
        
        <div class="row">
            <div class="small-12 columns">
                <hr class="shift-up" />
            </div>
        </div>
        <div class="row title-row">
            <div class="small-9 large-12 columns">
                <div class="row">
                    <div class="small-5 large-2 columns grade">
                        <img id="<? echo strtolower($summary->grade); ?>" src="/img/<? echo strtolower($summary->grade); ?>.png" />
                    </div>
                    <div class="small-5 large-8 columns title">
                        <h4>Summary Outline</h4>
                    </div>
                    <div class="small-2 large-2 columns seqname">
                        <? echo $summary->seqname;?> <br />
                        
                        <? if ($_SESSION['IsLoggedIn']) : ?>
                        
                            <a href="/summary/<? echo $summary->seqname; ?>/edit" title="Edit meta-data.">
                                <i class="foundicon-edit"></i>
                            </a>
                            <a href="/summary/<? echo $summary->seqname; ?>/history" title="View previous edits.">
                                <i class="foundicon-graph"></i>
                            </a>
                        
                        <? endif; ?>

                    </div>
                </div>
            </div>
        </div>
        
        <!-- Main Content-->
        <div class="results">

        <!-- Summary -->     
        <? include ('../template/Summary/summary-outline.php') ?>
        <? include ('../template/Summary/summary-meta-data.php') ?>
        <? include ('../template/Summary/summary-phenotype.php') ?>
        <? include ('../template/Summary/summary-sequence.php') ?>
        <? include ('../template/Summary/summary-assembly.php') ?>

        <!-- MLST -->
        <? require ('../template/Summary/mlst-outline.php') ?>
        <? include ('../template/Summary/mlst-related.php') ?>

        <? if ($summary->fullView) : ?>

        <!-- SCCmec -->
        <? include ('../template/Summary/sccmec-outline.php') ?>
        <? include ('../template/Summary/sccmec-coverage.php') ?>
        <? include ('../template/Summary/sccmec-protein.php') ?>
        <? include ('../template/Summary/sccmec-primer.php') ?>
        
        <!-- Resistance -->
        <? include ('../template/Summary/resistance-outline.php') ?>
        <? include ('../template/Summary/resistance-protein.php') ?>
        
        <!-- Virulence -->
        <? include ('../template/Summary/virulence-outline.php') ?>
        <? include ('../template/Summary/virulence-protein.php') ?>
        
        
        <? endif; ?>
        </div>
        <!-- End Main Content-->

        <? if (!$summary->fullView) : ?>
        
        <!-- Disclaimer -->
        <div class="row">
            <div class="small-12 columns disclaimer">
                <hr />
                <p class="small"><i><strong>Note: Use of unpublished sequence data in manuscripts.</strong></i></p>
                <p class="indent small">
                    If you plan to use unpublished data presented by Staphopia in your manuscripts, please make sure you 
                    are complying with the data use policies of the institutions that generated the data (follow the 
                    links through the SRA or sequence center page).  Users have a responsibility to 1)  Recognize the 
                    right of the data providers to initial publication of their data 2) Cite data providers in their 
                    publications 3) Assist journals and funding agencies in appropriate citation of the data.
                </p>
            </div>
        </div>
        <!-- End Disclaimer -->
        
        <? endif; ?>
        