            <div class="sccmec-outline info">
                <div class="row">
                    <div class="small-12 small-centered columns">
                        <h5 class="center">Top SCCmec Coverage Plot</h5>
                        <div class="panel no-border">
                            <? echo $sccmec->best; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 small-centered columns">
                        <hr />
                        <p></p>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <ul class="large-block-grid-2">
                            <li style="min-width: 30em;">
                                <h5 class="center">Significant Protein Hits</h5>
                                <table id="mecSProtein" class="proteinSTable" >
                                    <thead>
                                        <tr>
                                            <td>Gene</td>
                                            <td>Accession</td>
                                            <td>% Identity</td>
                                            <td>% Covered</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? echo $sccmec->proteins['strong']; ?>
                                    </tbody>
                                </table>
                            </li>
                            <li style="min-width: 30em;">
                                <h5 class="center">Significant Primer Hits</h5>
                                <table id="mecSPrimers" class="proteinSTable">
                                    <thead>
                                        <tr>
                                            <td>Gene</td>
                                            <td>Primer</td>
                                            <td>% Identity</td>
                                            <td>% Covered</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? echo $sccmec->primers['strong']; ?>
                                    </tbody>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>    
            </div>
