<div class="summary-outline info selected">
                <div class="row">
                    <div class="large-10 large-centered columns">
                        <div class="row">
                            <div class="large-8 columns">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td style="width: 40%;">Processed On:</td>
                                            <td style="width: 60%;"><? echo date("F j, Y", strtotime($summary->submit_date)); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Owner:</td>
                                            <td><? echo $summary->owner; ?></td>
                                        </tr>
                                        <tr>
                                            <td>SRA Experiment:</td>
                                            <td>
                                                <?  
                                                    if (isset($summary->info['SRAExperiment'])) {
                                                        echo '<a href="http://www.ncbi.nlm.nih.gov/sra/?term='. $summary->info['SRAExperiment'] .'" title="Open link in a new tab."
                                                                    target="_blank">'. $summary->info['SRAExperiment'] .'</a>';
                                                    }
                                                    else {
                                                        echo '-';
                                                    } 
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Visibility:</td>
                                            <td><? echo $summary->info['isPublic']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Publication:</td>
                                            <td><? echo (isset($summary->info['isPublished']) ? $summary->info['isPublished'] : '-'); ?></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Strain:</td>
                                            <td><? echo (isset($summary->info['Strain']) ? $summary->info['Strain'] : "-") ?></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Estimated Coverage:</td>
                                            <td><? echo round($summary->filtered->stats[0]['Coverage']); ?>x</td>
                                        </tr>
                                        <tr>
                                            <td>Average Read Length:</td>
                                            <td><? echo number_format(round($summary->filtered->stats[0]['MeanLength'])); ?> bp</td>
                                        </tr>
                                        <tr>
                                            <td>Average Quality:</td>
                                            <td>Q<? echo round($summary->filtered->stats[0]['Quality']); ?></td>
                                        </tr>
                                        <tr>
                                            <td>GC Content:</td>
                                            <td><? echo substr($assembly->stats[0]['GC'], 0, 5); ?> %</td>
                                        </tr>
                                        <tr>
                                            <td>Assembled Size:</td>
                                            <td><? echo number_format($assembly->stats[0]['TotalSize']); ?> bp</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="large-4 columns">
                                <div class="row ">
                                    <div class="large-12 columns">
                                        <p></p>
                                        <div class="panel no-border summary-box">
                                            <h5 class="italic">MLST:</h5>
                                            <h4 class="centered"><? echo ($mlst->stats[0]['SequenceType'] == 0 ? '-' : $mlst->stats[0]['SequenceType']); ?></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="large-12 columns">
                                        <div class="panel no-border summary-box">
                                            <h5 class="italic">Clonal Complex:</h5>
                                            <h4 class="centered"><? echo ($mlst->stats[0]['ClonalComplex'] == 0 ? '-' : $mlst->stats[0]['ClonalComplex']); ?></h4>
                                        </div>
                                    </div>
                                </div>
                                
                                <? if ($summary->fullView) : ?>
                                
                                <div class="row ">
                                    <div class="large-12 columns">
                                        <div class="panel no-border summary-box">
                                            <h5 class="italic">SCCmec Type:</h5>
                                            <h4 class="centered"><? echo $sccmec->mecType['consensus']; ?></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="large-12 columns">
                                        <div class="panel no-border summary-box">
                                            <h5 class="italic">Resistance Hits:</h5>
                                            <h4 class="centered"><? echo ($resistance->rHits == 0 ? '-' : $resistance->rHits ); ?></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="large-12 columns">
                                        <div class="panel no-border summary-box">
                                            <h5 class="italic">Virulence Hits:</h5>
                                            <h4 class="centered"><? echo ($virulence->vHits == 0 ? '-' : $virulence->vHits); ?></h4>
                                        </div>
                                    </div>
                                </div>
                                
                                <? endif; ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
