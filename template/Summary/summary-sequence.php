            <div class="summary-sequence info">
                <div class="row">
                    <div class="small-10 small-centered large-8 large-centered columns">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 50%;" >Estimated Coverage:</td>
                                <td style="width: 50%;" ><? echo round($summary->filtered->stats[0]['Coverage']); ?>x</td>
                            </tr>                            
                            <tr>
                                <td>Average Quality:</td>
                                <td>Q<? echo round($summary->filtered->stats[0]['Quality']); ?></td>
                            </tr>
                            <tr>
                                <td>GC Content:</td>
                                <td><? echo substr($assembly->stats[0]['GC'], 0, 5); ?> %</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Total Base Pairs:</td>
                                <td><? echo number_format($summary->filtered->stats[0]['BaseCount']); ?> bp</td>
                            </tr>
                            <tr>
                                <td>Total Reads:</td>
                                <td><? echo number_format($summary->filtered->stats[0]['ReadCount']); ?></td>
                            </tr>
                            <tr>
                                <td>Average Read Length:</td>
                                <td><? echo number_format(round($summary->filtered->stats[0]['MeanLength'])); ?> bp</td>
                            </tr>
                             <tr>
                                <td>Minimum Read Length:</td>
                                <td><? echo number_format(round($summary->filtered->stats[0]['MinLength'])); ?> bp</td>
                            </tr>
                            <tr>
                                <td>Maximum Read Length:</td>
                                <td><? echo number_format(round($summary->filtered->stats[0]['MaxLength'])); ?> bp</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
