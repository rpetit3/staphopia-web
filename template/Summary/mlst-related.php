            <div class="mlst-related info">
                <div class="row">
                    <div class="small-12 small-centered columns">
                        <div class="panel no-border">
                            <p></p>
                            <table id="mlstRelated">
                                <thead>
                                    <tr>
                                        <td>Seq Name</td>
                                        <td>Class</td>
                                        <td>Sequence Type</td>
                                        <td>Clonal Complex</td>
                                        <td>Submission Date</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? echo $mlst->get_related( $mlst->stats[0]['ClonalComplex'], $summary->GRADE ); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
