<div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                    <h4 class="center">Update <? echo $summary->seqname; ?> Associated Information</h4>
                <p class="indent">
                    You may use this form to edit to the information asscociated with genomes you have already 
                    submitted.  Edit history will be tracked in the database, so may revert to previous edits
                    if ever nedded.
                </p>
            </div>
        </div>
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-12 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
                <hr class="shift-up"/>
            </div>
        </div>
        
        <? endif; ?>
        <div class="row">
            <div class="large-12 columns">
                <form class="custom" id="SubmitForm" action="/php-bin/summary-edit.php" method="post" enctype="multipart/form-data">
                <div class="row title-row">
                    <div class="large-12 large-centered columns title">
                        <hr/>
                        Project Information
                        <hr />
                    </div>
                </div>

                    <fieldset class="project selected">
                        <div class="row">
                            <div class="large-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <label class="required">Contact Name </label>
                                            <input name="ContactName" placeholder="Arthur Dent" type="text" required tabindex="1"
                                                   value="<? echo (isset($summary->info['ContactName'])) ? $summary->info['ContactName'] : ''; ?>" />
                                        </li>
                                        <li>
                                            <label class="required">Contact Email </label>
                                            <input name="ContactEmail" placeholder="my@email.com" type="email" required tabindex="2"
                                                   value="<? echo (isset($summary->info['ContactEmail'])) ? $summary->info['ContactEmail'] : ''; ?>" />
                                        </li>
                                        <li>
                                            <label>Contact Link </label>
                                            <input name="ContactLink" placeholder="http://www.mysite.com" type="url" tabindex="3"
                                                   value="<? echo (isset($summary->info['ContactLink'])) ? $summary->info['ContactLink'] : ''; ?>" />
                                        </li>
                                        <li>
                                            <label>SRA Experiment </label>
                                            <input name="SRAExperiment" placeholder="SRX069237" type="text" tabindex="4"
                                                   value="<? echo (isset($summary->info['SRAExperiment'])) ? $summary->info['SRAExperiment'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>SRA Study </label>
                                            <input name="SRAStudy" placeholder="SRP006949" type="text" tabindex="5"
                                                   value="<? echo (isset($summary->info['SRAStudy'])) ? $summary->info['SRAStudy'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>SRA Sample </label>
                                            <input name="SRASample" placeholder="SRS211516" type="text" tabindex="6"
                                                   value="<? echo (isset($summary->info['SRASample'])) ? $summary->info['SRASample'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>PubMed ID </label>
                                            <input name="PubMedID" placeholder="20802046, 20802048" type="text" tabindex="5"
                                                   value="<? echo (isset($summary->info['PubMedID'])) ? $summary->info['PubMedID'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>Digital Object Identifier (DOI) </label>
                                            <input name="DigitalObjectIdentifier" placeholder="10.1128/JB.00951-10" type="text" tabindex="6"
                                                   value="<? echo (isset($summary->info['DigitalObjectIdentifier'])) ? $summary->info['DigitalObjectIdentifier'] : ''; ?>"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="large-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <label class="required">Sequencing Center </label>
                                            <input name="SequencingCenter" placeholder="Emory Genome Center, Broad, etc..." type="text" required tabindex="7"
                                                   value="<? echo (isset($summary->info['SequencingCenter'])) ? $summary->info['SequencingCenter'] : ''; ?>" />
                                        </li>
                                        <li>
                                            <label>Sequencing Center Link </label>
                                            <input name="SequencingCenterLink" placeholder="http://www.seq-center.com" type="url" tabindex="8"
                                                   value="<? echo (isset($summary->info['SequencingCenterLink'])) ? $summary->info['SequencingCenterLink'] : ''; ?>" />
                                        </li>
                                        <li>
                                            <label>Sequencing Date</label>
                                            <input id="SequencingDate" placeholder="03/11/2001" name="SequencingDate" type="date" tabindex="9"
                                                   value="<? echo (isset($summary->info['SequencingDate'])) ? $summary->info['SequencingDate'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>Sequencing Library Method</label>
                                            <input name="SequencingLibraryMethod" placeholder="Illumina standard library, etc..." type="text" tabindex="10"
                                                   value="<? echo (isset($summary->info['SequencingLibraryMethod'])) ? $summary->info['SequencingLibraryMethod'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>Funding Agency Name </label>
                                            <input name="FundingAgency" placeholder="NIH, NSF, etc ..." type="text" tabindex="11"
                                                   value="<? echo (isset($summary->info['FundingAgency'])) ? $summary->info['FundingAgency'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>Funding Agency Link </label>
                                            <input name="FundingAgencyLink" placeholder="http://www.fund-agency.com" type="url" tabindex="12"
                                                   value="<? echo (isset($summary->info['FundingAgencyLink'])) ? $summary->info['FundingAgencyLink'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>Publication Link </label>
                                            <input name="PublicationLink" placeholder="http://www.pub-link.com" type="url" tabindex="4"
                                                   value="<? echo (isset($summary->info['PublicationLink'])) ? $summary->info['PublicationLink'] : ''; ?>"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        

                    </fieldset>
                    
                    <div class="row title-row">
                        <div class="large-12 large-centered columns title">
                            <hr/>
                            Organism Information
                            <hr/>
                        </div>
                    </div>
                    <fieldset class="organism selected">
                        <div class="row">
                            <div class="large-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <label>Strain </label>
                                            <input name="Strain" placeholder="USA300, Newman, 119752, etc..." type="text" tabindex="13"
                                                   value="<? echo (isset($summary->info['Strain'])) ? $summary->info['Strain'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>Isolation Date </label>
                                            <input id="IsolationDate" placeholder="03/11/2001" name="IsolationDate" type="date" tabindex="14"
                                                   value="<? echo (isset($summary->info['IsolationDate'])) ? $summary->info['IsolationDate'] : ''; ?>"/>
                                        </li>
                                        <li id="HideDropDown">
                                            <label>Isolation Country </label>
                                            <select id="country-selector" autocorrect="off" autocomplete="off" tabindex="15">
                                                <option value="-" data-alternative-spellings="-" selected="selected"></option>
                                            	<option value="Afghanistan" data-alternative-spellings="AF افغانستان">Afghanistan</option>
                                            	<option value="Åland Islands" data-alternative-spellings="AX Aaland Aland" data-relevancy-booster="0.5">Åland Islands</option>
                                            	<option value="Albania" data-alternative-spellings="AL">Albania</option>
                                            	<option value="Algeria" data-alternative-spellings="DZ الجزائر">Algeria</option>
                                            	<option value="American Samoa" data-alternative-spellings="AS" data-relevancy-booster="0.5">American Samoa</option>
                                            	<option value="Andorra" data-alternative-spellings="AD" data-relevancy-booster="0.5">Andorra</option>
                                            	<option value="Angola" data-alternative-spellings="AO">Angola</option>
                                            	<option value="Anguilla" data-alternative-spellings="AI" data-relevancy-booster="0.5">Anguilla</option>
                                            	<option value="Antarctica" data-alternative-spellings="AQ" data-relevancy-booster="0.5">Antarctica</option>
                                            	<option value="Antigua And Barbuda" data-alternative-spellings="AG" data-relevancy-booster="0.5">Antigua And Barbuda</option>
                                            	<option value="Argentina" data-alternative-spellings="AR">Argentina</option>
                                            	<option value="Armenia" data-alternative-spellings="AM Հայաստան">Armenia</option>
                                            	<option value="Aruba" data-alternative-spellings="AW" data-relevancy-booster="0.5">Aruba</option>
                                            	<option value="Australia" data-alternative-spellings="AU" data-relevancy-booster="1.5">Australia</option>
                                            	<option value="Austria" data-alternative-spellings="AT Österreich Osterreich Oesterreich ">Austria</option>
                                            	<option value="Azerbaijan" data-alternative-spellings="AZ">Azerbaijan</option>
                                            	<option value="Bahamas" data-alternative-spellings="BS">Bahamas</option>
                                            	<option value="Bahrain" data-alternative-spellings="BH البحرين">Bahrain</option>
                                            	<option value="Bangladesh" data-alternative-spellings="BD বাংলাদেশ" data-relevancy-booster="2">Bangladesh</option>
                                            	<option value="Barbados" data-alternative-spellings="BB">Barbados</option>
                                            	<option value="Belarus" data-alternative-spellings="BY Беларусь">Belarus</option>
                                            	<option value="Belgium" data-alternative-spellings="BE België Belgie Belgien Belgique" data-relevancy-booster="1.5">Belgium</option>
                                            	<option value="Belize" data-alternative-spellings="BZ">Belize</option>
                                            	<option value="Benin" data-alternative-spellings="BJ">Benin</option>
                                            	<option value="Bermuda" data-alternative-spellings="BM" data-relevancy-booster="0.5">Bermuda</option>
                                            	<option value="Bhutan" data-alternative-spellings="BT भूटान">Bhutan</option>
                                            	<option value="Bolivia" data-alternative-spellings="BO">Bolivia</option>
                                            	<option value="Bonaire, Sint Eustatius and Saba" data-alternative-spellings="BQ">Bonaire, Sint Eustatius and Saba</option>
                                            	<option value="Bosnia and Herzegovina" data-alternative-spellings="BA Босна и Херцеговина">Bosnia and Herzegovina</option>
                                            	<option value="Botswana" data-alternative-spellings="BW">Botswana</option>
                                            	<option value="Bouvet Island" data-alternative-spellings="BV">Bouvet Island</option>
                                            	<option value="Brazil" data-alternative-spellings="BR Brasil" data-relevancy-booster="2">Brazil</option>
                                            	<option value="British Indian Ocean Territory" data-alternative-spellings="IO">British Indian Ocean Territory</option>
                                            	<option value="Brunei Darussalam" data-alternative-spellings="BN">Brunei Darussalam</option>
                                            	<option value="Bulgaria" data-alternative-spellings="BG България">Bulgaria</option>
                                            	<option value="Burkina Faso" data-alternative-spellings="BF">Burkina Faso</option>
                                            	<option value="Burundi" data-alternative-spellings="BI">Burundi</option>
                                            	<option value="Cambodia" data-alternative-spellings="KH កម្ពុជា">Cambodia</option>
                                            	<option value="Cameroon" data-alternative-spellings="CM">Cameroon</option>
                                            	<option value="Canada" data-alternative-spellings="CA" data-relevancy-booster="2">Canada</option>
                                            	<option value="Cape Verde" data-alternative-spellings="CV Cabo">Cape Verde</option>
                                            	<option value="Cayman Islands" data-alternative-spellings="KY" data-relevancy-booster="0.5">Cayman Islands</option>
                                            	<option value="Central African Republic" data-alternative-spellings="CF">Central African Republic</option>
                                            	<option value="Chad" data-alternative-spellings="TD تشاد‎ Tchad">Chad</option>
                                            	<option value="Chile" data-alternative-spellings="CL">Chile</option>
                                            	<option value="China" data-relevancy-booster="3.5" data-alternative-spellings="CN Zhongguo Zhonghua Peoples Republic 中国/中华">China</option>
                                            	<option value="Christmas Island" data-alternative-spellings="CX" data-relevancy-booster="0.5">Christmas Island</option>
                                            	<option value="Cocos (Keeling) Islands" data-alternative-spellings="CC" data-relevancy-booster="0.5">Cocos (Keeling) Islands</option>
                                            	<option value="Colombia" data-alternative-spellings="CO">Colombia</option>
                                            	<option value="Comoros" data-alternative-spellings="KM جزر القمر">Comoros</option>
                                            	<option value="Congo" data-alternative-spellings="CG">Congo</option>
                                            	<option value="Congo, the Democratic Republic of the" data-alternative-spellings="CD Congo-Brazzaville Repubilika ya Kongo">Congo, the Democratic Republic of the</option>
                                            	<option value="Cook Islands" data-alternative-spellings="CK" data-relevancy-booster="0.5">Cook Islands</option>
                                            	<option value="Costa Rica" data-alternative-spellings="CR">Costa Rica</option>
                                            	<option value="Côte d'Ivoire" data-alternative-spellings="CI Cote dIvoire">Côte d'Ivoire</option>
                                            	<option value="Croatia" data-alternative-spellings="HR Hrvatska">Croatia</option>
                                            	<option value="Cuba" data-alternative-spellings="CU">Cuba</option>
                                            	<option value="Curaçao" data-alternative-spellings="CW Curacao">Curaçao</option>
                                            	<option value="Cyprus" data-alternative-spellings="CY Κύπρος Kýpros Kıbrıs">Cyprus</option>
                                            	<option value="Czech Republic" data-alternative-spellings="CZ Česká Ceska">Czech Republic</option>
                                            	<option value="Denmark" data-alternative-spellings="DK Danmark" data-relevancy-booster="1.5">Denmark</option>
                                            	<option value="Djibouti" data-alternative-spellings="DJ جيبوتي‎ Jabuuti Gabuuti">Djibouti</option>
                                            	<option value="Dominica" data-alternative-spellings="DM Dominique" data-relevancy-booster="0.5">Dominica</option>
                                            	<option value="Dominican Republic" data-alternative-spellings="DO">Dominican Republic</option>
                                            	<option value="Ecuador" data-alternative-spellings="EC">Ecuador</option>
                                            	<option value="Egypt" data-alternative-spellings="EG" data-relevancy-booster="1.5">Egypt</option>
                                            	<option value="El Salvador" data-alternative-spellings="SV">El Salvador</option>
                                            	<option value="Equatorial Guinea" data-alternative-spellings="GQ">Equatorial Guinea</option>
                                            	<option value="Eritrea" data-alternative-spellings="ER إرتريا ኤርትራ">Eritrea</option>
                                            	<option value="Estonia" data-alternative-spellings="EE Eesti">Estonia</option>
                                            	<option value="Ethiopia" data-alternative-spellings="ET ኢትዮጵያ">Ethiopia</option>
                                            	<option value="Falkland Islands (Malvinas)" data-alternative-spellings="FK" data-relevancy-booster="0.5">Falkland Islands (Malvinas)</option>
                                            	<option value="Faroe Islands" data-alternative-spellings="FO Føroyar Færøerne" data-relevancy-booster="0.5">Faroe Islands</option>
                                            	<option value="Fiji" data-alternative-spellings="FJ Viti फ़िजी">Fiji</option>
                                            	<option value="Finland" data-alternative-spellings="FI Suomi">Finland</option>
                                            	<option value="France" data-alternative-spellings="FR République française" data-relevancy-booster="2.5">France</option>
                                            	<option value="French Guiana" data-alternative-spellings="GF">French Guiana</option>
                                            	<option value="French Polynesia" data-alternative-spellings="PF Polynésie française">French Polynesia</option>
                                            	<option value="French Southern Territories" data-alternative-spellings="TF">French Southern Territories</option>
                                            	<option value="Gabon" data-alternative-spellings="GA République Gabonaise">Gabon</option>
                                            	<option value="Gambia" data-alternative-spellings="GM">Gambia</option>
                                            	<option value="Georgia" data-alternative-spellings="GE საქართველო">Georgia</option>
                                            	<option value="Germany" data-alternative-spellings="DE Bundesrepublik Deutschland" data-relevancy-booster="3">Germany</option>
                                            	<option value="Ghana" data-alternative-spellings="GH">Ghana</option>
                                            	<option value="Gibraltar" data-alternative-spellings="GI" data-relevancy-booster="0.5">Gibraltar</option>
                                            	<option value="Greece" data-alternative-spellings="GR Ελλάδα" data-relevancy-booster="1.5">Greece</option>
                                            	<option value="Greenland" data-alternative-spellings="GL grønland" data-relevancy-booster="0.5">Greenland</option>
                                            	<option value="Grenada" data-alternative-spellings="GD">Grenada</option>
                                            	<option value="Guadeloupe" data-alternative-spellings="GP">Guadeloupe</option>
                                            	<option value="Guam" data-alternative-spellings="GU">Guam</option>
                                            	<option value="Guatemala" data-alternative-spellings="GT">Guatemala</option>
                                            	<option value="Guernsey" data-alternative-spellings="GG" data-relevancy-booster="0.5">Guernsey</option>
                                            	<option value="Guinea" data-alternative-spellings="GN">Guinea</option>
                                            	<option value="Guinea-Bissau" data-alternative-spellings="GW">Guinea-Bissau</option>
                                            	<option value="Guyana" data-alternative-spellings="GY">Guyana</option>
                                            	<option value="Haiti" data-alternative-spellings="HT">Haiti</option>
                                            	<option value="Heard Island and McDonald Islands" data-alternative-spellings="HM">Heard Island and McDonald Islands</option>
                                            	<option value="Holy See (Vatican City State)" data-alternative-spellings="VA" data-relevancy-booster="0.5">Holy See (Vatican City State)</option>
                                            	<option value="Honduras" data-alternative-spellings="HN">Honduras</option>
                                            	<option value="Hong Kong" data-alternative-spellings="HK 香港">Hong Kong</option>
                                            	<option value="Hungary" data-alternative-spellings="HU Magyarország">Hungary</option>
                                            	<option value="Iceland" data-alternative-spellings="IS Island">Iceland</option>
                                            	<option value="India" data-alternative-spellings="IN भारत गणराज्य Hindustan" data-relevancy-booster="3">India</option>
                                            	<option value="Indonesia" data-alternative-spellings="ID" data-relevancy-booster="2">Indonesia</option>
                                            	<option value="Iran, Islamic Republic of" data-alternative-spellings="IR ایران">Iran, Islamic Republic of</option>
                                            	<option value="Iraq" data-alternative-spellings="IQ العراق‎">Iraq</option>
                                            	<option value="Ireland" data-alternative-spellings="IE Éire" data-relevancy-booster="1.2">Ireland</option>
                                            	<option value="Isle of Man" data-alternative-spellings="IM" data-relevancy-booster="0.5">Isle of Man</option>
                                            	<option value="Israel" data-alternative-spellings="IL إسرائيل ישראל">Israel</option>
                                            	<option value="Italy" data-alternative-spellings="IT Italia" data-relevancy-booster="2">Italy</option>
                                            	<option value="Jamaica" data-alternative-spellings="JM">Jamaica</option>
                                            	<option value="Japan" data-alternative-spellings="JP Nippon Nihon 日本" data-relevancy-booster="2.5">Japan</option>
                                            	<option value="Jersey" data-alternative-spellings="JE" data-relevancy-booster="0.5">Jersey</option>
                                            	<option value="Jordan" data-alternative-spellings="JO الأردن">Jordan</option>
                                            	<option value="Kazakhstan" data-alternative-spellings="KZ Қазақстан Казахстан">Kazakhstan</option>
                                            	<option value="Kenya" data-alternative-spellings="KE">Kenya</option>
                                            	<option value="Kiribati" data-alternative-spellings="KI">Kiribati</option>
                                            	<option value="Korea, Democratic People's Republic of" data-alternative-spellings="KP North Korea">Korea, Democratic People's Republic of</option>
                                            	<option value="Korea, Republic of" data-alternative-spellings="KR South Korea" data-relevancy-booster="1.5">Korea, Republic of</option>
                                            	<option value="Kuwait" data-alternative-spellings="KW الكويت">Kuwait</option>
                                            	<option value="Kyrgyzstan" data-alternative-spellings="KG Кыргызстан">Kyrgyzstan</option>
                                            	<option value="Lao People's Democratic Republic" data-alternative-spellings="LA">Lao People's Democratic Republic</option>
                                            	<option value="Latvia" data-alternative-spellings="LV Latvija">Latvia</option>
                                            	<option value="Lebanon" data-alternative-spellings="LB لبنان">Lebanon</option>
                                            	<option value="Lesotho" data-alternative-spellings="LS">Lesotho</option>
                                            	<option value="Liberia" data-alternative-spellings="LR">Liberia</option>
                                            	<option value="Libyan Arab Jamahiriya" data-alternative-spellings="LY ليبيا">Libyan Arab Jamahiriya</option>
                                            	<option value="Liechtenstein" data-alternative-spellings="LI">Liechtenstein</option>
                                            	<option value="Lithuania" data-alternative-spellings="LT Lietuva">Lithuania</option>
                                            	<option value="Luxembourg" data-alternative-spellings="LU">Luxembourg</option>
                                            	<option value="Macao" data-alternative-spellings="MO">Macao</option>
                                            	<option value="Macedonia, The Former Yugoslav Republic Of" data-alternative-spellings="MK Македонија">Macedonia, The Former Yugoslav Republic Of</option>
                                            	<option value="Madagascar" data-alternative-spellings="MG Madagasikara">Madagascar</option>
                                            	<option value="Malawi" data-alternative-spellings="MW">Malawi</option>
                                            	<option value="Malaysia" data-alternative-spellings="MY">Malaysia</option>
                                            	<option value="Maldives" data-alternative-spellings="MV">Maldives</option>
                                            	<option value="Mali" data-alternative-spellings="ML">Mali</option>
                                            	<option value="Malta" data-alternative-spellings="MT">Malta</option>
                                            	<option value="Marshall Islands" data-alternative-spellings="MH" data-relevancy-booster="0.5">Marshall Islands</option>
                                            	<option value="Martinique" data-alternative-spellings="MQ">Martinique</option>
                                            	<option value="Mauritania" data-alternative-spellings="MR الموريتانية">Mauritania</option>
                                            	<option value="Mauritius" data-alternative-spellings="MU">Mauritius</option>
                                            	<option value="Mayotte" data-alternative-spellings="YT">Mayotte</option>
                                            	<option value="Mexico" data-alternative-spellings="MX Mexicanos" data-relevancy-booster="1.5">Mexico</option>
                                            	<option value="Micronesia, Federated States of" data-alternative-spellings="FM">Micronesia, Federated States of</option>
                                            	<option value="Moldova, Republic of" data-alternative-spellings="MD">Moldova, Republic of</option>
                                            	<option value="Monaco" data-alternative-spellings="MC">Monaco</option>
                                            	<option value="Mongolia" data-alternative-spellings="MN Mongγol ulus Монгол улс">Mongolia</option>
                                            	<option value="Montenegro" data-alternative-spellings="ME">Montenegro</option>
                                            	<option value="Montserrat" data-alternative-spellings="MS" data-relevancy-booster="0.5">Montserrat</option>
                                            	<option value="Morocco" data-alternative-spellings="MA المغرب">Morocco</option>
                                            	<option value="Mozambique" data-alternative-spellings="MZ Moçambique">Mozambique</option>
                                            	<option value="Myanmar" data-alternative-spellings="MM">Myanmar</option>
                                            	<option value="Namibia" data-alternative-spellings="NA Namibië">Namibia</option>
                                            	<option value="Nauru" data-alternative-spellings="NR Naoero" data-relevancy-booster="0.5">Nauru</option>
                                            	<option value="Nepal" data-alternative-spellings="NP नेपाल">Nepal</option>
                                            	<option value="Netherlands" data-alternative-spellings="NL Holland Nederland" data-relevancy-booster="1.5">Netherlands</option>
                                            	<option value="New Caledonia" data-alternative-spellings="NC" data-relevancy-booster="0.5">New Caledonia</option>
                                            	<option value="New Zealand" data-alternative-spellings="NZ Aotearoa">New Zealand</option>
                                            	<option value="Nicaragua" data-alternative-spellings="NI">Nicaragua</option>
                                            	<option value="Niger" data-alternative-spellings="NE Nijar">Niger</option>
                                            	<option value="Nigeria" data-alternative-spellings="NG Nijeriya Naíjíríà" data-relevancy-booster="1.5">Nigeria</option>
                                            	<option value="Niue" data-alternative-spellings="NU" data-relevancy-booster="0.5">Niue</option>
                                            	<option value="Norfolk Island" data-alternative-spellings="NF" data-relevancy-booster="0.5">Norfolk Island</option>
                                            	<option value="Northern Mariana Islands" data-alternative-spellings="MP" data-relevancy-booster="0.5">Northern Mariana Islands</option>
                                            	<option value="Norway" data-alternative-spellings="NO Norge Noreg" data-relevancy-booster="1.5">Norway</option>
                                            	<option value="Oman" data-alternative-spellings="OM عمان">Oman</option>
                                            	<option value="Pakistan" data-alternative-spellings="PK پاکستان" data-relevancy-booster="2">Pakistan</option>
                                            	<option value="Palau" data-alternative-spellings="PW" data-relevancy-booster="0.5">Palau</option>
                                            	<option value="Palestinian Territory, Occupied" data-alternative-spellings="PS فلسطين">Palestinian Territory, Occupied</option>
                                            	<option value="Panama" data-alternative-spellings="PA">Panama</option>
                                            	<option value="Papua New Guinea" data-alternative-spellings="PG">Papua New Guinea</option>
                                            	<option value="Paraguay" data-alternative-spellings="PY">Paraguay</option>
                                            	<option value="Peru" data-alternative-spellings="PE">Peru</option>
                                            	<option value="Philippines" data-alternative-spellings="PH Pilipinas" data-relevancy-booster="1.5">Philippines</option>
                                            	<option value="Pitcairn" data-alternative-spellings="PN" data-relevancy-booster="0.5">Pitcairn</option>
                                            	<option value="Poland" data-alternative-spellings="PL Polska" data-relevancy-booster="1.25">Poland</option>
                                            	<option value="Portugal" data-alternative-spellings="PT Portuguesa" data-relevancy-booster="1.5">Portugal</option>
                                            	<option value="Puerto Rico" data-alternative-spellings="PR">Puerto Rico</option>
                                            	<option value="Qatar" data-alternative-spellings="QA قطر">Qatar</option>
                                            	<option value="Réunion" data-alternative-spellings="RE Reunion">Réunion</option>
                                            	<option value="Romania" data-alternative-spellings="RO Rumania Roumania România">Romania</option>
                                            	<option value="Russian Federation" data-alternative-spellings="RU Rossiya Российская Россия" data-relevancy-booster="2.5">Russian Federation</option>
                                            	<option value="Rwanda" data-alternative-spellings="RW">Rwanda</option>
                                            	<option value="Saint Barthélemy" data-alternative-spellings="BL St. Barthelemy">Saint Barthélemy</option>
                                            	<option value="Saint Helena" data-alternative-spellings="SH St.">Saint Helena</option>
                                            	<option value="Saint Kitts and Nevis" data-alternative-spellings="KN St.">Saint Kitts and Nevis</option>
                                            	<option value="Saint Lucia" data-alternative-spellings="LC St.">Saint Lucia</option>
                                            	<option value="Saint Martin (French Part)" data-alternative-spellings="MF St.">Saint Martin (French Part)</option>
                                            	<option value="Saint Pierre and Miquelon" data-alternative-spellings="PM St.">Saint Pierre and Miquelon</option>
                                            	<option value="Saint Vincent and the Grenadines" data-alternative-spellings="VC St.">Saint Vincent and the Grenadines</option>
                                            	<option value="Samoa" data-alternative-spellings="WS">Samoa</option>
                                            	<option value="San Marino" data-alternative-spellings="SM">San Marino</option>
                                            	<option value="Sao Tome and Principe" data-alternative-spellings="ST">Sao Tome and Principe</option>
                                            	<option value="Saudi Arabia" data-alternative-spellings="SA السعودية">Saudi Arabia</option>
                                            	<option value="Senegal" data-alternative-spellings="SN Sénégal">Senegal</option>
                                            	<option value="Serbia" data-alternative-spellings="RS Србија Srbija">Serbia</option>
                                            	<option value="Seychelles" data-alternative-spellings="SC" data-relevancy-booster="0.5">Seychelles</option>
                                            	<option value="Sierra Leone" data-alternative-spellings="SL">Sierra Leone</option>
                                            	<option value="Singapore" data-alternative-spellings="SG Singapura  சிங்கப்பூர் குடியரசு 新加坡共和国">Singapore</option>
                                            	<option value="Sint Maarten (Dutch Part)" data-alternative-spellings="SX">Sint Maarten (Dutch Part)</option>
                                            	<option value="Slovakia" data-alternative-spellings="SK Slovenská Slovensko">Slovakia</option>
                                            	<option value="Slovenia" data-alternative-spellings="SI Slovenija">Slovenia</option>
                                            	<option value="Solomon Islands" data-alternative-spellings="SB">Solomon Islands</option>
                                            	<option value="Somalia" data-alternative-spellings="SO الصومال">Somalia</option>
                                            	<option value="South Africa" data-alternative-spellings="ZA RSA Suid-Afrika">South Africa</option>
                                            	<option value="South Georgia and the South Sandwich Islands" data-alternative-spellings="GS">South Georgia and the South Sandwich Islands</option>
                                            	<option value="South Sudan" data-alternative-spellings="SS">South Sudan</option>
                                            	<option value="Spain" data-alternative-spellings="ES España" data-relevancy-booster="2">Spain</option>
                                            	<option value="Sri Lanka" data-alternative-spellings="LK ශ්‍රී ලංකා இலங்கை Ceylon">Sri Lanka</option>
                                            	<option value="Sudan" data-alternative-spellings="SD السودان">Sudan</option>
                                            	<option value="Suriname" data-alternative-spellings="SR शर्नम् Sarnam Sranangron">Suriname</option>
                                            	<option value="Svalbard and Jan Mayen" data-alternative-spellings="SJ" data-relevancy-booster="0.5">Svalbard and Jan Mayen</option>
                                            	<option value="Swaziland" data-alternative-spellings="SZ weSwatini Swatini Ngwane">Swaziland</option>
                                            	<option value="Sweden" data-alternative-spellings="SE Sverige" data-relevancy-booster="1.5">Sweden</option>
                                            	<option value="Switzerland" data-alternative-spellings="CH Swiss Confederation Schweiz Suisse Svizzera Svizra" data-relevancy-booster="1.5">Switzerland</option>
                                            	<option value="Syrian Arab Republic" data-alternative-spellings="SY Syria سورية">Syrian Arab Republic</option>
                                            	<option value="Taiwan, Province of China" data-alternative-spellings="TW 台灣 臺灣">Taiwan, Province of China</option>
                                            	<option value="Tajikistan" data-alternative-spellings="TJ Тоҷикистон Toçikiston">Tajikistan</option>
                                            	<option value="Tanzania, United Republic of" data-alternative-spellings="TZ">Tanzania, United Republic of</option>
                                            	<option value="Thailand" data-alternative-spellings="TH ประเทศไทย Prathet Thai">Thailand</option>
                                            	<option value="Timor-Leste" data-alternative-spellings="TL">Timor-Leste</option>
                                            	<option value="Togo" data-alternative-spellings="TG Togolese">Togo</option>
                                            	<option value="Tokelau" data-alternative-spellings="TK" data-relevancy-booster="0.5">Tokelau</option>
                                            	<option value="Tonga" data-alternative-spellings="TO">Tonga</option>
                                            	<option value="Trinidad and Tobago" data-alternative-spellings="TT">Trinidad and Tobago</option>
                                            	<option value="Tunisia" data-alternative-spellings="TN تونس">Tunisia</option>
                                            	<option value="Turkey" data-alternative-spellings="TR Türkiye Turkiye">Turkey</option>
                                            	<option value="Turkmenistan" data-alternative-spellings="TM Türkmenistan">Turkmenistan</option>
                                            	<option value="Turks and Caicos Islands" data-alternative-spellings="TC" data-relevancy-booster="0.5">Turks and Caicos Islands</option>
                                            	<option value="Tuvalu" data-alternative-spellings="TV" data-relevancy-booster="0.5">Tuvalu</option>
                                            	<option value="Uganda" data-alternative-spellings="UG">Uganda</option>
                                            	<option value="Ukraine" data-alternative-spellings="UA Ukrayina Україна">Ukraine</option>
                                            	<option value="United Arab Emirates" data-alternative-spellings="AE UAE الإمارات">United Arab Emirates</option>
                                            	<option value="United Kingdom" data-alternative-spellings="GB Great Britain England UK Wales Scotland Northern Ireland" data-relevancy-booster="2.5">United Kingdom</option>
                                            	<option value="United States" data-relevancy-booster="3.5" data-alternative-spellings="US USA United States of America">United States</option>
                                            	<option value="United States Minor Outlying Islands" data-alternative-spellings="UM">United States Minor Outlying Islands</option>
                                            	<option value="Uruguay" data-alternative-spellings="UY">Uruguay</option>
                                            	<option value="Uzbekistan" data-alternative-spellings="UZ Ўзбекистон O'zbekstan O‘zbekiston">Uzbekistan</option>
                                            	<option value="Vanuatu" data-alternative-spellings="VU">Vanuatu</option>
                                            	<option value="Venezuela" data-alternative-spellings="VE">Venezuela</option>
                                            	<option value="Vietnam" data-alternative-spellings="VN Việt Nam" data-relevancy-booster="1.5">Vietnam</option>
                                            	<option value="Virgin Islands, British" data-alternative-spellings="VG" data-relevancy-booster="0.5">Virgin Islands, British</option>
                                            	<option value="Virgin Islands, U.S." data-alternative-spellings="VI" data-relevancy-booster="0.5">Virgin Islands, U.S.</option>
                                            	<option value="Wallis and Futuna" data-alternative-spellings="WF" data-relevancy-booster="0.5">Wallis and Futuna</option>
                                            	<option value="Western Sahara" data-alternative-spellings="EH لصحراء الغربية">Western Sahara</option>
                                            	<option value="Yemen" data-alternative-spellings="YE اليمن">Yemen</option>
                                            	<option value="Zambia" data-alternative-spellings="ZM">Zambia</option>
                                            	<option value="Zimbabwe" data-alternative-spellings="ZW">Zimbabwe</option>
                                            </select>
                                        </li> 
                                        <li id="isolation_city">
                                            <label style="display: inline !important;">Isolation City </label>
                                            <img style="display: none;" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/images/ui-anim_basic_16x16.gif"/>
                                            <span class="GeoNames">Powered by <a href="http://www.geonames.org/" target="_blank">GeoNames.org</a></span>
                                            <input id="IsolationCity" name="IsolationCity" disabled="true" tabindex="16"
                                                   placeholder="Atlanta, Bath, Naples" type="text" 
                                                   value="<? echo (isset($summary->info['IsolationCity'])) ? $summary->info['IsolationCity'] : ''; ?>" />
                                            <!-- http://www.geonames.org -->
                                        </li>
                                        <li>
                                            <label>Isolation Region </label>
                                            <input id="IsolationRegion" name="IsolationRegion" disabled="true" tabindex="17"
                                                   placeholder="Georgia, South West England, Guangdong" type="text"
                                                   value="<? echo (isset($summary->info['IsolationRegion'])) ? $summary->info['IsolationRegion'] : ''; ?>"/>
                                            <input id="Longitude" name="Longitude" type="hidden" 
                                                   value="<? echo (isset($summary->info['Longitude'])) ? $summary->info['Longitude'] : ''; ?>"/>
                                            <input id="Latitude" name="Latitude" type="hidden" 
                                                   value="<? echo (isset($summary->info['Latitude'])) ? $summary->info['Latitude'] : ''; ?>"/>
                                        </li>   

                                    </ul>
                                </div>
                            </div>
                            <div class="large-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li id="host_name">
                                            <label style="display: inline !important;">Host Species Name </label>
                                            <img style="display: none;" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/images/ui-anim_basic_16x16.gif"/>
                                            <input name="HostName" id="HostName" type="text" tabindex="18" placeholder="Homo sapiens" 
                                                   value="<? echo (isset($summary->info['HostName'])) ? $summary->info['HostName'] : ''; ?>"/>
                                            <input name="HostTaxonID" id="HostTaxonID" type="hidden"
                                                   value="<? echo (isset($summary->info['HostTaxonID'])) ? $summary->info['HostTaxonID'] : ''; ?>"/>
                                        </li>
                                        <li>
                                            <label>Host Health </label>
                                            <select name="HostHealth" tabindex="22">
                                                <option value="<? echo (isset($summary->info['HostHealth'])) ? $summary->info['HostHealth'] : ''; ?>"
                                                       selected="selected"><? echo (isset($summary->info['HostHealth'])) ? $summary->info['HostHealth'] : 'Select One'; ?></option>
                                                <option value="Acute Infective Endocarditis">Acute Infective Endocarditis</option>
                                                <option value="Boils">Boils</option>
                                                <option value="Bacteremia">Bacteremia</option>
                                                <option value="Carbuncles">Carbuncles</option>
                                                <option value="Cellulitis">Cellulitis</option>
                                                <option value="Food Poisoning">Food Poisoning</option>
                                                <option value="Gastroenteritis">Gastroenteritis</option>
                                                <option value="Impetigo">Impetigo</option>
                                                <option value="Necrotizing pneumonia">Necrotizing pneumonia</option>
                                                <option value="Osteomyelitis">Osteomyelitis</option>
                                                <option value="Pneumonia">Pneumonia</option>
                                                <option value="Septic arthritis">Septic arthritis</option>
                                                <option value="Septicemia">Septicemia</option>
                                                <option value="Staphylococcal Scalded Skin Syndrome">Staphylococcal Scalded Skin Syndrome</option>
                                                <option value="Toxic Shock Syndrome">Toxic Shock Syndrome</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </li>
                                        <li>
                                            <label>Host Age </label>
                                            <div class="row">
                                                <div class="large-4 columns">
                                                    <div class="row">
                                                        <div class="small-5 columns">
                                                            <label class="right">&nbsp;&nbsp;Years</label>
                                                        </div>
                                                        <div class="small-7 columns">
                                                            <input type="number" name="HostAgeYears" min="0" max="150" 
                                                                   value="<? echo (isset($summary->info['HostAgeYears'])) ? $summary->info['HostAgeYears'] : '0'; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="large-4 columns">
                                                    <div class="row">
                                                        <div class="small-5 columns">
                                                            <label class="left">Months</label>
                                                        </div>
                                                        <div class="small-7 columns">
                                                            <input type="number" name="HostAgeMonths" min="0" max="12" 
                                                                   value="<? echo (isset($summary->info['HostAgeMonths'])) ? $summary->info['HostAgeMonths'] : '0'; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="large-4 columns">
                                                    <div class="row">
                                                        <div class="small-5 columns">
                                                            <label class="right">&nbsp;&nbsp;&nbsp;Days</label>
                                                        </div>
                                                        <div class="small-7 columns">
                                                            <input type="number" name="HostAgeDays" min="0" max="365"
                                                                   value="<? echo (isset($summary->info['HostAgeDays'])) ? $summary->info['HostAgeDays'] : '0'; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <? 
                                                $male = '';
                                                $female = '';
                                                $unk = 'CHECKED';
                                                if(isset($summary->info['HostAgeDays'])) {
                                                    if ($summary->info['HostAgeDays'] == 1) {
                                                        $male = '';
                                                        $female = 'CHECKED';
                                                        $unk = '';
                                                    } 
                                                    else if ($summary->info['HostAgeDays'] == 2) {
                                                        $male = 'CHECKED';
                                                        $female = '';
                                                        $unk = '';
                                                    }
                                                }
                                            
                                            ?>
                                            <label>Host Gender </label>
                                            <div class="row">
                                                <div class="large-4 columns">
                                                    <div class="row">
                                                        <div class="small-8 columns">
                                                            <label class="right">Female</label>
                                                        </div>
                                                        <div class="small-4 columns">
                                                            <input name="HostGender" type="radio" id="HostGender" 
                                                                   style="display:none;" value="1" <? echo $female;?> >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="large-3 columns">
                                                    <div class="row">
                                                        <div class="small-8 columns">
                                                            <label class="right">Male</label>
                                                        </div>
                                                        <div class="small-4 columns">
                                                            <input name="HostGender" type="radio" id="HostGender" 
                                                                   style="display:none;" value="2" <? echo $male;?>>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="large-5 columns">
                                                    <div class="row">
                                                        <div class="small-10 columns">
                                                            <label class="right">Not Specified</label>
                                                        </div>
                                                        <div class="small-2 columns">
                                                            <input name="HostGender" type="radio" id="HostGender" 
                                                                   style="display:none;" value="0" <? echo $unk;?>>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns comments">
                                <div class="panel no-border">
                                    <label>Comments </label>
                                    <textarea cols="10" rows="10" placeholder="Any comments about this sample you may have. (i.e. Patient acquired pathogen from sibling.)" 
                                              name="Comments"><? echo (isset($summary->info['Comments'])) ? $summary->info['Comments'] : ''; ?></textarea>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                    
                    <div class="row title-row">
                        <div class="large-12 large-centered columns title">
                            <hr/>
                            Phenotype Information
                            <hr/>
                        </div>
                    </div>
                    <fieldset class="phenotype selected">
                        <div class="row">
                            <div class="large-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Vancomycin-value">Vancomyocin: <span id='Vancomycin-suffix'></span></label>
                                                    <input type="hidden" id="Vancomycin-value" name='Vancomycin'>
                                
                                    
                                                <div id="Vancomycin-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Penicillin-value">Penicillin: <span id='Penicillin-suffix'></span></label>
                                                    <input type="hidden" id="Penicillin-value" name='Penicillin'>
                                
                                    
                                                <div id="Penicillin-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Oxacillin-value">Oxacillin: <span id='Oxacillin-suffix'></span></label>
                                                    <input type="hidden" id="Oxacillin-value" name='Oxacillin'>
                                
                                    
                                                <div id="Oxacillin-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Clindamycin-value">Clindamycin: <span id='Clindamycin-suffix'></span></label>
                                                    <input type="hidden" id="Clindamycin-value" name='Clindamycin'>
                                
                                    
                                                <div id="Clindamycin-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Daptomycin-value">Daptomycin: <span id='Daptomycin-suffix'></span></label>
                                                    <input type="hidden" id="Daptomycin-value" name='Daptomycin'>
                                
                                    
                                                <div id="Daptomycin-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>	
                                    </ul>
                                </div>
                            </div>
                            <div class="large-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>                                               
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Levofloxacin-value">Levofloxacin: <span id='Levofloxacin-suffix'></span></label>
                                                    <input type="hidden" id="Levofloxacin-value" name='Levofloxacin'>
                                
                                    
                                                <div id="Levofloxacin-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>                                                   
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Linezolid-value">Linezolid: <span id='Linezolid-suffix'></span></label>
                                                    <input type="hidden" id="Linezolid-value" name='Linezolid'>
                                
                                    
                                                <div id="Linezolid-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>  
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Rifampin-value">Rifampin: <span id='Rifampin-suffix'></span></label>
                                                    <input type="hidden" id="Rifampin-value" name='Rifampin'>
                                
                                    
                                                <div id="Rifampin-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Tetracycline-value">Tetracycline: <span id='Tetracycline-suffix'></span></label>
                                                    <input type="hidden" id="Tetracycline-value" name='Tetracycline'>
                                
                                    
                                                <div id="Tetracycline-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div style="width:404px;display:block;">
                                    
                                                    <label for="Trimethoprim-value">Trimethoprim-Sulfamethoxazole: <span id='Trimethoprim-suffix'></span></label>
                                                    <input type="hidden" id="Trimethoprim-value" name='Trimethoprim'>
                                
                                    
                                                <div id="Trimethoprim-slider" style="width:400;"></div>
                                                <div class="steps" style="font-size: 0.6em">
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>U</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>S</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>I</span>
                                                    <span style="display:inline-block;width: 20px;text-align:center;">|<br>R</span>
                                                    <span style="display:inline-block;width: 30px;text-align:center;">|<br>0</span>
                                                    <span style="display:inline-block;width: 57px;text-align:center;">|<br>5</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>10</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>15</span>
                                                    <span style="display:inline-block;width: 33px;text-align:center;">|<br>20</span>
                                                    <span style="display:inline-block;width: 55px;text-align:center;">|<br>25</span>
                                                    <span style="display:inline-block;width: 31px;text-align:center;">|<br>30</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mic">
                                            * U - Untested, S - Supseptible, I - Intermediate, R - Resistant
                                        </li>    
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-6 columns">
                                <p></p>
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <label>Source </label>
                                            <select name="Source">
                                                <option value="Blood">Blood</option>
                                            	<option value="Body Fluid">Body Fluid</option>
                                            	<option value="Respiratory">Respiratory</option>
                                            	<option value="Tissue">Tissue</option>
                                            	<option value="Urine">Urine</option>
                                            	<option value="Wound">Wound</option>
                                            	
                                                <option value="<? echo (isset($summary->info['Source'])) ? $summary->info['Source'] : 'Not Specified'; ?>"
                                                       selected="selected"><? echo (isset($summary->info['Source'])) ? $summary->info['Source'] : 'Not Specified'; ?></option>
                                
                                            	<option value="">--------------------</option>
                                            	<option value="Biopsy NOS">Biopsy NOS</option>
                                            	<option value="Blood for Respiratory">Blood for Respiratory</option>
                                            	<option value="Body Fluid NOS">Body Fluid NOS</option>
                                            	<option value="Cerebrospinal Fluid">Cerebrospinal Fluid</option>
                                            	<option value="Colonic Wash">Colonic Wash</option>
                                            	<option value="Conjunctiva">Conjunctiva</option>
                                            	<option value="Contact Lens">Contact Lens</option>
                                            	<option value="Cord Blood">Cord Blood</option>
                                            	<option value="Cornea">Cornea</option>
                                            	<option value="Cornea Donor Rim">Cornea Donor Rim</option>
                                            	<option value="Cryoprecipitate Product">Cryoprecipitate Product</option>
                                            	<option value="Cyst Fluid">Cyst Fluid</option>
                                            	<option value="Decubitus Ulcer">Decubitus Ulcer</option>
                                            	<option value="Drainage">Drainage</option>
                                            	<option value="Drainage NOS">Drainage NOS</option>
                                            	<option value="Duodenal Drainage">Duodenal Drainage</option>
                                            	<option value="Ear">Ear</option>
                                            	<option value="Endocervix">Endocervix</option>
                                            	<option value="Endotracheal Aspirate">Endotracheal Aspirate</option>
                                            	<option value="Environmental">Environmental</option>
                                            	<option value="Esophageal Brush">Esophageal Brush</option>
                                            	<option value="Eye">Eye</option>
                                            	<option value="Eyelid ">Eyelid </option>
                                            	<option value="Fecal Aspirate">Fecal Aspirate</option>
                                            	<option value="Feces">Feces</option>
                                            	<option value="Fine Needle Aspirate">Fine Needle Aspirate</option>
                                            	<option value="Fistula">Fistula</option>
                                            	<option value="Gastric Fluid">Gastric Fluid</option>
                                            	<option value="Graft">Graft</option>
                                            	<option value="Groin">Groin</option>
                                            	<option value="Hair">Hair</option>
                                            	<option value="Heart Valve">Heart Valve</option>
                                            	<option value="Heart Valve - Porcine">Heart Valve - Porcine</option>
                                            	<option value="Hepatobiliary Brushing">Hepatobiliary Brushing</option>
                                            	<option value="Human Progenitor Cells-Apheresis">Human Progenitor Cells-Apheresis</option>
                                            	<option value="Human Progenitor Cells-Cord">Human Progenitor Cells-Cord</option>
                                            	<option value="Human Progenitor Cells-Marrow">Human Progenitor Cells-Marrow</option>
                                            	<option value="Implanted Material">Implanted Material</option>
                                            	<option value="Incision">Incision</option>
                                            	<option value="Intrauterine Device">Intrauterine Device</option>
                                            	<option value="Introducer Sheath">Introducer Sheath</option>
                                            	<option value="Islet Cells">Islet Cells</option>
                                            	<option value="Jackson Pratt Drainage">Jackson Pratt Drainage</option>
                                            	<option value="Joint Fluid">Joint Fluid</option>
                                            	<option value="Lesion">Lesion</option>
                                            	<option value="Liver Biopsy">Liver Biopsy</option>
                                            	<option value="Lochia">Lochia</option>
                                            	<option value="Lung Aspirate">Lung Aspirate</option>
                                            	<option value="Lymph Node">Lymph Node</option>
                                            	<option value="Middle Ear Fluid">Middle Ear Fluid</option>
                                            	<option value="Mini Bronchoalveolar Lavage">Mini Bronchoalveolar Lavage</option>
                                            	<option value="Nail">Nail</option>
                                            	<option value="Nares">Nares</option>
                                            	<option value="Nasal Wash">Nasal Wash</option>
                                            	<option value="Nasopharyngeal">Nasopharyngeal</option>
                                            	<option value="Other Blood Bank Product">Other Blood Bank Product</option>
                                            	<option value="Pancreas Fluid">Pancreas Fluid</option>
                                            	<option value="Pelvic Fluid">Pelvic Fluid</option>
                                            	<option value="Penile Discharge">Penile Discharge</option>
                                            	<option value="Penis">Penis</option>
                                            	<option value="Penrose Drain">Penrose Drain</option>
                                            	<option value="Pericardial Fluid">Pericardial Fluid</option>
                                            	<option value="Perirectum">Perirectum</option>
                                            	<option value="Peritoneal Dialysis Fluid">Peritoneal Dialysis Fluid</option>
                                            	<option value="Peritoneal Fluid">Peritoneal Fluid</option>
                                            	<option value="Pharmacy">Pharmacy</option>
                                            	<option value="Pharmacy Cardioplegic Fluid">Pharmacy Cardioplegic Fluid</option>
                                            	<option value="PICC Line">PICC Line</option>
                                            	<option value="Pilonidal Cyst">Pilonidal Cyst</option>
                                            	<option value="Pinworm Preparation">Pinworm Preparation</option>
                                            	<option value="Plasma">Plasma</option>
                                            	<option value="Plasma Product">Plasma Product</option>
                                            	<option value="Platelet Product">Platelet Product</option>
                                            	<option value="Pleural Fluid">Pleural Fluid</option>
                                            	<option value="Products of Conception">Products of Conception</option>
                                            	<option value="Pustule">Pustule</option>
                                            	<option value="QC">QC</option>
                                            	<option value="Rectum">Rectum</option>
                                            	<option value="Red Blood Cell Product">Red Blood Cell Product</option>
                                            	<option value="Retroperitoneal">Retroperitoneal</option>
                                            	<option value="Semen">Semen</option>
                                            	<option value="Serum">Serum</option>
                                            	<option value="Sinus">Sinus</option>
                                            	<option value="Skin">Skin</option>
                                            	<option value="Spore Strip">Spore Strip</option>
                                            	<option value="Sputum">Sputum</option>
                                            	<option value="Sputum Induced">Sputum Induced</option>
                                            	<option value="Stem Cells NOS">Stem Cells NOS</option>
                                            	<option value="Suprapubic Bladder Aspirate">Suprapubic Bladder Aspirate</option>
                                            	<option value="Swan Ganz Catheter">Swan Ganz Catheter</option>
                                            	<option value="Synovial Fluid">Synovial Fluid</option>
                                            	<option value="Throat">Throat</option>
                                            	<option value="Tissue NOS">Tissue NOS</option>
                                            	<option value="Tracheostomy Site">Tracheostomy Site</option>
                                            	<option value="Triple Lumen Catheter Tip">Triple Lumen Catheter Tip</option>
                                            	<option value="Umbilicus">Umbilicus</option>
                                            	<option value="Urethra">Urethra</option>
                                            	<option value="Urine Cystoscopic">Urine Cystoscopic</option>
                                            	<option value="Urine Nephrostomy">Urine Nephrostomy</option>
                                            	<option value="Urine, Catherized">Urine, Catherized</option>
                                            	<option value="Urine, Clean Catch">Urine, Clean Catch</option>
                                            	<option value="Urine, Ileal Conduit">Urine, Ileal Conduit</option>
                                            	<option value="Uvula">Uvula</option>
                                            	<option value="Vaginal">Vaginal</option>
                                            	<option value="Vaginal-Rectal">Vaginal-Rectal</option>
                                            	<option value="Ventricular Fluid CSF">Ventricular Fluid CSF</option>
                                            	<option value="Vertebra">Vertebra</option>
                                            	<option value="Vesicle">Vesicle</option>
                                            	<option value="Vitreous Fluid">Vitreous Fluid</option>
                                            	<option value="Vulva">Vulva</option>
                                            	<option value="Water">Water</option>
                                            	<option value="Whole Blood">Whole Blood</option>
                                            </select>
                                        </li>	
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr/>
                    </fieldset>
        
                    <div class="row">
                        <div class="large-3 large-centered columns">
                            <div class="panel no-border">
                                <input id="submit" type="submit" value="Submit Changes">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

