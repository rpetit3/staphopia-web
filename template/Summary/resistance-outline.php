                <div class="resistance-outline info">
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <div class="panel no-border">
                                <p></p>
                                <h5 class="center">Predicted Resistance Phenotype</h5>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 small-centered columns predicted-row">
                            <? echo $resistance->phenotype;  ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <div class="panel no-border">
                                <p></p>
                                <h5 class="center">Significant Resistance Protein Hits</h5>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <table id="resSTable" class="proteinSTable">
                                <thead>
                                    <tr>
                                        <td>Gene</td>
                                        <td>Category</td>
                                        <td>Accession</td>
                                        <td>% Identity</td>
                                        <td>% Covered</td>
                                        <td>Bit Score</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? echo $resistance->proteins['strong']; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
