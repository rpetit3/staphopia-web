                <div class="virulence-outline info">
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <div class="panel no-border">
                                <p></p>
                                <h5 class="center">Predicted Virulence Phenotype</h5>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 small-centered columns predicted-row">
                            <? echo $virulence->phenotype;  ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <div class="panel no-border">
                                <p></p>
                                <h5 class="center">Significant Virulence Protein Hits</h5>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <table id="virSTable" class="proteinSTable">
                                <thead>
                                    <tr>
                                        <td>Gene</td>
                                        <td>Category</td>
                                        <td>Accession</td>
                                        <td>% identity</td>
                                        <td>% Covered</td>
                                        <td>Bit Score</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? echo $virulence->proteins['strong']; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
