                <div class="virulence-protein info">
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <table id="virTable" class="proteinTable">
                                <thead>
                                    <tr>
                                        <td>Gene</td>
                                        <td>Category</td>
                                        <td>Accession</td>
                                        <td>% identity</td>
                                        <td>% Covered</td>
                                        <td>Bit Score</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? echo $virulence->proteins['all']; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
