        <style>
            div[class*='summary-'] table {
                border: none !important;
            }
                div[class*='summary-'] tr {
                    background: white !important;
                }
                    div[class*='summary-'] td {
                        line-height: 0.8em;
                        font-size: 1em;
                    }
                    div[class*='summary-'] tr td:first-child {
                        text-align: right !important;
                    }
                    div[class*='summary-'] tr td:last-child {
                        text-align: left !important;
                    }
            a i {
                font-size: 24px;
            }
        </style>