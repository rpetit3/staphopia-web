            <div class="sccmec-protein info">
                <div class="row">
                    <div class="small-12 small-centered columns">
                        <table id="mecProtein" class="proteinTable">
                            <thead>
                                <tr>
                                    <td>Gene</td>
                                    <td>Accession</td>
                                    <td>% Identity</td>
                                    <td>% Covered</td>
                                </tr>
                            </thead>
                            <tbody>
                                <? echo $sccmec->proteins['all']; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
