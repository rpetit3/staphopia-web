            <div class="summary-phenotype info">
                <div class="row">
                    <div class="large-6 columns">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="width: 65%;">Source:</td>
                                    <td style="width: 35%;"><? echo (isset($summary->info['Source']) ? ($summary->info['Source'] == 'not_specified' ? '-' : $summary->info['Source']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Vancomycin:</td>
                                    <td><? echo (isset($summary->info['Vancomycin']) ? ($summary->info['Vancomycin'] == 'U' ? '-' : $summary->info['Vancomycin']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Penicillin:</td>
                                    <td><? echo (isset($summary->info['Penicillin']) ? ($summary->info['Penicillin'] == 'U' ? '-' : $summary->info['Penicillin']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Oxacillin:</td>
                                    <td><? echo (isset($summary->info['Oxacillin']) ? ($summary->info['Oxacillin'] == 'U' ? '-' : $summary->info['Oxacillin']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Clindamycin:</td>
                                    <td><? echo (isset($summary->info['Clindamycin']) ? ($summary->info['Clindamycin'] == 'U' ? '-' : $summary->info['Clindamycin']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Daptomycin:</td>
                                    <td><? echo (isset($summary->info['Daptomycin']) ? ($summary->info['Daptomycin'] == 'U' ? '-' : $summary->info['Daptomycin']) : '-'); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="large-6 columns">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="width: 65%;">&nbsp;</td>
                                    <td style="width: 35%;"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Levofloxacin:</td>
                                    <td><? echo (isset($summary->info['Levofloxacin']) ? ($summary->info['Levofloxacin'] == 'U' ? '-' : $summary->info['Levofloxacin']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Linezolid:</td>
                                    <td><? echo (isset($summary->info['Linezolid']) ? ($summary->info['Linezolid'] == 'U' ? '-' : $summary->info['Linezolid']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Rifampin:</td>
                                    <td><? echo (isset($summary->info['Rifampin']) ? ($summary->info['Rifampin'] == 'U' ? '-' : $summary->info['Rifampin']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Tetracycline:</td>
                                    <td><? echo (isset($summary->info['Tetracycline']) ? ($summary->info['Tetracycline'] == 'U' ? '-' : $summary->info['Tetracycline']) : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Trimethoprim-Sulfamethoxazole:</td>
                                    <td><? echo (isset($summary->info['Trimethoprim']) ? ($summary->info['Trimethoprim'] == 'U' ? '-' : $summary->info['Trimethoprim']) : '-'); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
