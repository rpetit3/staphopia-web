                <div class="resistance-protein info">
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <table id="resTable" class="proteinTable">
                                <thead>
                                    <tr>
                                        <td>Gene</td>
                                        <td>Category</td>
                                        <td>Accession</td>
                                        <td>% Identity</td>
                                        <td>% Covered</td>
                                        <td>Bit Score</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? echo $resistance->proteins['all']; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
