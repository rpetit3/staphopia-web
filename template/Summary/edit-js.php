        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <script src="/js/dev/submit.js"></script>
        <script src="/js/jquery.form.js"></script>
        <script src="/js/jquery.select-to-autocomplete.js"></script>
        <script>
            var s;
            $(document).ready(function () {
                s = new Submit();
                $('#country-selector').selectToAutocomplete();
                $("#country-selector > option").each(function() {
                    s.CODES[$(this).val()] = $(this).data('alternativeSpellings').substring(0,2);
                });
                s.Sliders();
                s.AutoComplete();
                s.HostName()
                
                $('#IsolationCountry').bind("change paste keyup", function() {
                    if ($('#IsolationCountry').val() != '-' && $('#IsolationCountry').val() != '') {
                        $('#IsolationCity').prop('disabled', false);
                        $('#IsolationRegion').prop('disabled', false);
                    } else {
                        $('#IsolationCity').prop('disabled', true);
                        $('#IsolationRegion').prop('disabled', true);
                    }
                });
                
                <? if(isset($summary->info['IsolationCountry'])) : ?>
                $('#IsolationCountry').val('United States');
                $('#IsolationCity').prop('disabled', false);
                $('#IsolationRegion').prop('disabled', false);
                <? endif; ?>
                
                <? if(isset($summary->info['Vancomycin'])) : ?>
                $('#Vancomycin-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Vancomycin'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Penicillin'])) : ?>
                $('#Penicillin-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Penicillin'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Clindamycin'])) : ?>
                $('#Clindamycin-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Clindamycin'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Daptomycin'])) : ?>
                $('#Daptomycin-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Daptomycin'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Levofloxacin'])) : ?>
                $('#Levofloxacin-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Levofloxacin'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Oxacillin'])) : ?>
                $('#Oxacillin-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Oxacillin'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Linezolid'])) : ?>
                $('#Linezolid-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Linezolid'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Rifampin'])) : ?>
                $('#Rifampin-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Rifampin'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Tetracycline'])) : ?>
                $('#Tetracycline-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Tetracycline'];?>'));
                <? endif; ?>
                
                <? if(isset($summary->info['Trimethoprim'])) : ?>
                $('#Trimethoprim-slider').slider('value', s.SetPhenotype('<? echo $summary->info['Trimethoprim'];?>'));
                <? endif; ?>
            });              
        </script>
        
        
        
        
        
        
        
        
        
        
        