            <div class="sccmec-primer info">
                <div class="row">
                    <div class="small-12 small-centered columns">
                        <table id="mecPrimers" class="proteinTable">
                            <thead>
                                <tr>
                                    <td>Gene</td>
                                    <td>Primer</td>
                                    <td>% Identity</td>
                                    <td>% Covered</td>
                                </tr>
                            </thead>
                            <tbody>
                                <? echo $sccmec->primers['all']; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
