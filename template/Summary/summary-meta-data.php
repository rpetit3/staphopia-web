<div class="summary-meta-data info">
                <div class="row">
                    <div class="large-6 columns">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="width: 47%;">Contact Name:</td>
                                    <td style="width: 53%;">
                                        <? 
                                            echo '  <a href="mailto:'. $summary->info['ContactEmail'] .'?Subject=Staphopia - '. $summary->seqname .'">
                                                        '. $summary->info['ContactEmail'] .'
                                                    </a>';
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Contact Link:</td>
                                    <td><? echo (isset($summary->info['ContactLink']) ? $summary->info['ContactLink'] : "-"); ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Sequencing Date:</td>
                                    <td><? echo (isset($summary->info['SequencingDate']) ? $summary->info['SequencingDate'] : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Sequencing Center:</td>
                                    <td>
                                        <? 
                                            if (isset($summary->info['SequencingCenterLink'])) {
                                                $seq_center = '<a href="'. $summary->info['SequencingCenterLink'] .'" title-"Open link in a new tab." 
                                                                target="_blank">'. $summary->info['SequencingCenter'] .'</a>';
                                            }
                                            else {
                                                $seq_center = $summary->info['SequencingCenter'];
                                            } 
                                            echo $seq_center;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Library Method:</td>
                                    <td><? echo (isset($summary->info['SequencingLibraryMethod']) ? $summary->info['SequencingLibraryMethod'] : '-') ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Funding Agency:</td>
                                    <td>
                                        <?  
                                            if (isset($summary->info['FundingAgencyLink'])) {
                                                $agency = '<a href="'. $summary->info['FundingAgencyLink'] .'" title="Open link in a new tab."
                                                            target="_blank">'. $summary->info['FundingAgency'] .'</a>';
                                            }
                                            else {
                                                $agency = (isset($summary->info['FundingAgency']) ? $summary->info['FundingAgency'] : '-');
                                            } 
                                            echo $agency;  
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Publication Link:</td>
                                    <td><? echo (isset($summary->info['isPublished']) ? $summary->info['isPublished'] : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="large-6 columns">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="width: 47%;">Strain:</td>
                                    <td style="width: 53%;"><? echo (isset($summary->info['Strain']) ? $summary->info['Strain'] : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Host Name:</td>
                                    <td>
                                        <? 
                                            if (isset($summary->info['HostTaxonID'])) {
                                                $taxon = '<a href="http://www.ncbi.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id='. $summary->info['HostTaxonID'] .'" 
                                                           title="Open NCBI Taxonomy entry in a new tab."
                                                           target="_blank">'. $summary->info['HostName'].'</a>';
                                            } 
                                            else {
                                                $taxon = (isset($summary->info['HostName']) ? $summary->info['HostName'] : '-');
                                            }
                                            echo $taxon;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Host Gender:</td>
                                    <td><? echo (isset($summary->info['HostAge']) ? $summary->info['HostAge'] : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Host Age:</td>
                                    <td><? echo (isset($summary->info['HostGender']) ? $summary->info['HostGender'] : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>Host Health:</td>
                                    <td><? echo (isset($summary->info['HostHealth']) ? $summary->info['HostHealth'] : '-'); ?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>SRA Experiment:</td>
                                    <td>
                                        <?  
                                            if (isset($summary->info['SRAExperiment'])) {
                                                echo '<a href="http://www.ncbi.nlm.nih.gov/sra/?term='. $summary->info['SRAExperiment'] .'" title="Open link in a new tab."
                                                            target="_blank">'. $summary->info['SRAExperiment'] .'</a>';
                                            }
                                            else {
                                                echo '-';
                                            } 
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SRA Study:</td>
                                    <td>
                                        <?  
                                            if (isset($summary->info['SRAExperiment'])) {
                                                echo '<a href="http://www.ncbi.nlm.nih.gov/sra/?term='. $summary->info['SRAStudy'] .'" title="Open link in a new tab."
                                                            target="_blank">'. $summary->info['SRAStudy'] .'</a>';
                                            }
                                            else {
                                                echo '-';
                                            } 
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SRA Sample:</td>
                                    <td>
                                        <?  
                                            if (isset($summary->info['SRAExperiment'])) {
                                                echo '<a href="http://www.ncbi.nlm.nih.gov/biosample/?term='. $summary->info['SRASample'] .'" title="Open link in a new tab."
                                                            target="_blank">'. $summary->info['SRASample'] .'</a>';
                                            }
                                            else {
                                                echo '-';
                                            } 
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="large-2 columns">
                        <div class="panel no-border">
                            Comments:
                        </div>
                    </div>
                    <div class="large-10 columns">
                        <div class="panel no-border">
                            <textarea disabled><? echo (isset($summary->info['Comments']) ? $summary->info['Comments'] : '-'); ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
