<?php
    /**
     * Staphopia server configuration
     * 
     * Use this to set some server wide constants.  Such as host name, ip address, pwd, etc...  Must be manually updated
     * for each server.
     * 
     * Author: Robert A. Petit III
     * Date: 3/22/2013
     */
     
    class ServerConfig {
        
        // Server Host Name
        public $hostname = 'peregrine.genetics.emory.edu';
        public $address = 'http://peregrine.genetics.emory.edu';
        
    }

?>
