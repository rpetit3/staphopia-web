<?php

    # Submitters and Links
    $list = array (
        'BCM' => array(  
            'name' => 'Baylor College of Medicine',  
            'link' => 'http://www.hgsc.bcm.tmc.edu' 
        ),
        'BI'  => array( 
            'name' => 'Broad Institute', 
            'link' => 'http://www.broadinstitute.org' 
        ),
        'Emory University' => array( 
            'name' => 'Emory University', 
            'link' => 'http://med.emory.edu/main/research/core_labs/gra_genome_center/index.html' 
        ),
        'FG13' => array( 
            'name' => 'FG13', 
            'link' => '' 
        ),
        'Geneva University Hospitals' => array( 
            'name' => 'Geneva University Hospitals', 
            'link' => 'http://www.unige.ch/international/index_en.html' 
        ),
        'Harbin Medical University' => array( 
            'name' => 'Harbin Medical University', 
            'link' => 'http://website.hrbmu.edu.cn/English/index.html' 
        ),
        'ICL-CFB' => array( 
            'name' => 'Imperial College London - Centre for Bioinformatics', 
            'link' => 'http://www.bioinformatics.imperial.ac.uk/'     
        ),
        'ILLUMINA' => array( 
            'name' => 'Illumina Cambridge Ltd.', 
            'link' => 'http://www.illumina.com' 
        ),
        'JCVI' => array( 
            'name' => 'JCVI', 
            'link' => 'http://www.jcvi.org' 
        ),
        'National Center for Biological Sciences,NCBS-TIFR' => array( 
            'name' => 'National Center for Biological Sciences-TIFR', 
            'link' => 'http://www.ncbs.res.in' 
        ),
        'NCGR' => array( 
            'name' => 'National Center for Genome Resources', 
            'link' => 'http://www.ncgr.org' 
        ),
        'NIAID-RML-RTS' => array( 
            'name' => 'NIAID Rocky Mountain Laboratories', 
            'link' => 'http://www.niaid.nih.gov/about/organization/dir/rml/Pages/default.aspx' 
        ),
        'Robert Koch Institut' => array( 
            'name' => 'Robert Koch Institut', 
            'link' => 'http://www.rki.de/EN/Home/homepage_node.html' 
        ),
        'SC' => array( 
            'name' => 'Wellcome Trust Sanger Institute', 
            'link' => 'http://www.sanger.ac.uk' 
        ),
        'Statens Serum Institut' => array( 
            'name' => 'Statens Serum Institut', 
            'link' => 'http://www.ssi.dk/English.aspx' 
        ),
        'STSI' => array( 
            'name' => 'Scripps Translational Science Institute', 
            'link' => 'http://www.stsiweb.org/index.php' 
        ),
        'TGEN' => array( 
            'name' => 'Translational Genomics Research Institute', 
            'link' => 'https://www.tgen.org' 
        ),
        'TGEN-NORTH' => array( 
            'name' => 'Translational Genomics Research Institute North', 
            'link' => 'https://www.tgen.org/research/tgen-north.aspx' 
        ),
        'The Methodist Hospital Research Institute' => array( 
            'name' => 'The Methodist Hospital Research Institute', 
            'link' => 'http://www.methodisthealth.com/tmhri.cfm?id=36171' 
        ),
        'The Roslin Institute' => array( 
            'name' => 'The Roslin Institute', 
            'link' => 'http://www.roslin.ed.ac.uk' 
        ),
        'UMIGS' => array( 
            'name' => 'University of Maryland IGS', 
            'link' => 'http://www.igs.umaryland.edu' 
        ),
        'University of Edinburgh' => array( 
            'name' => 'University of Edinburgh', 
            'link' => 'http://genepool.bio.ed.ac.uk' 
        ),
        'University of Melbourne' => array( 
            'name' => 'University of Melbourne', 
            'link' => 'http://agd.path.unimelb.edu.au' 
        ),
        'WUGSC' => array( 
            'name' => 'Washington University GSC', 
            'link' => 'http://genome.wustl.edu' 
        )
    );
    
    $SUMMARY = $argv[1];
    $SAMPLE_DIR = $argv[2];
    $handle = fopen($SUMMARY, "r");
    $i = 1;
    $j = 1;
    if ($handle) {
        while (($buffer = fgets($handle, 4096)) !== false) {
            $cols = preg_split("/\t/", $buffer);

            /* SRX076945    454 sequencing of SACIG1612	SRS212037	CIG1612	SRP007206	LS454	454 GS FLX Titanium	UMIGS	Aparna Pallavajjala	Pubmed=21909268
             * Columns:
             *  0 - Experiment Accession   5 - SequencingPlatformMake
             *  1 - Experiment Title       6 - SequencingPlatformModel           
             *  2 - Sample Accession       7 - SequencingCenter            
             *  3 - Sample Name            8 - ContactName    
             *  4 - Study Accessione       9 - PublicationStatus
             */
            $_POST = array(
                'ContactName' => 'SRA Public Experiment', 
                'ContactEmail' => 'public@staphopia.com', 
                'SRAExperiment' => $cols[0],
                'SRASample'=> $cols[2],
                'SRAStudy' => $cols[4],
                'Comments' => 'SRA experiment '. $cols[0] .' titled "'. $cols[1] .'" sequenced using '. $cols[6] .'.',
                'SequencingCenter' => $list[$cols[7]]['name'], 
                'SequencingCenterLink' => $list[$cols[7]]['link'], 
                'Public' => 1,
                'Strain' => $cols[3],
                'SequencingPlatformMake' => $cols[5],
                'SequencingPlatformModel' => $cols[6]
            );
            
            if ($_POST['SequencingPlatformMake'] == 'ILLUMINA') {
                
                $FASTQ = $SAMPLE_DIR .'/'. $cols[2] .'/'. $cols[0] .'.fastq.bz2';
                if (file_exists($FASTQ)) {
                    if (filesize($FASTQ)/1048576 > 40) {
                        $_FILES = array(
                            'FASTQ' => array(
                                'name' => $cols[0] .'.fastq.bz2',
                                'tmp_name' => $FASTQ
                            )
                        );
                        
                        require_once ('/var/www/staphopia/lib/HTML/Submit.php');
                        $submit = new Submit($_POST, $_FILES);
                        $submit->SubmitSequence();
                        
                        if ($submit->IsError) {
                            print $submit->ErrorMessage .'\n';
                        } else {
                            print $cols[0] .' sequence submitted'. "\n";
                        }
                    } else {
                        print 'ERROR: '. $FASTQ .' is less than 40 MB, skipping!'. "\n";   
                    }              
                } else {
                    print 'ERROR: Could not open '. $FASTQ .'! Please check it.'. "\n";   
                }
            } else {
                print 'ERROR: '. $cols[0] .' not from ILLUMINA, skipping!'. "\n";   
            }
        }
    } else {
        print 'Unable to open '. $SUMMARY .'! Please check it.';   
    }
        
?>
