<?php
    /**
     * Staphopia mail server configuration
     * 
     * Use this to configure Staphopia's mail server settings. All email is sent
     * from a single account. 
     * 
     * Parameters:
     * 'host' => host name of the mail server
     * 
     * 'port' => port for the mail server
     * 
     * 'username' => username for the mail server 
     * 
     * 'password' => password for the user
     * 
     * 
     * Author: Robert A. Petit III
     * Date: 3/22/2013
     */
     
    class MailConfig {
        
        public $mail = array(
            'host' => 'ssl://mail.staphopia.com',
            'port' => 465,
            'auth' => true,
            'username' => 'no-reply@staphopia.com',
            'password' => 'NoReplyMail2013!@#$%^&*()'
        );
        
    }

?>
