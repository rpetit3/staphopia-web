<?php
    /**
     * Staphopia database configuration
     * 
     * Use this to configure Staphopia's MySQL database connections. For better 
     * security there are separate users for SELECT, INSERT, and DELETE.  You 
     * may choose to change this setup.
     * 
     * Parameters:
     * 'host' => host name of the database server
     * 
     * 'port' => port for database connection
     * 
     * 'database' => database to be used
     * 
     * 'username' => username for the database 
     * 
     * 'password' => password for the user
     * 
     * 
     * Author: Robert A. Petit III
     * Date: 3/21/2013
     */
     
    class DatabaseConfig {

        public $db = array (
            'host' => 'staphopiadev.cus0ahi8gdoj.us-east-1.rds.amazonaws.com',
            'port' => 29466,
            'database' => 'staphopia',
            
            // Select user credentials
            'select' => array(
                'username' => 'DeepThought',
                'password' => 'da208f6115574ef429684fa388b04fc958da12c7d2bd2c4d'
            ),
            
            // Insert user credentials
            'insert' => array (
                'username' => 'Earth',
                'password' => '50328320fc2f121a80a2061b804fae0615379f82822fa22b'
            ),
            
            // Delete user credentials
            'delete' => array (
                'username' => 'Krikkit',
                'password' => '91d424e389e75d4be12787bc2814e971fa8f6239cdc0000c'
            ),
            
            // Update user credentials
            'update' => array (
                'username' => 'HeartOfGold',
                'password' => '8d485021b098e4ece1b6d8775102cfc425c816808a8960e0'
            ),
            
            // Transaction 
            'transaction' => array (
                'username' => 'Slartibartfast',
                'password' => 'f15b12d0ab5806659ff754bea7f6a6de77929dd2bf5fcc3a'
            ),
            
            // MySQL Dump
            'dump' => array (
                'username' => 'Whale',
                'password' => '938e494346d1a407fdbe579fb7d964395b5bab711fad3095'
            )
        );
        
    }

?>
