<?php
    /**
     * Staphopia's Admin functions
     * 
     * Use this to create useful functions for staphopia administrators. 
     * 
     * Author: Robert A. Petit III
     * Date: 4/11/2013
     */
    class Admin {
        
        private $db;
        private $login;
        private $UserName;
        private $EmailArray = array();
        
        public function __construct( ) {
            // Initiate the database class
            require_once ('/var/www/staphopia/lib/Common/Database.php');
            $this->db = new Database();
        }
        
        public function LockedUsers() {
            $rows = $this->db->Query('select', "SELECT UserName, UserID, FirstName, LastName, Email
                                                FROM User
                                                WHERE IsAdmin=FALSE AND IsLocked=TRUE
                                                ORDER BY UserName ASC", array());
            return $rows;         
        }
        
        public function NormalUsers() {
            $rows = $this->db->Query('select', "SELECT UserName, UserID, FirstName, LastName, Email
                                                FROM User
                                                WHERE IsAdmin=FALSE AND IsLocked=FALSE
                                                ORDER BY UserName ASC", array());
            return $rows;                              
        }
        
        public function Admins() {
            $rows = $this->db->Query('select', "SELECT UserName, UserID, FirstName, LastName, Email
                                                FROM User
                                                WHERE IsAdmin=True AND UserName <> 'rpetit'
                                                ORDER BY UserName ASC", array());
            return $rows;                              
        }
        
        public function AllAdmins() {
            $rows = $this->db->Query('select', "SELECT UserName, UserID, FirstName, LastName, Email
                                                FROM User
                                                WHERE IsAdmin=True
                                                ORDER BY UserName ASC", array());
            return $rows;                              
        }
        
        private function UserFunction( $UserName ) {
            require_once ('/var/www/staphopia/lib/HTML/Login.php');
            $this->login = new Login(False);
            
            // Get user's information
            $this->login->TestUserName( $UserName );
            $this->EmailArray = array(
                                    'Email' => $this->login->User['Email'],
                                    'FirstName' => $this->login->User['FirstName'],
                                    'LastName' => $this->login->User['LastName']
                                );
        }
        
        public function InviteUser( $a ) {
            require_once ('/var/www/staphopia/lib/HTML/Register.php');
            $register = new Register();
            
            if (!$register->EmailExists( $a['Email'] )) {
                $AccessCode = $register->CreateAccessCode();
                if ($register->EmailInvitation($a['Email'], $a['FirstName'], $a['LastName'], $AccessCode)) {
                    $_SESSION['ErrorMessage']  = 'Successfully invited '.$_POST['FirstName'].' ';
                    $_SESSION['ErrorMessage'] .= $_POST['LastName'].' to become a beta-tester.';
                } else {
                    $_SESSION['ErrorMessage'] = 'An error occured during the invitation process.';
                }
            } else {
                $_SESSION['ErrorMessage'] = '"'. $a['Email'] .'" is already in use by another user.';
            }
        }
        
        public function LockUser( $UserName ) {
            $this->UserFunction( $UserName );
            if ($this->login->LockAccount()) {
                $_SESSION['ErrorMessage'] .= 'Successfully locked '. $UserName .'. <br />';
            } else {
                $_SESSION['ErrorMessage'] .= 'Unable to lock '. $UserName .'. <br />';
            }
        }
        
        public function UnlockUser( $UserName ) {
            $this->UserFunction( $UserName );
            $CodeID = $this->login->GetUnlockCode();
            if ($this->login->UnlockAccount( $CodeID )) {
                $_SESSION['ErrorMessage'] .= 'Successfully unlocked '. $UserName .'. <br />';
            } else {
                $_SESSION['ErrorMessage'] .= 'Unable to unlock '. $UserName .'. <br />';
            }
        }
        
        public function DeleteAccount( $a ) {
            $this->UserFunction( $a['UserName'] );
            if ($_SESSION['UserName'] == "rpetit") {
                $rows = $this->db->Query('delete', "DELETE FROM User
                                                    WHERE UserName=? AND FirstName=? AND LastName=? AND 
                                                          Email=? AND UserID=? AND UserName <> 'rpetit'", 
                                          array($a['UserName'], $a['FirstName'], $a['LastName'], 
                                                $a['Email'], $a['UserID']));
                if ($rows == 1) {
                    require_once ('/var/www/staphopia/lib/Common/Email.php');
                    $email = new Email();
                    $email->DeletedAccount( $this->EmailArray );
                    $_SESSION['ErrorMessage'] .= 'Successfully deleted account '. $a['UserName'] .'. <br />';
                } else {
                    $this->login->EmailError("Error deleting account.\n");
                    $_SESSION['ErrorMessage'] .= 'Unable to delete account '. $a['UserName'] .'. <br />';
                }
            }
        }
        
        public function MakeAdmin( $UserName ) {
            $this->UserFunction( $UserName );
            $rows = $this->db->Query('update', "UPDATE User
                                                SET IsAdmin=TRUE
                                                WHERE UserID=? AND UserName <> 'rpetit'", 
                                      array($this->login->User['UserID']));
            if ($rows == 1) {
                require_once ('/var/www/staphopia/lib/Common/Email.php');
                $email = new Email();
                $email->NewAdmin( $this->EmailArray );
                $_SESSION['ErrorMessage'] .= 'Successfully granted admin status to '. $UserName .'. <br />';
                
            } else {
                $this->login->EmailError("Error giving account admin status.\n");
                $_SESSION['ErrorMessage'] .= 'Unable to grant admin status to '. $UserName .'. <br />';
            }
        }
            
        public function RemoveAdmin( $UserName ) {
            $this->UserFunction( $UserName );
            if ($_SESSION['UserName'] == "rpetit") {
                $rows = $this->db->Query('update', "UPDATE User
                                                    SET IsAdmin=FALSE
                                                    WHERE UserID=? AND UserName <> 'rpetit'", 
                                          array($this->login->User['UserID']));
                if ($rows == 1) {
                    require_once ('/var/www/staphopia/lib/Common/Email.php');
                    $email = new Email();
                    $email->RevokedAdmin( $this->EmailArray );
                    $_SESSION['ErrorMessage'] .= 'Successfully revoked admin status from '. $UserName .'. <br />';
                } else {
                    $this->login->EmailError("Error removing account admin status.\n");
                    $_SESSION['ErrorMessage'] .= 'Unable to revoke admin status from '. $UserName .'. <br />';
                }
            }
        }
    }
?>
