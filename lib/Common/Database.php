<?php
    /**
     * Staphopia's database functions
     * 
     * Use this to interact with Staphopia's database.  There are generic
     * connect, disconnect, select, insert, etc... functions
     * 
     * Author: Robert A. Petit III
     * Date: 3/22/2013
     */
     
    class Database {

        private $config;
        private $dsn = "mysql:";
        private $dbh = array (
            'select' => False,
            'insert' => False,
            'delete' => False,
            'update' => False,
            'transaction' => False,
            'dump' => False
        );

        
        public function __construct() {
            // Get the database configuration
            require_once ('/var/www/staphopia/config/database.php');
            $this->config = new DatabaseConfig();
            
            // Configure the dsn
            $this->dsn .= "host=". $this->config->db['host'] .";";
            $this->dsn .= "dbname=". $this->config->db['database'] .";";
            $this->dsn .= "port=". $this->config->db['port'];
        }
        
        private function Connect( $dbUser, $dbPass ) {
            try {
                return new PDO($this->dsn, $dbUser, $dbPass);
            } catch (PDOexception $e) {
                error_log($e->getMessage());
            }
        }
        
        /**
         * function Query( select|insert|delete|update, string, array )
         * 
         * Use this for single row Inserts, Updates, and Deletes
         * 
         * Parameter:
         *      $type => SQL type: 'select', 'insert', 'delete', or 'update'
         *      $sql => A SQL stament to execute. (i.e. "DELETE FROM mailList WHERE emailhash=?")
         *      $array => Array of the values to delete.
         * 
         * Return:
         *      Select: the resulting row/s of the SQL statment
         *      Insert, Delete, Update: A count of the affected rows.
         */
        public function Query( $type, $sql, $array, $object=False ) {

            if (array_key_exists($type, $this->dbh)) {
                
                $this->CheckConnection( $type );
                // Query the database
                try {
                    $sth = $this->dbh[$type]->prepare($sql);
                    $sth->execute($array);
                    
                } catch (PDOException $e) {
                    error_log($e->getMessage());
                }
                
                if ($type == 'select') {
                    if ($object) {
                        return $sth->fetchAll(PDO::FETCH_OBJ);
                    } else {
                        return $sth->fetchAll(PDO::FETCH_ASSOC);
                    }
                } else {
                    // Number of rows affected by Delete, Update, Insert
                    return $sth->rowCount();
                }
            } else {
                // Unknown type;
            }
        }
        
        public function BeginTransaction( $type ) {
            $this->CheckConnection( $type );
            $this->dbh[$type]->beginTransaction();
        }
        
        public function Commit( $type ) {
            $this->CheckConnection( $type );
            $this->dbh[$type]->commit();
        }
        
        public function RollBack( $type ) {
            $this->CheckConnection( $type );
            $this->dbh[$type]->rollback();
        }
        
        private function CheckConnection( $type ) {
            if (!$this->dbh[$type]) {
                $this->dbh[$type] = $this->Connect( $this->config->db[$type]['username'], $this->config->db[$type]['password']);
            }  
        }
        
        public function LastInsertID( $type ) {
            return $this->dbh[$type]->lastInsertId();
        }
    }
?>
