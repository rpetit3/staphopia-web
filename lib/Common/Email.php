<?php
    /**
     * Staphopia's email functions
     * 
     * Use this to send specific emails to users, such as when joining the mail list or submitting jobs.
     * 
     * NOTE: Email should have been validated and sanitized prior to usage.
     * 
     * Author: Robert A. Petit III
     * Date: 4/5/2013
     */
     
    class Email {
        private $from = 'Staphopia <no-reply@staphopia.com>';
        private $subject;
        private $to;
        private $body;
        private $server;
        
        public function __construct() {
            // Get the server configuration
            require_once ('/var/www/staphopia/config/server.php');
            $this->server = new ServerConfig();
            
            // Get the mail configuration
            require_once ('/var/www/staphopia/config/mail.php');
            $this->config = new MailConfig();
        }
        
        /**
         * function SendMail()
         * 
         * Requires that the recipient (to), subject, and body have been set.  Other wise no email is sent.
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        private function SendMail() {
            // Verify all parameters are set
            if (!empty($this->subject) && !empty($this->to) && !empty($this->body)) {
                require_once ('Mail.php');
                
                // Set Headers
                $headers = array (
                    'From' => $this->from,
                    'To' => $this->to,
                    'Subject' => $this->subject,
                    'Date' => date("D, d M Y H:i:s O")
                );
                
                // Prepare Email
                $smtp = Mail::factory('smtp', $this->config->mail);
                $mail = $smtp->send($this->to, $headers, $this->body);

                if (PEAR::isError($mail)) {
                    error_log("<p>" . $mail->getMessage() . "</p>");
                    return False;
                } else {
                    return True;
                }
            } else {
                return False;
            }
        }
        
        /**
         * function JoinMailList( email_address, md5(email_address) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function JoinMailList( $email, $hash ) {
            if (isset($email)) {
                $this->to = $email;
                $this->subject = "Welcome to Staphopia's Mail List";
                $this->body  = 'Thank you very much for your interest in Staphopia. Our goal is to keep' . "\n";
                $this->body .= 'you informed of updates and progress we are making with Staphopia.' . "\n";
                $this->body .= "\n";
                $this->body .= 'If you believe you have recieved this email in error, please go to the' . "\n";
                $this->body .= 'link below to remove your email from this list.' . "\n\n";
                $this->body .= 'https://'. $this->server->hostname .'/php-bin/removeEmail.php?q='. $hash . "\n\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function Invitation( array('Email', 'FirstName', 'LastName', 'AccessCode' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function Invitation( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Invitation To Join Staphopia!";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'We would like to invite you to join the beta-test team for Staphopia.  As a beta-tester' . "\n"; 
                $this->body .= 'you will gain access to our development server.  If you are interested, please use the' . "\n";
                $this->body .= 'link below to register an account.' . "\n";
                $this->body .= "\n";
                $this->body .= 'https://'. $this->server->hostname .'/register/'. $a['AccessCode'] . "\n";
                $this->body .= "\n";
                $this->body .= 'If you run into any problems, feel free to contact us at "beta-tester@staphopia.com".' . "\n";
                $this->body .= "\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function NewRegistrant( array('Email', 'FirstName', 'LastName', 'UserName' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function NewRegistrant( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Welcome to Staphopia!";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'Thank you very much for your interest in being a Staphopia Beta tester. Your account' . "\n";
                $this->body .= 'under the username "'. $a['UserName'] .'" has been registered.' . "\n";
                $this->body .= "\n";
                $this->body .= 'As a Staphopia Beta tester, please generously use the "Report Issue" link, to report' . "\n";
                $this->body .= 'any bugs you find, propose feature requests, or suggest enhancements.  Your feedback' . "\n";
                $this->body .= 'is very valuable to us.  If you need to get in contact with us, please use the email' . "\n";
                $this->body .= '"beta-tester@staphopia.com".' . "\n\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function NewAdmin( array('Email', 'FirstName', 'LastName' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function NewAdmin( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Staphopia Admin Privileges Granted!";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'You have been granted admin privileges for Staphopia. You should notice another' . "\n";
                $this->body .= 'option under your menu named "Admin Tools".  This includes functions to lock an' . "\n";
                $this->body .= 'account, unlock an account, invite new users, among other functions. You will ' . "\n";
                $this->body .= 'need to log out then back in to update your status.  Please do not not abuse ' . "\n";
                $this->body .= 'these functions.' . "\n\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        public function NewSubmission( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Staphopia Job Submitted";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'We have received your genome to be processed through our pipeline. Your genome' . "\n";
                $this->body .= 'has been put into the queue and will be processed as soon as possible.  You may' . "\n";
                $this->body .= 'use the link below to check on the status of your job.' . "\n\n";
                $this->body .= 'https://'. $this->server->hostname .'/status/'. $a['MD5sum'] . "\n\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        public function CompleteSubmission( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Staphopia Job Completed";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'We are done processing your genome. You can follow the link below to view a summary' . "\n";
                $this->body .= 'of the results.' . "\n\n";
                $this->body .= 'https://'. $this->server->hostname .'/summary/'. $a['SampleTag'] . "\n\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function RevokedAdmin( array('Email', 'FirstName', 'LastName' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function RevokedAdmin( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Staphopia Admin Privileges Revoked";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'Your admin privileges for Staphopia have been revoked.  You will no longer have access' . "\n";
                $this->body .= 'to Admin Tools.  If you feel this may be in error please feel free to contact us.' . "\n";
                $this->body .= "\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function DeletedAccount( array('Email', 'FirstName', 'LastName' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function DeletedAccount( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Staphopia Account Deleted";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'Your account with Staphopia has been deleted.  All associated data has been removed.' . "\n";
                $this->body .= 'This includes all data stored within the database and all submitted data.' . "\n";
                $this->body .= "\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function LockedAccount( array('Email', 'FirstName', 'LastName', 'UserName' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function LockedAccount( $a, $Code ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Your Staphopia account has been locked.";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'Many unsuccessful attempts were made to log into your account. The IP address' . "\n";
                $this->body .= 'which these attempts originated from has been logged.  As a safety precaution' . "\n";
                $this->body .= 'we have temporarily locked your account.' . "\n";
                $this->body .= "\n";
                $this->body .= 'The security of your account is of utmost importance to us. We ask you please' . "\n";
                $this->body .= 'use the link below to remove the lock on your account.' . "\n";
                $this->body .= "\n";
                $this->body .= 'https://'. $this->server->hostname .'/unlock/'. $Code . "\n";
                $this->body .= "\n";
                $this->body .= 'If you have any questions or concerns, do not hesitate to contact us. Please ' . "\n";
                $this->body .= 'use the email "beta-tester@staphopia.com".' . "\n";
                $this->body .= "\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function VerificationEmail( array('Email', 'FirstName', 'LastName', 'CodeID' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function VerificationEmail( $a) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Please verify your email address.";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'In order to change your email, we need you to use the link below to verify' . "\n";
                $this->body .= 'your email.  This link will only be valid for 15 minutes.' . "\n";
                $this->body .= "\n";
                $this->body .= 'https://'. $this->server->hostname .'/update/email/'. $a['CodeID'] . "\n";
                $this->body .= "\n";
                $this->body .= 'If you have any questions or concerns, do not hesitate to contact us. Please ' . "\n";
                $this->body .= 'use the email "beta-tester@staphopia.com".' . "\n";
                $this->body .= "\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function EmailChangeNotice( array('Email', 'FirstName', 'LastName' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function EmailChangeNotice( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Your Staphopia account email has changed.";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'The email associated with your Staphopia account was recently changed. You' . "\n";
                $this->body .= 'will no longer receive emails from Staphopia at this email address. If you' . "\n";
                $this->body .= 'did not make this change, contact an admin at "beta-tester@staphopia.com".' . "\n\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function EmailUpdateNotice( array('Email', 'FirstName', 'LastName' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function EmailUpdateNotice( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Updated your Staphopia account email.";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'We have successfully updated the email address associated with your Staphopia' . "\n";
                $this->body .= 'account.  From now on you will receive emails at this address.' . "\n";
                $this->body .= "\n";
                $this->body .= 'As always, if you have any questions or concerns, do not hesitate to contact' . "\n";
                $this->body .= 'us at "beta-tester@staphopia.com".' . "\n";
                $this->body .= "\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function PasswordReset( array('Email', 'FirstName', 'LastName', 'CodeID' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function PasswordReset( $a) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Staphopia Password Reset Request";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'A request to reset your password has been made.  In order to reset your password, ' . "\n";
                $this->body .= 'you will need to use the link below. This link will only be valid for 15 minutes.' . "\n";
                $this->body .= "\n";
                $this->body .= 'https://'. $this->server->hostname .'/reset/password/'. $a['CodeID'] . "\n";
                $this->body .= "\n";
                $this->body .= 'If you have any questions or concerns, do not hesitate to contact us. Please ' . "\n";
                $this->body .= 'use the email "beta-tester@staphopia.com".' . "\n";
                $this->body .= "\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function PasswordChangeNotice( array('Email', 'FirstName', 'LastName' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function PasswordChangeNotice( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Your Staphopia account password has changed.";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'The password associated with your Staphopia account was recently changed. If' . "\n";
                $this->body .= 'you did not make this change, please get in contact with an admin at ' . "\n";
                $this->body .= '"beta-tester@staphopia.com".' . "\n\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        public function ErrorLog( $a ) {
            if (isset($a['Email'])) {
                $this->to = 'Staphopia Error Message <'.$a['Email'].'>';
                $this->subject = $a['Subject'];
                $this->body  = $a['Body'];
                error_log(print_r($a,true));
                return $this->SendMail();
            } else {
                return False;
            }
        }
        
        /**
         * function GroupInvite( array('Email', 'FirstName', 'LastName', 'CodeID', 'GroupName', 'GroupOwner' ) )
         * 
         * Returns a boolean as to whether or not an email was sent (True) or not (False)
         */
        public function GroupInvite( $a ) {
            if (isset($a['Email'])) {
                $this->to = $a['FirstName'] .' '. $a['LastName'] ."<". $a['Email'] .">";
                $this->subject = "Invitation To Join Staphopia Group!";
                $this->body  = 'Dear '. $a['FirstName'] .' '. $a['LastName'] .",\n\n"; 
                $this->body .= 'You have been invited to join the group "'. $a['GroupName'] .'", created by '. "\n";
                $this->body .= $a['GroupOwner'] .'.  If interested, please use the link below to join the group.' . "\n";
                $this->body .= "\n";
                $this->body .= 'https://'. $this->server->hostname .'/groups/'. $a['CodeID'] .'/'. "\n";
                $this->body .= "\n";
                $this->body .= 'If you run into any problems, feel free to contact us at "beta-tester@staphopia.com".' . "\n";
                $this->body .= "\n";
                $this->body .= 'Thank you very much,' . "\n";
                $this->body .= 'The Staphopia Team' . "\n";

                return $this->SendMail();
            } else {
                return False;
            }
        }
    }
?>
