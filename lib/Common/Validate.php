<?php
    
    class Validate {
        
        // Email Validators
        public function SanitizeEmail( $email ) { 
            return filter_var($email, FILTER_SANITIZE_EMAIL);
        }
        
        public function IsValidEmail( $email ) {
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        }
        
        // Input Validators
        public function IsValidName( $name ) {
            return preg_match("/^[a-zA-ZàáâäãåąćęèéêëìíîïłńòóôöõøùúûüÿýżźñçčšžÀÁÂÄÃÅĄĆĘÈÉÊËÌÍÎÏŁŃÒÓÔÖÕØÙÚÛÜŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{1,64}$/u", $name);
        }
        
        public function IsValidUser( $user ) {
            return preg_match('/^[a-zA-Z][a-zA-Z0-9_.]{4,14}$/', $user); 
        }
        
        public function IsValidPassword( $password ) {
            return preg_match('/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $password);
        }
        
        public function IsValidCode( $code ) {
            return preg_match('/^[a-zA-Z0-9]{32}$/', $code);
        }
        
        public function IsDigit( $digit ) {
            return is_numeric($digit);
        }
        
        public function IsValidDate( $date ) {
            if (date('Y-m-d', strtotime($date)) == $date) {
                return true;
            } else {
                return false;   
            }
        }
        
        public function IsValidDOI( $doi ) {
            return preg_match('/\b(10[.][0-9]{3,}(?:[.][0-9]+)*\/(?:(?!["&\'<>])\S)+)\b/', $doi);   
        }
    }    
?>
