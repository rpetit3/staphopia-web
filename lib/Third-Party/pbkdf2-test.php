<?php

require('pbkdf2.php');

function assert_true($result, $msg)
{
    if($result === true)
        echo "PASS: [$msg]\n";
    else
        echo "FAIL: [$msg]\n";
}

$session = create_salt();
$token = create_salt();
$good_hash = create_hash($session, $token);


#$salt = create_salt();
#$good_hash = create_hash("foobar", $salt);
#assert_true(validate_password("foobar", $good_hash, $salt), "Correct password");
#assert_true(validate_password("foobar2", $good_hash, $salt) === false, "Wrong password");

#$salt = 'AFKjhaHwQb8rXQ/jAvzwFD9MnadxkKPKH0c3klh8RYwiIdtB96UFXVbvin1AGv8045B8rW6ot8NTyirTiZs2FvJva0rS/35Eu9kMwDrr1LcoXwYEJcqjIyRjMzgTvgLw';
#$good_hash = 'KjPBnPFxN/3swY0b3wrBpIfuKDKpuVqaswx0Wuq8h0VbTQULRC7anz0GqrwvjrUpSAYb/Y5PcwuDkuxTeWlc8SJwkp9bR6hIGj085dGG93q2dd62WH8T4BcXsOqhIvXT';
#echo "Example salt: $salt\n";
#echo "Example hash: $good_hash\n";
#assert_true(validate_password("foobar", $good_hash, $salt), "Correct password");

echo "Example Series ID: \n$session\n";
echo "Example Token: \n$token\n";
echo "Example Hash:\n$good_hash\n";
?>
