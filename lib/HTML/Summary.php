<?php
    /**
     * Staphopia's summary functions
     * 
     * Use this to gather the results associated with a given sequence name
     * and display the results using html/summary.php.
     * 
     * Author: Robert A. Petit III
     * Date: 3/29/2013
     */
    require ('/var/www/staphopia/lib/Staphopia/Filtered.php');
    require ('/var/www/staphopia/lib/Staphopia/Assembly.php');
    require ('/var/www/staphopia/lib/Staphopia/MLST.php');
    require ('/var/www/staphopia/lib/Staphopia/SCCmec.php');
    require ('/var/www/staphopia/lib/Staphopia/Resistance.php');
    require ('/var/www/staphopia/lib/Staphopia/Virulence.php');
    
    class Summary {
        
        public $info;
        public $mlst;
        public $image;
        public $owner;
        public $sccmec;
        public $seqname;
        public $program;
        public $filtered;
        public $assembly;
        public $virulence;
        public $resistance;
        public $submit_date;
        public $exists = False;
        public $fullView = False;
        public $accessions;
        public $categories;
        
        public $db;
        public $sample_id;
        private $user_id;
        
        public $GRADE = array (
            '1' => 'Gold', 
            '2' => 'Silver', 
            '3' => 'Bronze'
        );
        
        public function __construct( $seqname ) {
            
            require_once ('../lib/Common/Database.php');
            $this->db = new Database();
            
            $this->seqExists( $seqname );
            if ( $this->exists ) {
                $this->seqname = $seqname;
                $this->get_info();
                $this->get_programs();
                $this->get_accessions();
                $this->get_categories();
                $this->getVisibility();
                
                // Filtered Stats
                $this->filtered = new Filtered( $this->db, $this->sample_id );
                $this->set_grade($this->filtered->stats[0]['Coverage'], $this->filtered->stats[0]['Quality'],
                                 $this->filtered->stats[0]['MeanLength']);
            }
        }
        
        /**
         * function seqExists( string )
         * 
         * Parameter:
         *      $q => A seqname to test if it exists in the database or not.
         * 
         * Return:
         *      Returns a bool as to whether or not the seqname exists.
         */        
        private function seqExists( $q ) {
            $rows = $this->db->Query('select', "SELECT SampleID,SubmitDate,UserID
                                                FROM `Sample`
                                                WHERE SampleTag=?
                                                LIMIT 1", array($q));
            
            if ( isset($rows[0]['SampleID']) ) {
                $this->sample_id = $rows[0]['SampleID'];
                $this->submit_date = $rows[0]['SubmitDate'];
                $this->user_id = $rows[0]['UserID'];
                $this->get_username();
                $this->exists = True;
            }
            else {
                $this->exists = False;
            }
        }
        
        public function get_username() {
            $rows = $this->db->Query('select', "SELECT `UserName` 
                                                FROM `User` 
                                                WHERE UserID=?
                                                LIMIT 1", array( $this->user_id ));
            if (isset($rows[0]['UserName'])) {
                $this->owner = $rows[0]['UserName'];   
            }
        }
        
        /**
         * function getSummary( )
         *      
         * Description:
         *      Query the seqSummary table and put result into $this->info array.
         * 
         * Parameter:
         *      none
         * 
         */        
        private function getSummary() {
            $rows = $this->db->Query('select', "SELECT id, public, published, owner, submit_date, grade, resistance, 
                                                       rHits, virulence, vHits, st, cc, basecount, readcount, version,
                                                       minread, avgread, maxread, quality, cov, n50, total_contigs,
                                                       total_size, max_contig, min_contig, greater_500, greater_1k,
                                                       greater_10k, greater_100k, mean, median, gc, assembler
                                                FROM `seqSummary`
                                                WHERE seqname=?
                                                LIMIT 1", array($this->seqname));
                                                
            $this->info = $rows[0];
            $this->image = strtolower($this->GRADE[ $this->info['grade'] ]);
        }
        
        /**
         * function getInfo( )
         * 
         * Description:
         *      Add user submitted information into $this->info array.
         * 
         * Parameter:
         *      none
         */        
        private function get_info() {
            // Create seqInfoList
            $this->info = array();
            $rows = $this->db->Query('select', "SELECT Tag.Tag AS tag, Information.Value AS val
                                                FROM  `Information` 
                                                INNER JOIN Tag ON Information.TagID = Tag.TagID
                                                WHERE Information.SampleID=?", array($this->sample_id));

            foreach ( $rows as $row ) {
                $this->info[ $row['tag'] ] = $row['val'];
            }  
        }
        
        public function get_programs() {
            $this->program = array();
            $rows = $this->db->Query('select', "SELECT `ProgramID`, `Name`, `Version` 
                                                FROM `Program` 
                                                WHERE 1", array());
                                                
            foreach ( $rows as $row ) {
                $this->program[ $row['ProgramID'] ] = array(
                    'Name' => $row['Name'],
                    'Version' => $row['Version']
                );
            }
        }
        
        private function set_grade($coverage, $quality, $mean) {
            if ($mean >= 75) {
                if ($coverage >= 45 && $quality >= 30) {
                    $this->grade = 'gold';   
                } 
                else if ($coverage >= 20 && $quality >= 20) {
                    $this->grade = 'silver';
                } else {
                    $this->grade = 'bronze';   
                }
            } else {
                $this->grade = 'bronze';   
            }  
        }
        
        private function get_accessions() {
            $rows = $this->db->Query('select', "SELECT `AccessionID`, `Gene`, `Database`, `Accession`
                                                FROM `Accession` AS a
                                                INNER JOIN `Gene` AS g
                                                ON a.GeneID=g.GeneID
                                                INNER JOIN `Database` AS d
                                                ON a.DatabaseID=d.DatabaseID
                                                WHERE 1", array());
            foreach($rows as $row) {
                $this->accessions[ $row['AccessionID'] ] = array(
                    'Accession' => $row['Accession'],
                    'Database' => $row['Database'],
                    'Gene' => $row['Gene']
                );
            }
        }
        
        private function get_categories() {
            $rows = $this->db->Query('select', "SELECT `CategoryID`, `Name`
                                                FROM `Category`
                                                WHERE 1", array());
            foreach($rows as $row) {
                $this->categories[ $row['CategoryID'] ] = $row['Name'];
            }
        }
        
        
        /** 
         * function getVisibility( )
         * 
         * Description:
         *      Determine whether to display limited or full results
         * 
         * Parameter:
         *      none
         */        
        private function getVisibility() {
            $this->info['isPublic']    = ($this->info['Public'] == 1 ? 'Public' : 'Private');
            $this->info['isPublished'] = "";
            
            $rows = $this->db->Query('select', "SELECT `PubMedID` 
                                                FROM `Publication` 
                                                WHERE `SampleID`=?", array($this->sample_id));
            
            if (isset($rows[0]['PubMedID'])) {
                foreach ($rows as $row) {
                    $this->info['isPublished'] .= '<a href="http://www.ncbi.nlm.nih.gov/pubmed/'. $row['PubMedID'] .'" title="Open PubMed entry in a new tab." target=_blank>'. $row['PubMedID'] .'</a>, ';
                }
                $this->info['isPublished'] = preg_replace("/, $/", "", $this->info['isPublished']);
                $this->fullView = True;
            } else {
                $this->info['isPublished'] = 'Unpublished';
                $this->info['isPublic'] = 'Limited View';
            }
            
            // If owner or admin the data should be visible.
            if ($this->user_id == $_SESSION['UserID'] || $_SESSION['IsAdmin'] == 1) {
                $this->fullView = True;
            }
        }
    }

?>
