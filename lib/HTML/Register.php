<?php 

    class Register {
        
        private $db;
        private $FirstName;
        private $LastName;
        private $Email;
        private $UserName;
        private $UserTag = NULL;
        private $Password;
        private $Hash = NULL;
        private $Salt = NULL;
        private $AccessCode;
        
        public $HasError = False;
        public $ErrorMessage = '';
        
        public function __construct() {
            // Initiate the database class
            require_once ('/var/www/staphopia/lib/Common/Database.php');
            $this->db = new Database();
            
            require_once ('/var/www/staphopia/lib/Common/Validate.php');
            $this->v = new Validate();
        }
        
        public function ValidInputs( $a ) {
            
            if ($this->v->IsValidName( $a['FirstName'] )) {
                $this->FirstName = $a['FirstName'];
            } else {
                $this->ErrorMessage .= '"'. $a['FirstName'] .'" is an invalid first name. <br />';
            }
            
            if ($this->v->IsValidName( $a['LastName'] )) {
                $this->LastName = $a['LastName'];
            } else {
                $this->ErrorMessage .= '"'. $a['LastName'] .'" is an invalid last name. <br />';
            }
            
            $a['Email'] = $this->v->SanitizeEmail( $a['Email'] ); 
            if ($this->v->IsValidEmail( $a['Email'] )) {
                $this->Email = $a['Email'];
            } else {
                $this->ErrorMessage .= '"'. $a['Email']  .'" is an invalid email. <br />';
            }
            
            if ($this->v->IsValidUser( $a['UserName'] )) {
                $this->UserName = $a['UserName'];
            } else {
                $this->ErrorMessage .= '"'. $a['UserName'] .'" is an invalid user name. <br />';
            }
            
            if ($this->v->IsValidPassword( $a['Password'] )) {
                $this->Password = $a['Password'];
            } else {
                $this->ErrorMessage .= 'The given password does not meet security requirements. <br />';
            }
            
            if ($this->v->IsValidCode( $a['AccessCode'] )) {
                $this->AccessCode = $a['AccessCode'];
            } else {
                $this->ErrorMessage .= 'The given access code is not an acceptable code. <br />';
            }
            
            if (!empty($this->ErrorMessage)) {
                $this->HasError = True;
                return False;
            } else {
                return True;
            }
        }
        
        public function EmailExists( $Email ) {
            $rows = $this->db->Query('select', "SELECT Email 
                                                FROM User
                                                WHERE Email=? LIMIT 1", array($Email));
                                                
            if (isset($rows[0]['Email'])) {
                $this->HasError = True;
                $this->ErrorMessage .= 'The email "'. $Email .'" is already in use by another account. <br />';
                return True;
            } else {
                return False;
            }
        }
        
        public function UserExists( $UserName ) {
            $rows = $this->db->Query('select', "SELECT UserName 
                                                FROM User 
                                                WHERE UserName=? LIMIT 1", array($UserName));
                                                
            if (isset($rows[0]['UserName'])) {
                $this->HasError = True;
                $this->ErrorMessage .= 'The username "'. $UserName .'" has already been taken. <br />';
                return True;
            } else {
                return False;
            }
        }
        
        public function CheckAccessCode() {
            $rows = $this->db->Query('select', "SELECT CodeID, UserID
                                                FROM AccessCode 
                                                WHERE CodeID=? LIMIT 1", array($this->AccessCode));
                                                                               
            if (isset($rows[0]['CodeID'])) {
                // Code has been used.
                if (!empty($rows[0]['UserID'])) {
                    $this->HasError = True;
                    $this->ErrorMessage .= 'The given access code has already been used by another user. <br />';
                    return False;
                } else {
                    return True;
                }                
            } else {
                $this->HasError = True;
                $this->ErrorMessage .= 'The given access code is not an acceptable code. <br />';
                return False;
            }
        }
            
        public function AddNewUser() {
            $this->GenerateUserTag();
            $this->HashPassword();
            if (!empty($this->UserTag) && !empty($this->Hash) && !empty($this->Salt) && !$this->HasError) {
                $this->db->BeginTransaction( 'transaction' );
                $rows = $this->db->Query('transaction', "INSERT INTO User (UserName, UserTag, Password, Salt, FirstName, 
                                                                            LastName, Email, IsAdmin, IsLocked)
                                                         VALUES (?,?,?,?,?,?,?,FALSE,FALSE)", array( $this->UserName, $this->UserTag, 
                                                         $this->Hash, $this->Salt, $this->FirstName, $this->LastName,
                                                         $this->Email));
                $UserID = $this->db->LastInsertID( 'transaction' );
                                                      
                if (isset($UserID)) {
                    $rows = $this->db->Query('transaction', "UPDATE AccessCode
                                                             SET UserID=?
                                                             WHERE CodeID=?", array($UserID, $this->AccessCode));
                    if ($rows == 1) {
                        $this->db->Commit( 'transaction' );
                        $this->EmailRegistrant();
                        return True;
                    } else {
                        $this->db->RollBack( 'transaction' );
                        $this->HasError = True;
                        $this->ErrorMessage .= 'Unable to associate the access code, an admin has been notified. <br />';
                        return False;
                    }
                } else {
                    $this->db->RollBack( 'transaction' );
                    $this->HasError = True;
                    $this->ErrorMessage .= 'Unable to create user account, an admin has been notified. <br />';
                    return False;
                }
            } else {
                return False;
            }
        }
        
        private function GenerateUserTag() {
            $tag = '';
            $v = array_merge(range(0, 9), range('A', 'Z'), range('a', 'z'));
            $i = 0;
            if (strlen($this->UserName) == 5) {
                $tag = $this->UserName . $v[$i];
                $i++;
            } else {
                $tag = substr($this->UserName, 0, 6);
            }
            
            $tag = strtolower($tag);
            while (empty($this->UserTag)) {
                
                $rows = $this->db->Query('select', "SELECT UserTag
                                                    FROM User
                                                    WHERE UserTag=?
                                                    LIMIT 1", array( $tag ));
                
                if (isset($rows[0]['UserTag'])) {
                    $tag = substr($this->UserName, 0, 5) . $v[$i];
                    $i++;
                } else {
                    $this->UserTag = $tag;
                }
            }
        }
        
        private function HashPassword() {
            require_once ('/var/www/staphopia/lib/Third-Party/pbkdf2.php');
            $this->Salt = create_salt();
            $this->Hash = create_hash($this->Password, $this->Salt);
            
            if (strlen($this->Salt) != 128 || strlen($this->Hash) != 128) {
                $this->HasError = True;
                $this->ErrorMessage .= 'An error has occured during the hashing and salting of your password, an admin has been notified. <br />';
                return False;
            }
        }
        
        
        
        /*******************************************************************************************
         * function CreateAccessCode()
         * 
         * Create a random 32 character string and insert it into database.
         *******************************************************************************************/
        public function CreateAccessCode() {
            $CodeID = bin2hex(openssl_random_pseudo_bytes(16, $strong));
            $rows = $this->db->Query('insert', "INSERT INTO AccessCode (CodeID)
                                                VALUES (?)", array( $CodeID ));
                                               
            if ($rows == 1) {
                return $CodeID;
            } else {
                $this->EmailError("Error inviting user, unable to create new access code.\n");
            }
        }
        
        public function EmailInvitation( $Email, $FirstName, $LastName, $AccessCode ) {
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $email = new Email();
            return $email->Invitation(array( 
                        'Email' => $Email, 
                        'FirstName' => $FirstName, 
                        'LastName' => $LastName, 
                        'AccessCode' => $AccessCode 
                    ));
        }
        
        private function EmailRegistrant() {
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $email = new Email();
            return $email->NewRegistrant(array( 
                        'Email' => $this->Email, 
                        'FirstName' => $this->FirstName, 
                        'LastName' => $this->LastName, 
                        'UserName' => $this->UserName 
                    ));
        }
        
        public function EmailErrors( ) {
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $email = new Email();
            
            $body  = 'FirstName => ' . $this->FirstName . "\n";
            $body .= 'LastName => ' . $this->LastName . "\n";
            $body .= 'Email => ' . $this->Email . "\n";
            $body .= 'UserName => '. $this->UserName . "\n";
            $body .= 'AccessCode => '. $this->AccessCode ."\n\n";
            $body .= "Errors: \n" . preg_replace("/<br \/>/", "\n", $this->ErrorMessage);
            $array = array(
                'Email' => 'error@staphopia.com',
                'Subject' => 'Registration Related Error',
                'Body' => $body
            );
            
            $email->ErrorLog( $array );
        }
    }

?>
