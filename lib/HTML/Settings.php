<?php

    class Settings {
        private $db;
        private $email;
        
        public function __construct() {
            require_once ('/var/www/staphopia/lib/Common/Database.php');
            $this->db = new Database();
            
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $this->email = new Email();
        }
        
        public function ViewSessions() {
            $rows = $this->db->Query('select', "SELECT SessionToken, Location, INET_NTOA(IPv4) AS IPv4, CreateDate
                                                FROM UserSession
                                                WHERE UserID=?", array($_SESSION['UserID']));
            return $rows;
        }
        
        public function UpdateEmail( $Email ) {
            // Make sure email doesn't already exist
            $rows = $this->db->Query('select', "SELECT Email 
                                                FROM User
                                                WHERE Email=? LIMIT 1", array($Email));
            if (!isset($rows[0]['Email'])) {
                $this->db->BeginTransaction('transaction');
                $rows = $this->db->Query('transaction', "DELETE FROM UserCode
                                                         WHERE UserID=? AND IsEmailChange=TRUE", array( $_SESSION['UserID'] ));
                $CodeID = bin2hex(openssl_random_pseudo_bytes(16, $strong));
                $rows = $this->db->Query('transaction', "INSERT INTO UserCodes (UserID, Extra, CodeID, IsEmailChange)
                                                         VALUES (?,?,?,TRUE)", array( $_SESSION['UserID'], $Email, $CodeID));
                if ($rows == 1) {
                    $this->db->Commit('transaction');
                    $_SESSION['ErrorMessage'] = 'A verification email has been sent to "'. $Email .'".'; 
                    $this->email->VerificationEmail(  array(
                                                        'Email' => $Email,
                                                        'FirstName' => $_SESSION['FirstName'],
                                                        'LastName' => $_SESSION['LastName'],
                                                        'CodeID' => $CodeID
                                                    ));
                } else {
                    $this->db->RollBack('transaction');
                    $_SESSION['ErrorMessage'] = 'An error has occured, please try again.';
                }
            } else {
                $_SESSION['ErrorMessage'] = 'The email given is already in use by another acocunt.';
            }                                    
        }
        
        public function VerifyEmail( $CodeID ) {
            // Delete expired changes
            $rows = $this->db->Query('delete', "DELETE FROM UserCode
                                                WHERE CreateDate < (NOW() - INTERVAL 15 MINUTE) AND IsEmailChange=TRUE", array());
            $rows = $this->db->Query('select', "SELECT UserID, Extra
                                                FROM UserCode
                                                WHERE CodeID=? AND IsEmailChange=TRUE LIMIT 1", array($CodeID));
            if (isset($rows[0]['UserID'])) {
                $UserID = $rows[0]['UserID'];
                $Email = $rows[0]['Email'];
                $rows = $this->db->Query('update', "UPDATE User
                                                    SET Email=?
                                                    WHERE UserID=?", array($Email, $UserID));
                if ($rows == 1) {
                    $this->email->EmailChangeNotice( array(
                                                        'Email' => $_SESSION['Email'],
                                                        'FirstName' => $_SESSION['FirstName'],
                                                        'LastName' => $_SESSION['LastName']
                                                    ));
                    $_SESSION['Email'] = $Email;
                    $this->email->EmailUpdateNotice( array(
                                                        'Email' => $_SESSION['Email'],
                                                        'FirstName' => $_SESSION['FirstName'],
                                                        'LastName' => $_SESSION['LastName']
                                                    ));
                    $_SESSION['ErrorMessage'] = 'Successfully updated your email.';
                } else {
                    $_SESSION['ErrorMessage'] = 'An error occured, your email was not updated.';
                }
            } else {
                $_SESSION['ErrorMessage'] = 'Invalid or expired email request, your email was not changed.';
            }
                                                                                  
        }
        
        public function UpdatePassword( $Original, $NewPassword ) {
            $_SESSION['ErrorMessage'] = '';
            $rows = $this->db->Query('select', "SELECT Password, Salt
                                                FROM User
                                                WHERE UserID=? LIMIT 1", array( $_SESSION['UserID'] ));
            if (isset($rows[0]['Password'])) {
                require_once ('/var/www/staphopia/lib/Third-Party/pbkdf2.php');
                if (validate_password($Original, $rows[0]['Password'], $rows[0]['Salt'])) {
                    $Salt = create_salt();
                    $Hash = create_hash($NewPassword, $Salt);
                    
                    if (strlen($Salt) == 128 || strlen($Hash) == 128) {
                        $rows = $this->db->Query('update', "UPDATE User
                                                            SET Password=?, Salt=?
                                                            WHERE UserID=?", array($Hash, $Salt, $_SESSION['UserID']));
                        if ($rows == 1) {
                            $this->email->PasswordChangeNotice( array(
                                                                    'Email' => $_SESSION['Email'],
                                                                    'FirstName' => $_SESSION['FirstName'],
                                                                    'LastName' => $_SESSION['LastName']
                                                                ));
                            $_SESSION['ErrorMessage'] = 'Successfully updated your password.';
                        }
                    }
                } else {
                    $_SESSION['ErrorMessage'] = 'Incorrect password, your password was not updated.';
                }
            }
            
            if (empty($_SESSION['ErrorMessage'])) {
                $_SESSION['ErrorMessage'] = 'An error occured, your password was not updated.';
            }            
        }
        
        public function PasswordResetRequest( $Email ) {
            $rows = $this->db->Query('select', "SELECT UserID, Email, FirstName, LastName
                                                FROM User
                                                WHERE Email=? LIMIT 1", array($Email));
            if (isset($rows[0]['UserID'])) {
                $a = $rows[0];
                $this->db->BeginTransaction('transaction');
                $rows = $this->db->Query('transaction', "DELETE FROM UserCode
                                                         WHERE UserID=? AND IsPasswordReset=TRUE", array( $a['UserID'] ));
                $CodeID = bin2hex(openssl_random_pseudo_bytes(16, $strong));
                $rows = $this->db->Query('transaction', "INSERT INTO UserCode (UserID, CodeID, IsPasswordReset)
                                                         VALUES (?,?,TRUE)", array( $a['UserID'], $CodeID));
                if ($rows == 1) {
                    $this->db->Commit('transaction');
                    $_SESSION['ErrorMessage'] = 'A password reset email has been sent.'; 
                    $this->email->PasswordReset(  array(
                                                        'Email' => $a['Email'],
                                                        'FirstName' => $a['FirstName'],
                                                        'LastName' => $a['LastName'],
                                                        'CodeID' => $CodeID
                                                    ));
                } else {
                    $this->db->RollBack('transaction');
                    $_SESSION['ErrorMessage'] = 'An error has occured, please try again.';
                }
            } else {
                $_SESSION['ErrorMessage'] = 'The given email is not asscociated with any accounts.';
            }  
        }
        
        public function ResetPassword( $CodeID, $Password ) {
            $_SESSION['ErrorMessage'] = '';
            // Delete expired changes
            $rows = $this->db->Query('delete', "DELETE FROM UserCode
                                                WHERE CreateDate < (NOW() - INTERVAL 15 MINUTE) AND IsPasswordReset=TRUE", array());
            $rows = $this->db->Query('select', "SELECT UserID
                                                FROM UserCode
                                                WHERE CodeID=? AND IsPasswordReset=TRUE LIMIT 1", array( $CodeID ));
            if (isset($rows[0]['UserID'])) {
                $UserID = $rows[0]['UserID'];
                $rows = $this->db->Query('select', "SELECT Email, FirstName, LastName
                                                    FROM User
                                                    WHERE UserID=? LIMIT 1", array( $UserID ));
                if (isset($rows[0]['Email'])) {
                    $a = $rows[0];
                    require_once ('/var/www/staphopia/lib/Third-Party/pbkdf2.php');
                    $Salt = create_salt();
                    $Hash = create_hash($Password, $Salt);                        
                
                    if (strlen($Salt) == 128 || strlen($Hash) == 128) {
                        $rows = $this->db->Query('update', "UPDATE User
                                                            SET Password=?, Salt=?
                                                            WHERE UserID=?", array($Hash, $Salt, $UserID));
                        if ($rows == 1) {                             
                            $this->email->PasswordChangeNotice( array(
                                                                    'Email' => $a['Email'],
                                                                    'FirstName' => $a['FirstName'],
                                                                    'LastName' => $a['LastName']
                                                                ));
                            $_SESSION['ErrorMessage'] = 'Successfully reset your password.';
                            return True;
                        }
                    }
                }
            } else {
                $_SESSION['ErrorMessage'] = 'Invalid or expired password reset request, your password was not changed.';
                return False;
            }
            
            if (empty($_SESSION['ErrorMessage'])) {
                $_SESSION['ErrorMessage'] = 'An error occured, your password was not updated.';
                return False;
            }
        }
        
        public function GetTags($Empty) {
            $rows = $this->db->Query('select', "SELECT TagID, Tag
                                                FROM Tag
                                                WHERE 1", array());
            $Tags = array();
            if (isset($rows[0]['TagID'])) {
                foreach ($rows as $row) {
                    if ($Empty) {
                        $Tags[ $row['Tag'] ] = '';   
                    } else {
                        $Tags[ $row['Tag'] ] = $row['TagID'];   
                    }
                }
            }
            
            return $Tags;
        }
        
        public function InsertTag( $Tag ) {
            $rows = $this->db->Query('insert', "INSERT INTO Tag (Tag) 
                                                VALUES (?)", array( $Tag ));
            if ($rows == 1) {
                return $this->db->LastInsertID('insert');
            } else {
                $_SESSION['ErrorMessage'] = 'An error occured, your settings were not saved.';
                return 0;
            }
        }
        
        public function DeleteSetting($UserID, $TagID) {
            $rows = $this->db->Query('delete', "DELETE FROM UserSetting
                                                WHERE UserId=? AND TagID=?", array($UserID, $TagID));
            if ($rows == 1) {
                $_SESSION['ErrorMessage'] = 'Successfully saved your settings.';
                return True;   
            } else {
                $_SESSION['ErrorMessage'] = 'An error occured, your settings were not saved.';
                return False;   
            }
        }
        
        public function SaveSetting( $UserID, $TagID, $Value, $Insert) {
            $rows;
            if ($Insert) {
                $rows = $this->db->Query('insert', "INSERT INTO UserSetting (UserID, TagID, Value)
                                                    VALUES (?,?,?)", array($UserID, $TagID, $Value));
            } else {
                $rows = $this->db->Query('update', "UPDATE UserSetting
                                                    SET Value=?
                                                    WHERE UserID=? AND TagID=?", array($Value, $UserID, $TagID));
            }
            if ($rows == 1) {
                $_SESSION['ErrorMessage'] = 'Successfully saved your settings.';
                return True;
            } else {
                if ($Insert) {
                    $_SESSION['ErrorMessage'] = 'An error occured, your settings were not saved.';
                } else {
                 $_SESSION['ErrorMessage'] = 'An error occured, your settings were not saved.';   
                }
                return False;   
            }
        }
        
        public function GetSavedSettings( $UserID ) {
            $rows = $this->db->Query('select', "SELECT s.Value, t.Tag 
                                                FROM `UserSetting` AS s
                                                LEFT JOIN Tag AS t ON s.TagID=t.TagID
                                                WHERE UserID=?", array($UserID));
            $Tags = $this->GetTags(True);
            if (isset($rows[0]['Value'])) {
                foreach ($rows as $row) {
                    $Tags[ $row['Tag'] ] = $row['Value'];      
                }
            }
            
            return $Tags;
        }
    }
?>