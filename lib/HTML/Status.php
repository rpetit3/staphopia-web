<?php

    class Status {
        public $complete = False;
        public $begun;
        public $valid;
        public $exists;
        public $title;
        public $job;
        public $link;
        public $state = array(
            'Valid' => 'foundicon-minus',
            'Filter' => 'foundicon-minus',
            'Assembly' => 'foundicon-minus',
            'MLST' => 'foundicon-minus',
            'SCCmec' => 'foundicon-minus',
            'Resistance' => 'foundicon-minus',
            'Virulence' => 'foundicon-minus'
        );
        
        private $db;
        private $email;
        private $user_id;
        private $sample_id;
        private $sample_tag;
        private $IsComplete;
        
        public function __construct($md5sum) {
            require_once ('/var/www/staphopia/lib/Common/Database.php');
            $this->db = new Database();
            
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $this->email = new Email();
            
            return $this->GetStatus($md5sum);
            
        }
        
        private function GetStatus($md5sum) {
            $rows = $this->db->Query('select', "SELECT JobID, UserID, MD5sum, Queued, Post, Begun, Valid, 
                                                       Filter, Assembly, MLST, SCCmec, Resistance, Virulence 
                                                FROM Job 
                                                WHERE MD5sum=?
                                                LIMIT 1", array($md5sum));
            if (isset($rows[0]['JobID'])) {
                if (strtotime($rows[0]['Queued']) > strtotime($rows[0]['Begun'])) {
                    $this->begun = False;
                } else {
                    $this->begun = True;   
                    if ($rows[0]['Valid'] == 3) {
                        $this->valid = True;   
                        $sample = $this->db->Query('select', "SELECT SampleTag, SampleID, IsFinished
                                                              FROM Sample
                                                              WHERE MD5sum=?
                                                              LIMIT 1", array($md5sum));
                        $this->title = $sample[0]['SampleTag'] . ' Status';
                        $this->link =  '<a href="http://dev.staphopia.com/summary/'. $sample[0]['SampleTag'] .'" 
                                           title="View summary of '.$sample[0]['SampleTag'].'.">'. $sample[0]['SampleTag'] .'</a>';
                        $this->sample_tag = $sample[0]['SampleTag'];
                        $this->sample_id = $sample[0]['SampleID'];
                        $this->IsComplete = ord($sample[0]['IsFinished']);
                        $this->user_id = $rows[0]['UserID'];
                        
                    } else {
                        $this->state['Valid'] = 'foundicon-error';
                        $this->valid = False;  
                        $this->title = 'Job '. $rows[0]['JobID'] .' Status';
                    }
                }

                $this->job = $rows[0];
                $this->JobStatus();
                $this->exists = True;

            } else {
                $this->exists = False;  
            }
        }
        
        private function JobStatus($job, $state) {

            $status = array( 
                '0' => 'foundicon-minus', // Default (Waiting)
                '1' => 'foundicon-clock', // Queued
                '2' => 'foundicon-refresh', // Running
                '3' => 'foundicon-checkmark', // Complete
                '8' => 'foundicon-error', // Error
                '9' => 'foundicon-remove'  // Unknown
            );
            
            $jobs = array( 'Valid', 'Filter', 'Assembly', 'MLST', 'SCCmec', 'Resistance', 'Virulence');
            $completed = 0;
            foreach ($jobs as $job) {
                $this->state[ $job ] = $status[ $this->job[ $job ] ];
                if ($this->job[ $job ] == 3) {
                    $completed++;   
                }
            }

            if ($this->job[ 'Valid' ] > 1) {
                $this->state['EC2'] = $status[ '3' ];
            } else {
                $this->state['EC2'] = $status[ '2' ];
            }
            
            if ($completed == 7) {
                $this->complete = True;
                if (!$this->IsComplete) {    
                    $this->db->Query('update', "UPDATE `Sample` 
                                                SET `IsFinished`=TRUE 
                                                WHERE `SampleID`=?", array($this->sample_id));
                    $user = $this->db->Query('select', "SELECT `FirstName`, `LastName`, `Email` 
                                                        FROM `User` 
                                                        WHERE `UserID`=?", array($this->user_id));
                    if (isset($user[0]['Email'])) {
                        $this->email->CompleteSubmission(array(
                            'Email' => $user[0]['Email'],
                            'FirstName' => $user[0]['FirstName'],
                            'LastName' => $user[0]['LastName'],
                            'SampleTag' => $this->sample_tag
                        ));
                    }
                }
            }
            
        }
        
    }
?>