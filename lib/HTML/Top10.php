<?php
    /**
     * 
     * 
     * 
     * 
     * 
     */
    
    class Top10 {
        
        private $db;
        private $links = array(
            'Baylor College of Medicine' => 'http://www.hgsc.bcm.tmc.edu',
            'Broad Institute' => 'http://www.broadinstitute.org',
            'Emory University' => 'http://med.emory.edu/main/research/core_labs/gra_genome_center/index.html',
            //'FG13' => '' 
            'Geneva University Hospitals' => 'http://www.unige.ch/international/index_en.html',
            'Imperial College London - Centre for Bioinformatics' => 'http://www.bioinformatics.imperial.ac.uk/',
            'Illumina Cambridge Ltd.' => 'http://www.illumina.com',
            'JCVI' => 'http://www.jcvi.org',
            'National Center for Biological Sciences-TIFR' => 'http://www.ncbs.res.in',
            'National Center for Genome Resources' => 'http://www.ncgr.org',
            'NIAID Rocky Mountain Laboratories' => 'http://www.niaid.nih.gov/about/organization/dir/rml/Pages/default.aspx',
            'Wellcome Trust Sanger Institute' => 'http://www.sanger.ac.uk',
            'Statens Serum Institut' => 'http://www.ssi.dk/English.aspx',
            'Scripps Translational Science Institute' => 'http://www.stsiweb.org/index.php',
            'Translational Genomics Research Institute' => 'https://www.tgen.org',
            'The Roslin Institute' => 'http://www.roslin.ed.ac.uk',
            'University of Maryland IGS' => 'http://www.igs.umaryland.edu',
            'University of Edinburgh' => 'http://genepool.bio.ed.ac.uk',
            'University of Melbourne' => 'http://agd.path.unimelb.edu.au',
            'Washington University GSC' => 'http://genome.wustl.edu' 
        );
        
        
        
        public function __construct() {
            // Initiate the database class
            require_once ('../lib/Common/Database.php');
            $this->db = new Database();
        }
        
        public function getTop10( $table, $type, $col1, $col2 ) {
            $rows = $this->db->Query('select', "SELECT $type, n FROM $table LIMIT 10", array());
            
            if ($rows) {
                $l = '';
                foreach ($rows as $row) {
                    if (isset($this->links[ $row[ $type ] ])) {
                        $l .= ' <tr><td class="right-align"><a href="'.$this->links[ $row[ $type ] ].'" title="'. $row[ $type ] .'" target="_blank">'. $row[ $type ]  .'</a></td>';

                    } 
                    else {
                        $l .= '<tr><td>'. $row[ $type ] .'</td>';
                    }
                    
                    $l .= '<td>'. $row["n"] .'</td></tr>';
                }

                $list = '<table class="center">
                            <thead>
                                <tr>
                                    <th>'. $col1 .'</th>
                                    <th>'. $col2 .'</th>
                                </tr>
                            </thead>
                            <tbody>
                                '. $l .'
                            </tbody>    
                        </table>';

                return $list;
            } else {
                return False;
            }
        }
    }

?>
