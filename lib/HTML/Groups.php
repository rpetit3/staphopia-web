<?php

    class Groups {
        private $db;
        public $Info;
        public $IsOwner = False;
        public $CanEdit = False;
        public $CanInvite = False;
        public $HasGroups = False;
        public $HasRemovables = False;
        public $ErrorMessage = '';
        public $groups = array();
        
        
        public function __construct( ) {

            require_once ('/var/www/staphopia/lib/Common/Database.php');
            $this->db = new Database();
        }
        
        /*******************************************************************************************
         * function CreateGroup( String )
         * 
         * Create a new group.
         *******************************************************************************************/
        public function CreateGroup( $UserID, $GroupName, $GroupMessage ) {
            $this->db->BeginTransaction( 'transaction' );
            $rows = $this->db->Query('transaction', "INSERT INTO `Group`(`OwnerID`, `Name`, `Title`) 
                                                VALUES (?, ?, ?)", array($UserID, $GroupName, $GroupMessage));
                                                
            $GroupID = $this->db->LastInsertID( 'transaction' );
            if (isset($GroupID)) {
                $rows = $this->db->Query('transaction', "INSERT INTO `UserToGroup`(`UserID`, `GroupID`, `Permission`) 
                                                         VALUES (?,?,?)", array($UserID, $GroupID, '3'));
                if ($rows == 1) {
                    $this->db->Commit( 'transaction' );
                    //$this->EmailOwner();
                    return True;
                } else {
                    $_SESSION['ErrorMessage'] = 'An error occured, the group was not created.  Please try again.';
                    $this->db->RollBack( 'transaction' );
                    return False;
                }
            } else {
                $_SESSION['ErrorMessage'] = 'An error occured, unable to create the group.  Please try again.'; 
                $this->db->RollBack( 'transaction' );
                return False;
            }
        }
        
        /*******************************************************************************************
         * function DeleteGroup( GroupID )
         * 
         * Delete a group, only the owner/creator can do so.
         *******************************************************************************************/
        public function DeleteGroup( $GroupID ) {
            $Name = $this->GroupName( $GroupID );
            $rows = $this->db->Query('delete', "DELETE FROM `Group` 
                                                WHERE `GroupID`=?", array( $GroupID ));
            if ($rows == 1) {
                $_SESSION['ErrorMessage'] = 'Successfully removed the group "'. $Name .'".';
            } else {
                $_SESSION['ErrorMessage'] = 'An error occured, unable to remove group at the moment.  Please try again.';
            }
        }
        
        /*******************************************************************************************
         * function InviteUser( array( Int, String, String, Email )
         * 
         * Invite a user to a group.
         *******************************************************************************************/
        public function InviteUser( $a ) {
            // Create a random 32 character string and insert it into database.
            $CodeID = bin2hex(openssl_random_pseudo_bytes(16, $strong));
            $rows = $this->db->Query('insert', "INSERT INTO `GroupInvite`(`GroupID`, `CodeID`) 
                                                VALUES (?,?)", array( $a['GroupID'], $CodeID));
            
            if ($rows == 1) {
                $success = $this->EmailInvite($a, $CodeID);
                if ($success) {
                    $_SESSION['ErrorMessage']  = 'Successfully invited '.$a['FirstName'].' '. $a['LastName'];
                    $_SESSION['ErrorMessage'] .= ' to join your group.';
                } else {
                    $_SESSION['ErrorMessage'] = 'An error occured while emailing an invitation.';
                }
            } else {
                $_SESSION['ErrorMessage'] = 'An error occured during the invitation process.';
            }

        }
        
        public function JoinGroup( $UserID, $CodeID ) {
            $rows = $this->db->Query('select', "SELECT GroupID
                                                FROM GroupInvite
                                                WHERE CodeID=?
                                                LIMIT 1", array( $CodeID ));
            if (isset($rows[0]['GroupID'])) {
                $GroupID = $rows[0]['GroupID'];
                $this->db->BeginTransaction( 'transaction' );
                $rows = $this->db->Query('transaction', "INSERT INTO `UserToGroup`(`UserID`, `GroupID`, `Permission`) 
                                                         VALUES (?,?,?)", array($UserID, $GroupID, '0'));
                if ($rows == 1) {
                    $rows = $this->db->Query('transaction', "DELETE FROM GroupInvite
                                                             WHERE CodeID=?", array( $CodeID ));
                    if ($rows == 1) {
                        $this->db->Commit( 'transaction' );
                        $_SESSION['ErrorMessage'] = 'Successfully joined the group "'. $this->GroupName( $GroupID ) .'".';
                        return True;
                    } else {
                        $_SESSION['ErrorMessage'] = 'An error occured, unable to join at the moment.  Please try again.';
                        $this->db->RollBack( 'transaction' );
                        return False;
                    }
                } else {
                    $_SESSION['ErrorMessage'] = 'An error occured, unable to join at the moment.  Please try again.';
                    $this->db->RollBack( 'transaction' );
                    return False;
                }
            } else {
                $_SESSION['ErrorMessage'] = 'An invalid code was used, or has already been used. Please request another invite.';  
            }
        }


        /*******************************************************************************************
         * function RemoveUser( Int, Int )
         * 
         * Remove a user from a group.
         *******************************************************************************************/
        public function RemoveUser( $GroupID, $UserID ) {
            $rows = $this->db->Query('delete', "DELETE FROM `UserToGroup` 
                                                WHERE `UserID`=? AND `GroupID`=?", array($UserID, $GroupID));
            if ($rows == 1) {
                $_SESSION['ErrorMessage'] = 'Successfully removed user from "'. $this->GroupName( $GroupID ) .'".';
            } else {
                $_SESSION['ErrorMessage'] = 'An error occured, unable to remove user at the moment.  Please try again.';
            }
        }
        
        public function TestRemovables( $Permission, $GroupID ) {
            $rows = $this->db->Query('select', "SELECT count(UserID) AS c
                                                FROM UserToGroup
                                                WHERE GroupID=? AND Permission < ?
                                                LIMIT 1", array( $GroupID, $Permission ));
            if ($rows[0]['c'] > 0) {
                $this->HasRemovables = True;
            }
        }
        
        /*******************************************************************************************
         * function ChangeUserStatus( UserID )
         * 
         * Change a users permission level within a group.
         *******************************************************************************************/
        public function ChangeUserStatus( $Permission, $UserID, $GroupID) {
            $rows = $this->db->Query('update', "UPDATE `UserToGroup` 
                                                SET `Permission`=? 
                                                WHERE `UserID`=? AND `GroupID`=?", array($Permission, $UserID, $GroupID));
            if ($rows == 1) {
                $_SESSION['ErrorMessage'] = 'Successfully change user permission from "'. $this->GroupName( $GroupID ) .'".';
            } else {
                $_SESSION['ErrorMessage'] = 'An error occured, unable to change user permission at the moment.  Please try again.';
            }
        }        
        
                
        /*******************************************************************************************
         * function UserGroups( Int )
         * 
         * Discover which groups a user belongs to and the permission level.
         *******************************************************************************************/
        public function UserGroups( $UserID ) {
            $rows = $this->db->Query('select', "SELECT u.GroupID, u.Permission, g.Name, g.Title
                                                FROM `UserToGroup` AS u
                                                LEFT JOIN `Group` AS g
                                                ON u.GroupID=g.GroupID
                                                WHERE `UserID`=?
                                                ORDER BY u.Permission DESC", array($UserID));
            if (isset($rows[0]['GroupID'])) {
                $this->HasGroups = True;
                foreach ($rows as $row) {
                    if ($row['Permission'] >= 2) {
                        $this->CanInvite = True;   
                        if ($rows[0]['Permission'] == 3) {
                            $this->IsOwner = True;  
                        }
                    }
                    elseif ($row['Permission'] == 1) {
                        $this->CanEdit = True;   
                    }
                    array_push($this->groups, $row);   
                }
            }
        }
        
        public function UserGroup( $UserID, $GroupID ) {
            $rows = $this->db->Query('select', "SELECT Permission
                                                FROM `UserToGroup` 
                                                WHERE `UserID`=? AND `GroupID`=?
                                                LIMIT 1", array( $UserID, $GroupID ));
            if (isset($rows[0]['Permission'])) {
                $this->HasGroups = True;
                if ($rows[0]['Permission'] >= 2) {
                    $this->CanInvite = True;   
                    if ($rows[0]['Permission'] == 3) {
                        $this->IsOwner = True;  
                    }
                }
                elseif ($row[0]['Permission'] == 1) {
                    $this->CanEdit = True;   
                }
                
                return $rows[0]['Permission'];
            }
        }
        
        public function GroupInfo( $GroupID, $UserID ) {
            // Test if user is appart of the group
            $rows = $this->db->Query('select', "SELECT UserID
                                                FROM UserToGroup
                                                WHERE GroupID=? AND UserID=?
                                                LIMIT 1", array ($GroupID, $UserID));
                                                
            if (isset($rows[0]['UserID'])) {
                $rows = $this->db->Query('select', "SELECT Name, Title
                                                    FROM Group
                                                    WHERE GroupID=?
                                                    LIMIT 1", array( $GroupID ));
                if (isset($rows[0]['Name'])) {
                    $this->Info = $rows[0];
                    
                    $rows = $this->db->Query('select', "SELECT u.UserID, u.FirstName, u.LastName, u.UserName, g.Permission
                                                        FROM UserToGroup AS g
                                                        LEFT JOIN User AS u
                                                        ON u.UserID=g.UserID
                                                        WHERE g.GroupID=?
                                                        ORDER BY g.Permission DESC", array( $GroupID ));
                    if (isset($rows[0]['UserName'])) {
                        $this->Info['Members'] = $rows;
                    }
                }
            } else {
                $_SESSION['ErrorMessage'] = 'You must be a member of the group to use that command.';  
            }
        }
        
        public function GroupName( $GroupID ) {
            $rows = $this->db->Query('select', "SELECT Name
                                                FROM Groups
                                                WHERE GroupID=?
                                                LIMIT 1", array($GroupID));
            return $rows[0]['Name'];
        }
        
        private function GroupOwner( $GroupID ) {
            $rows = $this->db->Query('select', "SELECT FirstName, LastName
                                                FROM Group AS g
                                                LEFT JOIN User AS u
                                                ON u.UserID=g.OwnerID
                                                WHERE GroupID=?
                                                LIMIT 1", array($GroupID));
                                                
            return $rows[0]['FirstName'] .' '. $rows[0]['LastName'];
        }
        
        public function GroupOwnerID ( $GroupID ) {
            $rows = $this->db->Query('select', "SELECT OwnerID
                                                FROM Group
                                                WHERE GroupID=?
                                                LIMIT 1", array($GroupID));
                                                
            return $rows[0]['OwnerID'];
        }
        
        public function GenomeCount( $UserID ) {
            $rows = $this->db->Query('select', "SELECT count(UserID) AS c
                                                FROM Sample
                                                WHERE UserID=? AND IsFinished=TRUE
                                                LIMIT 1", array ( $UserID ));
                                                
            return $rows[0]['c'];
        }
        
        public function EmailInvite( $a, $CodeID) {
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $email = new Email();
            return $email->GroupInvite( array( 
                        'Email' => $a['Email'], 
                        'FirstName' => $a['FirstName'], 
                        'LastName' => $a['LastName'], 
                        'CodeID' => $CodeID,
                        'GroupName' => $this->GroupName( $a['GroupID'] ),
                        'GroupOwner' => $this->GroupOwner( $a['GroupID'] )
                    ));
        }
    }
?>