<?php
    class Edit {
        
        public $IsError = False;
        public $ErrorMessage = '';
        public $history = array ();
        
        private $info = array();
        private $tag = array();
        private $SampleID;
        private $SampleTag;
        private $UserID; 
        private $_POST;
        private $Required = array (
            'SequencingCenter' => 'Sequencing Center', 
            'ContactName' => 'Contact Name', 
            'ContactEmail' => 'Contact Email'
        );
        
        private $Validate = array (
            'ContactEmail' => 'Contact Email',
            'SequencingDate' => 'Sequencing Date',
            'IsolationDate' => 'Isolation Date',
            'HostTaxonID' => 'Host Taxon ID',
            'PubMedID' => 'PubMed ID',
            'DigitalObjectIdentifier' => 'Digital Object Identifier',
        );
                
        public function __construct( $SampleTag, $UserID, $POST ) {    
            require_once ('/var/www/staphopia/lib/Common/Database.php');
            $this->db = new Database();
            
            $this->SampleTag = $SampleTag;
            $this->UserID = $UserID;
            $this->_POST = $POST;
        }
        
        public function CheckRequired() {
            foreach ($this->Required as $Tag => $Name) {
                if (empty($_POST[ $Tag ])) {
                    $this->ErrorMessage .= "Please complete required field '$Name'.<br />"; 
                    $this->IsError = True;
                }
            }
            
            return !$this->IsError;
        }
        
        public function Validate() {
            require_once ('/var/www/staphopia/lib/Common/Validate.php');
            $v = new Validate();
            foreach ($this->Validate as $Tag => $Name) {
                if (!empty($this->_POST[ $Tag ])) {
                    $IsValid = True;
                    if (preg_match('/Email$/', $Tag)) {
                        $IsValid = $v->IsValidEmail($this->_POST[ $Tag ]);
                    } 
                    else if (preg_match('/Date$/', $Tag)) {
                        $IsValid = $v->IsValidDate($this->_POST[ $Tag ]);
                    } 
                    else if (preg_match('/ID$/', $Tag)) { 
                        $IsValid = $v->IsDigit($this->_POST[ $Tag ]);
                    }
                    else if ($Tag == 'DigitalObjectIdentifier') {
                        $IsValid = $v->IsValidDOI($this->_POST[ $Tag ]);
                    }
                    
                    if (!$IsValid) {
                        $this->ErrorMessage .= "'". $this->_POST[ $Tag ] ."' is not a valid format for '$Name'.<br />"; 
                        $this->IsError = True;
                    }
                }
            }
            
            return !$this->IsError;
        }
        
        
        public function SubmitChanges() {
            $this->get_sampleid();
            $this->get_info();
            $this->get_tags();
            
            if ($this->compare_info()) {
                $this->ErrorMessage = 'Successfully saved your changes. <br />';   
            }
            
        }
        
        public function GetHistory() {
            $this->get_sampleid();
            $this->get_tags();
            
            $rows = $this->db->Query('select', "SELECT `SampleID`, u.`UserName`, `Tag`, `Date`, `From`, `To` 
                                                FROM `InformationHistory` AS i 
                                                LEFT JOIN `User` AS u
                                                ON u.`UserID`=i.`UserID`
                                                LEFT JOIN `Tag` AS t
                                                ON t.`TagID`=i.`TagID`
                                                WHERE i.`SampleID`=?
                                                ORDER BY `Date` DESC", array($this->SampleID));
                                                
            $this->history = $rows;
        }
        
        private function get_info() {
            $rows = $this->db->Query('select', "SELECT Tag.Tag AS tag, Information.Value AS val
                                                FROM  `Information` 
                                                INNER JOIN Tag ON Information.TagID = Tag.TagID
                                                WHERE Information.SampleID=?", array($this->SampleID));

            foreach ( $rows as $row ) {
                $this->info[ $row['tag'] ] = $row['val'];
            }  
        }
        
        private function get_tags() {
            $rows = $this->db->Query('select', "SELECT `TagID`, `Tag` FROM `Tag` WHERE 1", array());
            
            foreach ($rows as $row) {
                $this->tag[ $row['Tag'] ] = $row['TagID'];   
            }
        }
        
        public function compare_info() {
            $success = True;
            foreach ($this->_POST as $key => $value) {
                if (isset($this->info[ $key ])) {
                    // Existed
                    if ($value != $this->info[ $key ]) {
                        
                        if (empty($value)) {
                            // Its was emotied, so delete it  
                            $q = $this->update_info($this->info[ $key ], $value, $this->tag[ $key ], 2);
                            if (!$q) {
                                $success = False;   
                            }
                        } else {
                            // Its different lets update it  
                            $q = $this->update_info($this->info[ $key ], $value, $this->tag[ $key ], 1);
                            if (!$q) {
                                $success = False;   
                            }
                        }
                    }
                } else {
                    // DNE --> 'Did Not Exist'
                    if (!empty($value)) {
                        if (!preg_match('/^Untested$|^Not Specified$/', $value)) {
                           $q = $this->update_info('DNE', $value, $this->tag[ $key ], 0);
                            if (!$q) {
                                $success = False;   
                            }
                        }
                    }
                }
            }
            
            return $success;
        }
        
        public function update_info($old, $new, $tag_id, $existed) {
            // Add to InformationHistory
            $this->db->BeginTransaction( 'transaction' );
            $rows = $this->db->Query('transaction', "INSERT INTO `InformationHistory`(`SampleID`, `UserID`, `TagID`, `From`, `To`) 
                                                VALUES (?,?,?,?,?)", array($this->SampleID, $this->UserID, $tag_id, $old, $new));
            $HistoryID = $this->db->LastInsertID( 'transaction' );
            if (isset($HistoryID)) {
                $rows = False;
                if ($existed == 1) {
                    $rows = $this->db->Query('transaction', "UPDATE `Information` 
                                                             SET `Value`=? 
                                                             WHERE `SampleID`=? AND `TagID`=?", array($new, $this->SampleID, $tag_id));
                } else if ($existed == 2) {
                    $rows = $this->db->Query('transaction', "DELETE FROM `Information` 
                                                             WHERE `SampleID`=? AND `TagID`=?", array($this->SampleID, $tag_id));
                } else {
                    $rows = $this->db->Query('transaction', "INSERT INTO `Information`(`SampleID`, `TagID`, `Value`) 
                                                             VALUES (?,?,?)", array($this->SampleID, $tag_id, $new));
                }
                
                if ($rows == 1) {
                    $this->db->Commit( 'transaction' );
                    return True;
                } else {
                    $this->db->RollBack( 'transaction' );
                    $this->ErrorMessage = 'An error occured, some changes were not saved.  An admin has been notified. <br />';
                    return False;
                }
                
            } else {
                $this->db->RollBack( 'transaction' );
                $this->ErrorMessage = 'An error occured, your changes were note saved. An admin has been notified. <br />';
                return False;
            }
            
        }
        
        public function get_sampleid( ){
            $rows = $this->db->Query('select', "SELECT SampleID
                                                FROM  `Sample` 
                                                WHERE SampleTag=?
                                                LIMIT 1", array($this->SampleTag));
            if ($rows[0]['SampleID']) {
                $this->SampleID = $rows[0]['SampleID'];
                return True;
            } else {
                $this->ErrorMessage .= 'An error occured, your changes were note saved. An admin has been notified. <br />';
                return False;
            }
        }
    }
?>