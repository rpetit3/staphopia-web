<?php
    class Submit {
        
        public $IsError = False;
        public $ErrorMessage = '';
        
        private $_POST;
        public $_FILES;
        private $Format;
        private $Required = array (
            'ProjectStatus' => 'Project Status', 
            'SequencingStatus' => 'Sequencing Status', 
            'SequencingCenter' => 'Sequencing Center', 
            'ContactName' => 'Contact Name', 
            'ContactEmail' => 'Contact Email',
            'FASTQ' => 'FASTQ'
        );
        
        private $Validate = array (
            'ContactEmail' => 'Contact Email',
            'SequencingDate' => 'Sequencing Date',
            'IsolationDate' => 'Isolation Date',
            'HostTaxonID' => 'Host Taxon ID',
            'PubMedID' => 'PubMed ID',
            'DigitalObjectIdentifier' => 'Digital Object Identifier',
            'FASTQ' => 'FASTQ'
        );
        
        private $MIME = array (
            'application/x-bzip2' => 'bzip2',
            'application/x-bzip' => 'bzip',
            'application/zip' => 'zip',
            'application/x-gzip' => 'gzip'
        );
        
        public function __construct( $POST, $FILES ) {        
            $this->_POST = $POST;
            $this->_FILES = $FILES;
        }
        
        public function CheckRequired() {
            foreach ($this->Required as $Tag => $Name) {
                if ($Tag == 'FASTQ') {
                    if (empty($_FILES)) {
                        $this->ErrorMessage .= "You must give a FASTQ file containing your sequence.<br />";
                        $this->IsError = True;
                    }
                }
                else if (empty($_POST[ $Tag ])) {
                    $this->ErrorMessage .= "Please complete required field '$Name'.<br />"; 
                    $this->IsError = True;
                }
            }
            
            return !$this->IsError;
        }
        
        public function Validate() {
            require_once ('/var/www/staphopia/lib/Common/Validate.php');
            $v = new Validate();

            foreach ($this->Validate as $Tag => $Name) {
                if ($Tag == 'FASTQ') {
                    $MIME = trim($this->FileType($this->_FILES[ 'FASTQ' ]['tmp_name']));
                    if (!array_key_exists($MIME, $this->MIME)){
                        $this->ErrorMessage .= "'". $MIME."' is not an accepted file format, must be Zip (.zip), Gzip (.gz), or Bzip2 (bz2).<br />"; 
                        $this->IsError = True;
                    } else {
                        $this->Format = $this->MIME[ $MIME ];
                    }
                }
                else if (!empty($this->_POST[ $Tag ])) {
                    $IsValid = True;
                    if (preg_match('/Email$/', $Tag)) {
                        $IsValid = $v->IsValidEmail($this->_POST[ $Tag ]);
                    } 
                    else if (preg_match('/Date$/', $Tag)) {
                        $IsValid = $v->IsValidDate($this->_POST[ $Tag ]);
                    } 
                    else if (preg_match('/ID$/', $Tag)) { 
                        $IsValid = $v->IsDigit($this->_POST[ $Tag ]);
                    }
                    else if ($Tag == 'DigitalObjectIdentifier') {
                        $IsValid = $v->IsValidDOI($this->_POST[ $Tag ]);
                    }
                    
                    if (!$IsValid) {
                        $this->ErrorMessage .= "'". $this->_POST[ $Tag ] ."' is not a valid format for '$Name'.<br />"; 
                        $this->IsError = True;
                    }
                }
            }
            
            return !$this->IsError;
        }
        
        private function FileType( $file ) {
            return shell_exec('file -b --mime-type '. escapeshellcmd($file));
        }
        
        public function SubmitSequence() {
            require_once ('/var/www/staphopia/lib/Common/Database.php');
            $this->db = new Database();
            
            // MD5sum the input
            $a = preg_split("/\s+/", shell_exec("md5sum ".escapeshellarg($this->_FILES[ 'FASTQ' ]['tmp_name'])));
            $this->MD5sum = $a[0];
            $PWD = "/staphopia-ebs/tmp/". $this->MD5sum;
            shell_exec("mkdir -p $PWD/logs/sge");
            shell_exec("mkdir -p $PWD/scripts");
            
            // Copy the file (PHP will destroy the TMP file)
            $FILE = $PWD ."/". $this->_FILES[ 'FASTQ' ]['name'];
            shell_exec("cp ". $this->_FILES[ 'FASTQ' ]['tmp_name'] ." $FILE");
            
            // Allow mass submit script to work for SRA data
            $user_id = isset($_SESSION['UserID']) ? $_SESSION['UserID'] : '0'; // 0:public
            
            // Add job to database
            $rows = $this->db->Query('insert', "INSERT INTO Job (UserID, MD5sum, Post, Valid)
                                                VALUES (?,?,?,?)", 
                                                array($user_id, $this->MD5sum, json_encode($this->_POST), '1'));
            if ($rows == 1) {
                $id = $this->db->LastInsertID( 'insert' );
                $this->QueueJob( $PWD, $id, $FILE );
                
                if (isset($_SESSION['UserID'])) {
                    $this->EmailSubmitter();
                }
            } else {
                $this->ErrorMessage .= "An error has occured, an administrator has been notified.<br />"; 
                $this->IsError = True;
            }
            
            return !$this->IsError;
        }
        
        private function QueueJob($PWD, $id, $FILE) {
            $template = fopen("$PWD/scripts/00_validate.sh", 'w');
            fwrite($template, "#!/bin/bash\n");
            fwrite($template, "### Change to the current working directory:\n");
            fwrite($template, "#\$ -wd ". $PWD ."\n");
            fwrite($template, "### Job name:\n");
            fwrite($template, "#\$ -V\n");
            fwrite($template, "#\$ -N j". $id ."_0\n");
            fwrite($template, "#\$ -S /bin/bash\n");
            fwrite($template, "#\$ -pe orte 1\n");
            fwrite($template, "#\$ -o ". $PWD ."/logs/sge/validate.out\n");
            fwrite($template, "#\$ -e ". $PWD ."/logs/sge/validate.err\n");
            fwrite($template, "export PYTHONPATH=/staphopia-ebs/staphopia-pipeline/lib/python/:\$PYTHONPATH\n");
            fwrite($template, "\n");
            fwrite($template, "python /staphopia-ebs/staphopia-pipeline/bin/validate_fastq.py --out $PWD --fastq $FILE --jobid $id \n");
            fclose($template);
            
            // queue job
            shell_exec("qsub $PWD/scripts/00_validate.sh");
        }
        
       private function EmailSubmitter() {
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $email = new Email();
            return $email->NewSubmission(array( 
                        'Email' => $_SESSION['Email'], 
                        'FirstName' => $_SESSION['FirstName'], 
                        'LastName' => $_SESSION['LastName'], 
                        'MD5sum' => $this->MD5sum
                    ));
        }
    }
?>