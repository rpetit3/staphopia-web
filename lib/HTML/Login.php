<?php

    class Login {
        private $db;
        private $Input;
        private $RememberMe = False;
        private $SessionToken;
        private $isValid = False;
        public $IsLocked = False;
        public $ErrorMessage = '';
        public $User = array();
        
        public function __construct( $RememberMe ) {

            require_once ('/var/www/staphopia/lib/Common/Database.php');
            $this->db = new Database();
            
            $this->RememberMe = ($RememberMe == 'true') ? True : False;
        }
        
        
        
        /*******************************************************************************************
         *******************************************************************************************
         * 
         * Login Related
         * 
         *******************************************************************************************
         *******************************************************************************************
         * function TestUserName( String )
         * 
         * Tests whether the user actually exists, if so it stores the asscociated data
         *******************************************************************************************/
        public function TestUserName( $UserName ) {
            $this->Input = $UserName;
            $rows = $this->db->Query('select', "SELECT UserID, UserName, UserTag, Password, Salt, FirstName, LastName, 
                                                       Email, IsAdmin, IsLocked, CreateDate
                                                FROM User
                                                WHERE UserName=? OR Email=?
                                                LIMIT 1", array( $UserName, $UserName ));

            if (isset($rows[0]['UserID'])) {
                $this->User = $rows[0];
                return True;

            } else {
                $this->ErrorMessage = 'Invalid username and/or password. <br />';
                return False;
            }
        }
        
        /*******************************************************************************************
         * function TestPassword( String )
         * 
         * Validate a password, should take ~1 second to validate.
         *******************************************************************************************/
        public function TestPassword( $Password ) {
            require_once ('/var/www/staphopia/lib/Third-Party/pbkdf2.php');
            
            if (!isset($this->User['UserID'])) {
                // Still test to weaken brute force attacks
                $this->User['Salt'] = create_salt();
                $this->User['Password'] = create_salt();
            }
            
            if ($this->RememberMe) {
                $this->SessionToken = create_salt();
            }

            // Validate the Hash and Salt are correct lengths
            if (strlen($this->User['Salt']) != 128 || strlen($this->User['Password']) != 128) {
                $this->ErrorMessage .= 'An error has occured, an admin has been notified. <br />';
                $this->EmailError("Errors: \n Error creating salt and hashing.\n");
            }        
            
            // Test the given password against the Hash
            if (validate_password($Password, $this->User['Password'], $this->User['Salt'])) {
                if (ord($this->User['IsLocked']) == 1) {
                    $_SESSION['IsLocked'] = True;
                    $_SESSION['IsAuthorized'] = False;

                    $this->ErrorMessage .= 'This account has been locked, if you are the owner of this account ';
                    $this->ErrorMessage .= 'please check your email. <br />';
                    $this->EmailLockedAccount($this->GetUnlockCode());
                } else {
                    $this->SetSession( True );
                    // User logged in with a password
                    $_SESSION['IsValidated'] = True;
                }
                $this->ClearAttempts();
                return True;
            } else {
                $this->ErrorMessage = 'Invalid username and/or password. <br />';
                return False;
            }
        }
        
        /*******************************************************************************************
         * function LogAttempt( IP Address )
         * 
         * Return a count of total attempts for an IP address, and attempts against a user account.
         *******************************************************************************************/
        public function LogAttempt( $IP ) {
            $this->db->BeginTransaction( 'transaction' );
            $TotalAttempts = 0;
            $UserAttempts = 0;
            $sql = '';
            $a;
            if (isset($this->User['UserID'])) {
                $sql = "INSERT INTO UserLoginAttempt (IPv4, UserID) VALUES (INET_ATON(?),?)";
                $a = array( $IP, $this->User['UserID'] );
            } else {
                $sql = "INSERT INTO UserLoginAttempt (IPv4) VALUES (INET_ATON(?))";
                $a = array( $IP );
            }

            $rows = $this->db->Query('insert', $sql, $a);
            if ($rows == 1) {
                $this->db->Commit( 'transaction' );
                $rows = $this->db->Query('select', "SELECT COUNT(IPv4) AS i
                                                    FROM UserLoginAttempt
                                                    WHERE IPv4=INET_ATON(?)
                                                    LIMIT 1", array($IP));
                if (isset($rows[0]['i'])) {
                    $TotalAttempts = $rows[0]['i'];
                    
                    // If valid user, check the number of log attempts.
                    if (isset($this->User['UserID'])) {
                        $rows = $this->db->Query('select', "SELECT count(UserID) AS i
                                                            FROM UserLoginAttempt
                                                            WHERE IPv4=INET_ATON(?) AND UserID=?
                                                            LIMIT 1", array($IP, $this->User['UserID']));
                        if (isset($rows[0]['i'])) {
                            $UserAttempts = $rows[0]['i'];
                        }
                    }
                }
            }
            
            // We should always have at least one
            if ($TotalAttempts == 0) {
                $this->EmailError("Error adding attempt from $IP to database.\n");
            }
            
            return array( $TotalAttempts, $UserAttempts );
        }
        
        
        /*******************************************************************************************
         * function ClearAttempts()
         * 
         * Successfully logged in, clear all previous attempts from the client IP.
         *******************************************************************************************/
        private function ClearAttempts() {
            if (isset($_SESSION['IsLockedOut'])) {
                unset($_SESSION['IsLockedOut']);
            }
            $rows = $this->db->Query('delete', "DELETE FROM UserLoginAttempt
                                                WHERE IPv4=INET_ATON(?)", array( $_SERVER["REMOTE_ADDR"] ));
            if (!isset($rows)) {
                $this->EmailError("Error deleting login attempts by IP.\n");
            } 
        }
        
        
        /*******************************************************************************************
         *******************************************************************************************
         * 
         * Account Locking/Unlocking Related
         * 
         *******************************************************************************************
         *******************************************************************************************
         * function LockAccount()
         * 
         * Update 'IsLocked' bit field and email the user an email with an unlock code.
         *******************************************************************************************/
        public function LockAccount() {
            $rows = $this->db->Query('update', "UPDATE User
                                                SET IsLocked=TRUE
                                                WHERE UserID=?", array($this->User['UserID']));
            if ($rows == 1) {
                $this->DeleteAssociatedTokens();
                $this->EmailLockedAccount($this->CreateUnlockCode());
                $_SESSION['IsLocked'] = True;
                return True;
            } else {
                $this->EmailError("Error locking account.\n");
                return False;
            }
        }
        
        /*******************************************************************************************
         * function CreateUnlockCode()
         * 
         * Create a random 32 character string and asscociate it with an user id.
         *******************************************************************************************/
        private function CreateUnlockCode() {
            $CodeID = bin2hex(openssl_random_pseudo_bytes(16, $strong));
            $rows = $this->db->Query('insert', "INSERT INTO UserCode (UserID, CodeID, IsUserUnlock)
                                                VALUES (?,?,TRUE)", array( $this->User['UserID'], $CodeID ));
                                               
            if ($rows == 1) {
                return $CodeID;
            } else {
                $this->EmailError("Error locking account, unable to set unlock code.\n");
            }
        }
        
        /*******************************************************************************************
         * function GetUnlockCode()
         * 
         * Retrieve the unlock code asscociated with a given user id.
         *******************************************************************************************/
        public function GetUnlockCode() {
            $rows = $this->db->Query('select', "SELECT CodeID
                                                FROM UserCode
                                                WHERE UserID=? AND IsUserUnlock=TRUE
                                                LIMIT 1", array( $this->User['UserID']));
            if (isset($rows[0]['CodeID'])) {
                return $rows[0]['CodeID'];
            } else {
                $this->EmailError("Error locking account, unable to get unlock code.\n");
            }
        }
        
        /*******************************************************************************************
         * function UnlockAccount( string )
         * 
         * Lift the locked status on an account given a valid unlock code.
         *******************************************************************************************/
        public function UnlockAccount( $CodeID ) {
            $this->User['UserName'] = $CodeID;
            $UserID = $this->GetUserID( $CodeID );
            
            if ($UserID > 0) {
                $this->User['UserName'] = $UserID;
                $this->DeleteUnlockCode( $UserID );
                $rows = $this->db->Query('update', "UPDATE User
                                                    SET IsLocked=FALSE
                                                    WHERE UserID=?", array( $UserID ));
                if ($rows == 1) {
                    if (isset($_SESSION['IsLocked'])) {
                        unset($_SESSION['IsLocked']);
                    }
                    return True;
                } else {
                    $this->EmailError("Error unlocking account, unable to set IsLocked value to FALSE.\n");
                }
            }
            return False;
        }
        
        /*******************************************************************************************
         * function GetUserID( string )
         * 
         * Retrieve the user id associated with an unlock code.
         *******************************************************************************************/
        private function GetUserID( $CodeID ) {
            $rows = $this->db->Query('select', "SELECT UserID
                                                FROM UserCode
                                                WHERE CodeID=? AND IsUserUnlock=TRUE
                                                LIMIT 1", array( $CodeID ));
            if (isset($rows[0]['UserID'])) {
                return $rows[0]['UserID'];
            } else {
                $this->EmailError("Error unlocking account, unable to get user id.\n");
                return False;
            }
        }
        
        /*******************************************************************************************
         * function DeleteUnlockCode( int )
         * 
         * Delete associated unlock code given an user id.
         *******************************************************************************************/
        private function DeleteUnlockCode( $UserID ) {
            $rows = $this->db->Query('delete', "DELETE FROM UserCode
                                                WHERE UserID=? AND IsUserUnlock=TRUE", array( $UserID ));
            if (!isset($rows)) {
                $this->EmailError("Error unlocking account, unable to delete unlock code.\n");
            } 
        }
        
        
        
        /*******************************************************************************************
         *******************************************************************************************
         * 
         * Email Related
         * 
         ******************************************************************************************* 
         *******************************************************************************************
         * function EmailError()
         * 
         * Email any system associated errors
         *******************************************************************************************/
        public function EmailError( $error ) {
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $email = new Email();
            
            $body  = 'UserName => ' . ((isset($this->Input)) ? $this->Input : $this->User['UserName'])  . "\n";
            $body .= $error;
            $array = array(
                'Email' => 'error@staphopia.com',
                'Subject' => 'Login Related Error',
                'Body' => $body
            );
            
            $email->ErrorLog( $array );
        }
        
        /*******************************************************************************************
         * function EmailLockedAccount()
         * 
         * Resend owner an email to unlock their account.
         *******************************************************************************************/
        private function EmailLockedAccount( $CodeID ) {
            require_once ('/var/www/staphopia/lib/Common/Email.php');
            $email = new Email();
            $email->LockedAccount($this->User, $CodeID);
        }
        
        
        
        /*******************************************************************************************
         *******************************************************************************************
         * 
         * Cookie/Session Related
         * 
         *******************************************************************************************
         *******************************************************************************************
         * function TestSessionToken( string )
         * 
         * Test if the session token is a valid token.  If so create a new token, and set Session 
         * variables.  Otherwise delete the associated cookie it from the database.
         *******************************************************************************************/
        public function TestSessionToken( $token ) {
            $rows = $this->db->Query('select', "SELECT UserID
                                                FROM UserSession
                                                WHERE SessionToken=?
                                                LIMIT 1", array( $token ));
            if (isset($rows[0]['UserID'])) {               
                $rows = $this->db->Query('select', "SELECT UserID, UserName, UserTag, FirstName, LastName, 
                                                           Email, IsAdmin, IsLocked
                                                    FROM User
                                                    WHERE UserID=?
                                                    LIMIT 1", array( $rows[0]['UserID'] ));

                if (isset($rows[0]['UserTag'])) {
                    $this->User = $rows[0];
                    $this->SetSession( False );
                    // User did not log in with a password
                    $_SESSION['IsValidated'] = False;
                    
                    require_once ('/var/www/staphopia/lib/Third-Party/pbkdf2.php');
                    $this->SessionToken = create_salt();
                    $this->StoreSessionToken( $token );
                    
                    return True; 
                } else {
                    $this->EmailError("Error retrieving user details from session token.\n");
                }
            } else {
                // Session Token doesn't exist so lets have the cookie deleted
                setcookie("SessionToken", "", time()-3600, "/");
            }
            
            return False;
        }
        
        /*******************************************************************************************
         * function StoreSessionToken( string )
         * 
         * If a token is given, update the database row.  Otherwise insert the new token into the 
         * database.  10% of the time, clear out expired session tokens.  Create the new cookie. 
         *******************************************************************************************/
        private function StoreSessionToken( $token ) {
            $rows;
            require_once('/var/www/staphopia/lib/Third-Party/geoplugin.php');
            $geoplugin = new geoPlugin();
            $geoplugin->locate();
            $Location = $geoplugin->countryName .' ('. $geoplugin->city .', '. $geoplugin->region .')';
            if (!empty($token)) { //INET_ATON(?)
                $rows = $this->db->Query('update', "UPDATE UserSession
                                                    SET SessionToken=?, IPv4=INET_ATON(?), Location=?
                                                    WHERE SessionToken=?", 
                                         array($this->SessionToken, $geoplugin->ip, $Location, $token));
            } else {
                $rows = $this->db->Query('insert', "INSERT INTO UserSession (UserID, SessionToken, IPv4, Location)
                                                    VALUES (?,?,INET_ATON(?),?)", 
                                         array( $this->User['UserID'], $this->SessionToken, $geoplugin->ip, $Location));
            }

            if ($rows != 1) {
                $this->EmailError("Error saving session token to database.\n");
            } else {
                // 25% chance we clean up old session tokens
                if (rand(1, 4) == 2) {
                    $this->CleanSessionTokens();
                }
                
                // Expire in 7 days (604,800 seconds)
                setcookie('SessionToken', $this->SessionToken, time()+604800, "/");
            }
        }
        
        /*******************************************************************************************
         * function CleanSessionTokens()
         * 
         * Delete all rows that are older than 7 days.
         *******************************************************************************************/        
        private function CleanSessionTokens() {
            $rows = $this->db->Query('delete', "DELETE FROM UserSession
                                                WHERE CreateDate < (NOW() - INTERVAL 7 DAY)", array());
            if (!isset($rows)) {
                $this->EmailError("Error deleting session tokens from database.\n");
            } 
        }
        
        /*******************************************************************************************
         * function DeleteToken( string )
         * 
         * Delete a row given a token value.
         *******************************************************************************************/
        public function DeleteToken( $token ) {
            $rows = $this->db->Query('delete', "DELETE FROM UserSession
                                                WHERE SessionToken=?", array( $token ));
            if (!isset($rows)) {
                $this->EmailError("Error deleting session tokens from database.\n");
            } 
        }
        
        /*******************************************************************************************
         * function DeleteAssociatedTokens( )
         * 
         * Delete rows given a user id.
         *******************************************************************************************/
        private function DeleteAssociatedTokens() {
            $rows = $this->db->Query('delete', "DELETE FROM UserSession
                                                WHERE UserID=?", array( $this->User['UserID'] ));
            if (!isset($rows)) {
                $this->EmailError("Error deleting associated session tokens from database.\n");
            } 
        }
        
        /*******************************************************************************************
         * function SetSession( boolean )
         * 
         * If $Password is true, the user gave a valid password, therefor they actually logged in.
         * Otherwise a cookie has logged them in, so they will need to enter a password if they
         * want to change any settings.
         *******************************************************************************************/
        private function SetSession( $Password ) {
            
            if (ord($this->User['IsLocked']) == 1) {
                $_SESSION['IsLocked'] = True;
                $_SESSION['IsLoggedIn'] = False;
                
                if ($Password) {
                    $this->ErrorMessage .= 'This account has been locked, if you are the owner of this account ';
                    $this->ErrorMessage .= 'please check your email. <br />';
                    $this->EmailLockedAccount($this->GetUnlockCode());
                }    
            } else {
                $_SESSION['UserID'] = $this->User['UserID'];
                $_SESSION['UserName'] = $this->User['UserName'];
                $_SESSION['UserTag'] = $this->User['UserTag'];
                $_SESSION['Email'] = $this->User['Email'];
                $_SESSION['FirstName'] = $this->User['FirstName'];
                $_SESSION['LastName'] = $this->User['LastName'];
                $_SESSION['IsAdmin'] = ord($this->User['IsAdmin']);
                $_SESSION['IsLoggedIn'] = True;
                
                // User logged in with a password
                $_SESSION['IsValidated'] = ($Password) ? True : False;
                
                if ($this->RememberMe) {
                    $this->StoreSessionToken( '' );
                }
            }
        }
    }
    
?>
