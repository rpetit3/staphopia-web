<?php
    /*
     * 
     * 
     * 
     */
    class Virulence {
        
        public $vHits = 0;
        public $phenotype = '';
        public $proteins = array (
            'all' => '', 
            'strong' => ''
        );
        
        private $ACC = array ();
        private $categories = array();
        private $results;
        private $accessions;
        private $seen = array();
        private $types = array();
        
        public function __construct( $db, $id, $accessions, $category ) {
            $this->db = $db;
            $this->id = $id;
            $this->accessions = $accessions;
            
            $this->get_types();
            $this->get_results();
            $this->get_hits( $category );
            $this->predictPhenotype();
        }
        
        private function get_results() {
            $this->results = $this->db->Query('select', "SELECT `BlastID`, `Version`, `RunTime` 
                                                         FROM `Virulence` 
                                                         WHERE SampleID=?
                                                         ORDER BY Version DESC", array( $this->id ));   
        }

        private function get_types( ) {
            $rows = $this->db->Query('select', "SELECT `AccessionID`, `Type`, `SubType` 
                                                FROM `VirulenceType` 
                                                WHERE 1", array( ));
                                                
            foreach ($rows as $row) {
                $this->types[ $row['AccessionID'] ] = array(
                    'Type' => $row['Type' ],
                    'SubType' => $row['SubType']
                );
            }
        }

        private function get_hits( $type ) {
            
            $rows = $this->db->Query('select', "SELECT  `AccessionID`, `Length`, `Identity`, `Evalue`, `BitScore`, 
                                                        `Coverage`, `Score` 
                                                FROM `BlastResult` 
                                                WHERE BlastID=?", array($this->results[0]['BlastID']));
            foreach ($rows as $row) {
                $accession = $this->accessions[ $row['AccessionID'] ]['Accession'];
                $gene = $this->accessions[ $row['AccessionID'] ]['Gene'];
                $link = '<a href="http://www.uniprot.org/uniprot/'. $accession .'" target="_blank" title="View UniProt entry in a new window.">'. $accession .'</a>';
                $category = ucwords($type[ $this->types[ $row['AccessionID'] ]['Type'] ]);
                $cov = $row['Coverage'] * 100;
                $cov = number_format($cov, 2, '.', '');
                
                if ($row['Identity'] >= 80 && $cov >= 80) {
                    $res ='<tr class="strongHit">
                                <td>'. $gene .'</td>
                                <td>'. $category .'</td>
                                <td>'. $link .'</td>
                                <td>'. $row['Identity'] .'</td>
                                <td>'. $cov .'</td>
                                <td>'. $row['BitScore'].'</td>
                            </tr>';
                    $this->categories[$category] = 1;
                    $this->proteins['all'] .= $res;
                    $this->proteins['strong'] .= $res;
                    $this->vHits++;
                }
                else {
                    $this->proteins['all'] .='  <tr>
                                                    <td>'. $gene .'</td>
                                                    <td>'. $category .'</td>
                                                    <td>'. $link .'</td>
                                                    <td>'. $row['Identity'] .'</td>
                                                    <td>'. $cov .'</td>
                                                    <td>'. $row['BitScore'] .'</td>
                                                </tr>';
                }
            }
        }
        
        private function predictPhenotype() {
            
            ksort($this->categories);
            if (count($this->categories) > 0) {
                $i = 1;
                $row = '';
                foreach ($this->categories as $k => $v) {
                    $row .= '<li>'. $k .'</li>';
                    if ($i == 4) {
                        $this->phenotype .= '<ul class="inline-list predicted-phenotype">'. $row .'</ul>';
                        $row = '';
                        $i = 0;
                    }
                    $i++;
                }
                $this->phenotype .= '<ul class="inline-list predicted-phenotype">'. $row .'</ui>';
            }
            else {
                $this->phenotype .= '<p class="center" style="height: 3em;">There were no significant virulence hits.</p><p></p>';
            }
        }
    }
?>
