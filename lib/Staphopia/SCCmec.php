<?php
    /*
     * 
     * 
     * 
     * 
     */
    class SCCmec {
    
        public $best;
        public $images = '';
        public $coverage = array(
            'cov' => array(),
            'perc' => array(),
            'lists' => array()
        );
        public $proteins = array(
            'all' => '',
            'strong' => '',
            'mecA' => False
        );
        public $primers = array(
            'all' => '',
            'strong' => '',
            'mecA' => False
        );
        public $mecType = array(
            'coverage' => 'Negative',
            'protein' => 'Negative',
            'primer' => 'Negative',
            'consensus' => 'Negative'
        );
        
        private $db;
        private $id;
        private $result;
        private $mec_types;
        private $primer_list;
        private $accessions;
        
        public function __construct( $db, $id, $accessions ) {
            $this->db = $db;
            $this->id = $id;
            $this->accessions = $accessions;
            
            $this->get_results();
            $this->get_mec_type();
            $this->get_protein_hits();
            $this->get_primers();
            $this->get_primer_hits();
            $this->get_coverage_results();
            $this->coverageImages();
        }
        
        private function get_results() {
            $this->results = $this->db->Query('select', "SELECT `SCCmecID`, `BlastID`, `Version`, `RunTime`, 
                                                                `CoverageType`, `PrimerType`, `ProteinType` 
                                                         FROM `SCCmec` 
                                                         WHERE SampleID=? 
                                                         ORDER BY Version DESC", array( $this->id ));     
        }
        
        private function get_mec_type() {
            $rows = $this->db->Query('select', "SELECT `TypeID`, `Type` 
                                                FROM `SCCmecType` 
                                                WHERE 1", array());
            $type = array();
            foreach ($rows as $row) {
                $type[$row['TypeID']] = $row['Type'];
            }
            
            $this->mecType['coverage'] = $type[ $this->results[0]['CoverageType'] ];
            $this->mecType['protein'] = $type[ $this->results[0]['ProteinType'] ];
            $this->mecType['primer'] = $type[ $this->results[0]['PrimerType'] ];
            $this->consensusType();
            $this->mec_types = $type;
        }
        
        private function get_protein_hits() {
            $rows = $this->db->Query('select', "SELECT  `AccessionID`, `Length`, `Identity`, `Evalue`, `BitScore`, 
                                                        `Coverage`, `Score` 
                                                FROM `BlastResult` 
                                                WHERE BlastID=?", array( $this->results[0]['BlastID']));
                                                
            foreach ($rows as $row) {
                
                $cov = $row['Coverage'] * 100;
                $cov = number_format($cov, 2, '.', '');
                $gene = $this->accessions[ $row['AccessionID'] ]['Gene'];
                $accession = $this->accessions[ $row['AccessionID'] ]['Accession'];
                // Gene, Accession, %Identity, %Coverage
                if ($row['Identity'] >= 90 && $cov >= 50) {
                    // Significant hit
                    if (preg_match("/mecA/", $gene)) {
                        $this->proteins['mecA'] = True;
                    }
                    $r = '  <tr class="strongHit">
                                <td>'.$gene.'</td>
                                <td>'.$accession.'</td>
                                <td>'.$row['Identity'].'</td>
                                <td>'.$cov.'</td>
                            </tr>';
                    $this->proteins['strong'] .= $r;
                }
                else {
                    $r = '  <tr>
                                <td>'.$gene.'</td>
                                <td>'.$accession.'</td>
                                <td>'.$row['Identity'].'</td>
                                <td>'.$cov.'</td>
                            </tr>';
                }
                $this->proteins['all'] .= $r;
            }
        }
        
        private function get_primers() {
            $rows = $this->db->Query('select', "SELECT `PrimerID`, `Gene`, `Primer` 
                                                FROM `Primer` AS p
                                                INNER JOIN `Gene` AS g
                                                ON g.GeneID=p.GeneID
                                                WHERE 1", array());
            foreach ($rows as $row) {
                $this->primer_list[ $row['PrimerID'] ] = array(
                    'Gene' => $row['Gene'],
                    'Primer' => $row['Primer']
                );
            }
        }
        
        private function get_primer_hits() {
            $rows = $this->db->Query('select', "SELECT `PrimerID`, `Length`, `Identity`, `Evalue`, `BitScore`, `Coverage` 
                                                FROM `SCCmecPrimer` 
                                                WHERE SCCmecID=?", array($this->results[0]['SCCmecID']));
            

            foreach ($rows as $row) {
                $cov = $row['Coverage'] * 100;
                $cov = number_format($cov, 2, '.', '');
                $gene = $this->primer_list[ $row['PrimerID'] ]['Gene'];
                $primer = $this->primer_list[ $row['PrimerID'] ]['Primer'];
                
                if ($gene == "ccrB") {
                    $primer = "ccrB";
                    $gene = "ccrB1-3";
                }
                
                // Gene Primer Idenity Coverage
                if ($cov == 100 && $row['Identity'] >= 90) {
                    // Significant hit
                    if (preg_match("/mecA/", $gene)) {
                        $this->primers['mecA'] = True;
                    }
                    $r = '  <tr class="strongHit">
                                <td>'.$gene.'</td>
                                <td>'.$primer.'</td>
                                <td>'.$row['Identity'].'</td>
                                <td>'. $cov .'</td>
                            </tr>';
                    $this->primers['strong'] .= $r;
                }
                else {
                    $r = '  <tr>
                                <td>'.$gene.'</td>
                                <td>'.$primer.'</td>
                                <td>'.$row['Identity'].'</td>
                                <td>'. $cov .'</td>
                            </tr>';
                }
                $this->primers['all'] .= $r;
            }
        }
        
        private function get_coverage_results() {

            $best = 0;
            $rows = $this->db->Query('select', "SELECT `TypeID`, `Max`, `Mean`, `Percent` 
                                                FROM `SCCmecCoverage` 
                                                WHERE SCCmecID=?", array( $this->results[0]['SCCmecID'] ));
            foreach ($rows as $row) {
                $type = $this->mec_types[ $row['TypeID'] ];
                $this->coverage['cov'][$type]  = $row['Mean'];
                $this->coverage['perc'][$type] = $row['Percent']; 
                $test = $row['Mean'] * $row['Percent'];
                if ($test > $best) {
                    $this->coverage['type'] = $type;
                    $best = $row['Percent'];
                }
            }
        }   
        
        private function coverageImages() {
            $types = array("I"  , "IIa", "IIb", "IIe", "III", "IVa", "IVb", "IVc" , "IVd", "IVe", "IVg",
                           "IVh", "IVi", "IVj", "IVl", "V"  , "VI" , "VII", "VIII", "IX" , "X"  , "XI");
                           
            $li = '';
            foreach ($types as $type) {
                $SHA1  = sha1($this->id . "_" . $type);
                
                if ($type == "IVd" || $type == "V") {
                    array_push($this->coverage['lists'], $li);
                    $li = '';
                }
                
                if ($type == $this->coverage['type']) {
                    $this->images .= '<img class="centered sccmec" src="/images/' . substr($SHA1, 0, 2) . '/'. substr($SHA1, 2, 2) . '/' . $SHA1 . '.png" name="'.$type.'"/>';
                    $this->best = '<img class="centered" src="/images/' . substr($SHA1, 0, 2) . '/'. substr($SHA1, 2, 2) . '/' . $SHA1 . '.png"/>';
                    $li .= '<li class="active '. $type .'" onclick="showCoverage(\''.$type.'\');">';
                }
                else {
                    $this->images .= '<img class="centered sccmec" src="/images/' . substr($SHA1, 0, 2) . '/'. substr($SHA1, 2, 2) . '/' . $SHA1 . '.png" name="'.$type.'" style="display:none;"/>';
                    $li .= '<li class="'. $type .'" onclick="showCoverage(\''. $type .'\');">';

                }
                $li .= '<a href="#">
                            <ul class="inline-list">
                                <li>'. $type .'</li>
                                <li>'. round($this->coverage['cov'][$type]) .'x</li>
                                <li>'. round($this->coverage['perc'][$type]) .'%</li></li>
                            </ul>
                        </a>
                    </li>';
            }
            array_push($this->coverage['lists'], $li);
        }
        
        private function consensusType() {
                        
            if ($this->mecType['coverage'] == $this->mecType['protein'] && $this->mecType['protein'] == $this->mecType['primer']) {
                $this->mecType['consensus'] = $this->mecType['coverage'];
            }
            else if ($this->mecType['coverage'] == $this->mecType['protein']) {
                $this->mecType['consensus'] = $this->mecType['coverage'];
            }
            else if ($this->mecType['coverage'] == $this->mecType['primer']) {
                $this->mecType['consensus'] = $this->mecType['coverage'];
            }
            else if ($this->mecType['protein'] == $this->mecType['primer']) {
                $this->mecType['consensus'] = $this->mecType['protein'];
            }
        
            
            if ($this->proteins['mecA'] || $this->primers['mecA'] && $this->mecType['consensus'] == 'Negative') {
                $this->mecType['consensus'] = 'mecA Present';
            }
            else if ($this->proteins['mecA'] && $this->mecType['consensus'] == 'Negative') {
                $this->mecType['consensus'] = 'mecA Present (Protein)';
            }
            else if ($this->primers['mecA'] && $this->mecType['consensus'] == 'Negative') {
                $this->mecType['consensus'] = 'mecA Present (Primer)';
            }
        }
    }
?>

