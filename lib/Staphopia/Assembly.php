<?php 
    /*
     * 
     * 
     * 
     * 
     */
     
    class Assembly {
        public $stats = array();
        
        private $db;
        private $id;
        
        public function __construct( $db, $id ) {
            $this->db = $db;
            $this->id = $id;
            
            $this->get_assemblies();
        }
        
        public function get_assemblies() {
            $rows = $this->db->Query('select', "SELECT `Version`, `RunTime`, `N50`, `GC`, `TotalContigs`, 
                                                              `TotalSize`, `Mean`, `Median`, `MaxContig`, `MinContig`, 
                                                              `Greater500`, `Greater1K`, `Greater10K`, `Greater100K`, 
                                                              `ProgramID` 
                                                       FROM `Assembly` 
                                                       WHERE SampleID=?
                                                       ORDER BY Version DESC", array( $this->id ));

            foreach ($rows as $row) {
                array_push($this->stats, $row);   
            }
            
            if (count($this->stats) == 1) {
                array_push($this->stats, False);
            }
        }
    }
?>
