<?php 
    /*
     * 
     * 
     * 
     * 
     */
     
    class Filtered {
        
        public $stats = array();
        
        private $db;
        private $id;
        
        public function __construct( $db, $id ) {
            $this->db = $db;
            $this->id = $id;
            
            $this->get_stats();
        }
        
        public function get_stats() {
            $rows = $this->db->Query('select', "SELECT `Version`, `RunTime`, `MD5sum`, `BaseCount`, `ReadCount`, 
                                                              `MinLength`, `MeanLength`, `MaxLength`, `Quality`, 
                                                              `Coverage` 
                                                       FROM `FilteredStat` 
                                                       WHERE SampleID=?
                                                       ORDER BY Version DESC", array( $this->id ));
                                         
            foreach ($rows as $row) {
                array_push($this->stats, $row);
            }
            
            if (count($this->stats) == 1) {
                array_push($this->stats, False);
            }
        }
    }
?>
