<?php
    /*
     * 
     * 
     * 
     */
    class Resistance {
        
        public $rHits = 0;
        public $phenotype = '';
        public $proteins = array (
            'all' => '', 
            'strong' => ''
        );
        
        private $types = array ();
        private $categories = array();
        private $accessions;
        
        public function __construct( $db, $id, $accessions, $category ) {
            $this->db = $db;
            $this->id = $id;
            $this->accessions = $accessions;

            $this->get_resistance_types( $category );
            $this->get_results();
            $this->get_hits();
            $this->predictPhenotype();
        }
        
        private function get_results() {
            $this->results = $this->db->Query('select', "SELECT `BlastID`, `Version`, `RunTime` 
                                                         FROM `Resistance` 
                                                         WHERE SampleID=?
                                                         ORDER BY Version DESC", array( $this->id ));   
        }
        
        private function get_resistance_types( $category ) {
            $rows = $this->db->Query('select', "SELECT t.AccessionID, c.CategoryID
                                                FROM  `ResistanceType` AS t
                                                LEFT JOIN  `ResistanceClassToType` AS tc ON t.TypeID = tc.TypeID
                                                LEFT JOIN  `ResistanceClass` AS c ON c.ClassID = tc.ClassID", array());
                                                
            foreach ($rows as $row) {
                $this->types[ $row['AccessionID'] ] = $category[ $row['CategoryID'] ];
            }
        }
        
        private function get_hits() {
            
            $rows = $this->db->Query('select', "SELECT  `AccessionID`, `Length`, `Identity`, `Evalue`, `BitScore`, 
                                                        `Coverage`, `Score` 
                                                FROM `BlastResult` 
                                                WHERE BlastID=?", array($this->results[0]['BlastID']));
            foreach ($rows as $row) {
                $accession = $this->accessions[ $row['AccessionID'] ]['Accession'];
                $gene = $this->accessions[ $row['AccessionID'] ]['Gene'];
                $link = '<a href="http://www.uniprot.org/uniprot/'. $accession .'" target="_blank" title="View UniProt entry in a new window.">'. $accession .'</a>';
                $category = ucwords($this->types[ $row['AccessionID'] ]);
                $cov = $row['Coverage'] * 100;
                $cov = number_format($cov, 2, '.', '');
                
                if ($row['Identity'] >= 80 && $cov >= 80) {
                    $res ='<tr class="strongHit">
                                <td>'. $gene .'</td>
                                <td>'. $category .'</td>
                                <td>'. $accession .'</td>
                                <td>'. $row['Identity'] .'</td>
                                <td>'. $cov .'</td>
                                <td>'. $row['BitScore'].'</td>
                            </tr>';
                    $this->categories[$category] = 1;
                    $this->proteins['all'] .= $res;
                    $this->proteins['strong'] .= $res;
                    $this->rHits++;
                }
                else {
                    $this->proteins['all'] .='  <tr>
                                                    <td>'. $gene .'</td>
                                                    <td>'. $category .'</td>
                                                    <td>'. $accession .'</td>
                                                    <td>'. $row['Identity'] .'</td>
                                                    <td>'. $cov .'</td>
                                                    <td>'. $row['BitScore'] .'</td>
                                                </tr>';
                }
            }
        }
        
        private function predictPhenotype() {
            
            ksort($this->categories);
            if (count($this->categories) > 0) {
                $i = 1;
                $row = '';
                foreach ($this->categories as $k => $v) {
                    $row .= '<li>'. $k .'</li>';
                    if ($i == 4) {
                        $this->phenotype .= '<ul class="inline-list predicted-phenotype">'. $row .'</ul>';
                        $row = '';
                        $i = 0;
                    }
                    $i++;
                }
                $this->phenotype .= '<ul class="inline-list predicted-phenotype">'. $row .'</ui>';
            }
            else {
                $this->phenotype .= '<p class="center" style="height: 3em;">There were no significant resistance hits.</p><p></p>';
            }
        }
    }
?>
