<?php 
    /*
     * 
     * 
     * 
     * 
     */
     
    class MLST {
        
        public $stats = array();
        public $method = array (
            '1' => 'Assembly/Mapping', 
            '2' => 'Assembly', 
            '3' => 'Mapping', 
            '4' => 'Unable to Determine'
        );
        private $db;
        private $id;
        
        public function __construct( $db, $id ) {
            $this->db = $db;
            $this->id = $id;
            
            $this->get_sequence_type();
        }
        
        public function get_sequence_type() {
            $rows = $this->db->Query('select', "SELECT `Version`, `RunTime`, `SequenceType`, `ClonalComplex`, 
                                                              `arcc_id`, `arcc_aScore`, `arcc_aCov`, `arcc_aIdent`, 
                                                              `arcc_aCount`, `arcc_mScore`, `arcc_mCount`, `arcc_status`, 
                                                              `aroe_id`, `aroe_aScore`, `aroe_aCov`, `aroe_aIdent`, 
                                                              `aroe_aCount`, `aroe_mScore`, `aroe_mCount`, `aroe_status`, 
                                                              `glpf_id`, `glpf_aScore`, `glpf_aCov`, `glpf_aIdent`, 
                                                              `glpf_aCount`, `glpf_mScore`, `glpf_mCount`, `glpf_status`, 
                                                              `gmk__id`, `gmk__aScore`, `gmk__aCov`, `gmk__aIdent`, 
                                                              `gmk__aCount`, `gmk__mScore`, `gmk__mCount`, `gmk__status`, 
                                                              `pta__id`, `pta__aScore`, `pta__aCov`, `pta__aIdent`, 
                                                              `pta__aCount`, `pta__mScore`, `pta__mCount`, `pta__status`, 
                                                              `tpi__id`, `tpi__aScore`, `tpi__aCov`, `tpi__aIdent`, 
                                                              `tpi__aCount`, `tpi__mScore`, `tpi__mCount`, `tpi__status`, 
                                                              `yqil_id`, `yqil_aScore`, `yqil_aCov`, `yqil_aIdent`, 
                                                              `yqil_aCount`, `yqil_mScore`, `yqil_mCount`, `yqil_status` 
                                                       FROM `SequenceType` 
                                                       WHERE SampleID=?
                                                       ORDER BY Version DESC", array( $this->id ));
            foreach ($rows as $row) {
                array_push($this->stats, $row);   
            }
                
            if (count($this->stats) == 1) {
                array_push($this->stats, False);
            }
        }
        
        public function stat($index, $key) {
            return $this->stats[$index]->$key;    
        }
        
        public function get_related( $cc, $GRADE ) {
            $rows = $this->db->Query('select', "SELECT s.SampleID, s.SampleTag, s.SubmitDate, t.ClonalComplex, 
                                                       t.SequenceType
                                                FROM SequenceType AS t
                                                INNER JOIN Sample AS s
                                                ON s.SampleID=t.SampleID
                                                WHERE ClonalComplex=? AND NOT SampleID=? 
                                                ORDER BY s.SubmitDate DESC", array( $cc, $this->id ));
                                            
            $related = '';
            foreach ($rows as $row) {
                if (empty($row['SequenceType'])) {
                    $row['SequenceType'] = '-';
                }
                
                if ($row['ClonalComplex'] == 0) {
                    $row['ClonalComplex'] = '-';
                }
                $related .= '   <tr>
                                    <td>'. $row['SampleTag'] .'</td>
                                    <td>'. $GRADE .'</td>
                                    <td>'. $row['SequenceType'] .'</td>
                                    <td>'. $row['ClonalComplex'] .'</td>
                                    <td>'. date("F j, Y", strtotime($row['SubmitDate'])) .'</td>
                                </tr>';
            }
            
            return $related;
        }
    }
?>
