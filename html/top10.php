<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (!isset($_SESSION['IsLoggedIn'])) {
        // Must be logged in to access
        header('Location: /index.php');
    } else {
        require_once ('/var/www/staphopia/lib/HTML/Top10.php');
        $top10 = new Top10();
    }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <!-- End Defaults -->
    <style>
        hr.shift-up {
            margin-top: -0.5em !important;
        }
        table.center {
            margin-left:auto; 
            margin-right:auto;
        }
    </style>
    
    <title>Staphopia - Top 10</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->

        <div class="row">
            <div class="small-12 columns">
                <h3 class="center">Top 10</h5>
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="small-8 small-centered columns">
                <h5 class="center">Contributers</h5>
                <hr class="shift-up"/>
                <div class="row">
                    <div class="small-12 columns">
                        <? 
                            echo $top10->getTop10('topSeqCenters', 'seq_center', 'Sequencing Center', 'Total Genomes'); 
                        ?>
        
                    </div>
                </div>        
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <p></p>
            </div>
        </div>
        <div class="row">
            <div class="small-11 small-centered columns">
                <ul class="large-block-grid-2">
                    <li>
                        <h5 class="center">Sequence Types (ST)</h5>
                        <hr class="shift-up"/>
                        <? echo $top10->getTop10('topST', 'st', 'ST', 'Total Genomes') ?>
                        
                    </li>
                    <li>
                        <h5 class="center">Clonal Complexes (CC)</h5>
                        <hr class="shift-up"/>
                        <? echo $top10->getTop10('topCC', 'cc', 'CC', 'Total Genomes') ?>
                        
                    </li>
                </ul>
            </div>
        </div>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script>
            $(document).foundation();
        </script>
        <!-- End Javascript -->

    </body>
</html>
