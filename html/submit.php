<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (!isset($_SESSION['IsLoggedIn'])) {
        // Must be logged in to access
        header('Location: /index.php');
    } else {
        $Tags = array();
        if (isset($_SESSION['POST'])){
            $Tags = $_SESSION['POST'];  
            unset($_SESSION['POST']);
        } else {
            require_once ('/var/www/staphopia/lib/HTML/Settings.php');
            $settings =  new Settings();
            $Tags = $settings->GetSavedSettings($_SESSION['UserID']); 
        }
    }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <!-- End Defaults -->
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" type="text/css" rel="Stylesheet" />
    <link rel="stylesheet" href="/css/general_foundicons.css">
    
    <title>Staphopia - Submission</title>
    <style>
        .required {
            font-weight: bold;
            font-style: italic;
        }
        div.callout {
            padding: 0.5em !important;
        }
        img.loading {
            display: none;
            margin-left: auto;
            margin-right: auto;
        }
        .ui-widget-content {
            width: 410px !important;
        }
        .ui-autocomplete {
          padding: 0;
          list-style: none;
          background-color: #fff;
          width: 218px;
          border: 1px solid #B0BECA;
          max-height: 350px;
          overflow-y: scroll;
        }
        .ui-autocomplete .ui-menu-item a {
          border-top: 1px solid #B0BECA;
          display: block;
          padding: 4px 6px;
          color: #353D44;
          cursor: pointer;
        }
        .ui-autocomplete .ui-menu-item:first-child a {
          border-top: none;
        }
        .ui-autocomplete .ui-menu-item a.ui-state-hover {
          background-color: #D5E5F4;
          color: #161A1C;
        }
        #HideDropDown div.dropdown {
            display: none;
        }
        
        #IsolationCountry {
            display: block !important;
        }
        
        span.GeoNames {
            font-size: 0.7em;
            float: right;
            bottom: 0px;
            line-height: 2.32em;
        }
        
        .loading {
            display: inline !important;
        }
        
        .mic {
            font-size: 0.7em;
            padding-top: 1em;
            text-align: right;
            font-style: italic;
            font-weight: bold;
        }
    </style>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->
        
        <!-- Main Page Content-->
        <? include ('/var/www/staphopia/template/submit.php') ?>
        <!-- End Main Content-->

        <a href="#" data-reveal-id="submit-modal" class="reveal-link" style="display:none !important;"></a>
        <div id="submit-modal" class="reveal-modal small">
            <p class="response center"></p>
            <img class="loading" src="/img/loading.gif">
            <a class="close-reveal-modal">&#215;</a>
        </div>

        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <script src="/js/dev/submit.js"></script>
        <script src="/js/jquery.form.js"></script>
        <script src="/js/jquery.select-to-autocomplete.js"></script>
        <script>
            var s;
            $(document).ready(function () {
                s = new Submit();
                $( "#SubmitForm input[name=Ajax]" ).val('1');
                $('.error .alert-box').css('visibility', 'hidden');
                $( '#SubmitForm' ).ajaxForm({
                    beforeSubmit: this.Validate,
                    uploadProgress: this.UpdateStatus,
                    success: this.Success
                });
                $('#country-selector').selectToAutocomplete();
                $("#country-selector > option").each(function() {
                    s.CODES[$(this).val()] = $(this).data('alternativeSpellings').substring(0,2);
                });
                s.Sliders();
                s.AutoComplete();
                s.HostName()

                $('#IsolationCountry').bind("change paste keyup", function() {
                    if ($('#IsolationCountry').val() != '-' && $('#IsolationCountry').val() != '') {
                        $('#IsolationCity').prop('disabled', false);
                        $('#IsolationRegion').prop('disabled', false);
                    } else {
                        $('#IsolationCity').prop('disabled', true);
                        $('#IsolationRegion').prop('disabled', true);
                    }
                });
            });              
        </script>
        <!-- End Javascript -->

    </body>
</html>
