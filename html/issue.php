<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (!isset($_SESSION['IsLoggedIn']) && !isset($_SESSION['IsLive'])) {
        header('Location: /index.php');
    }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <!-- End Defaults -->

    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <div class="row">
            <div class="small-10 small-centered columns">
                <p></p>
                <p class="indent">
                    Thank you very much for helping us to improve Staphopia. Please feel free to use the issue tracker 
                    as much as you see fit.  Also, you are not limited to reporting issues, you may also make feature 
                    requests and propose ideas to enhance Staphopia.  We only ask that you follow a few guidlines when 
                    creating a new issue.  
                </p>

                <strong>General</strong>
                <p style="padding-left: 2em;">
                    - Try to be as descriptive as possible, and fill out as many fields as you can. <br />
                    - If you would like to be notified on issue updates, leave an email in the description.
                </p>
                
                <strong>Issue Titles</strong>
                <p style="padding-left: 2em;">
                    - Start issue titles with '<em>Development - </em>'<br />
                    - e.g. <em>Development - Broken link on Top10 page </em>
                </p>
                
                <strong title="Making bitbucket work!">Component</strong>
                <p style="padding-left: 2em;">
                    - A general group to place the issue in.<br />
                    - None Applicable? Add '<em>Component - Proposed Group</em>' in the description.
                </p>
                
                <strong title="Making bitbucket work!">Milestone</strong>
                <p style="padding-left: 2em;">
                    - Internet browser you are using.<br />
                    - Not Listed? Add '<em>Milestone - Your Browser Name</em>' in the description.
                </p>
                
                <strong title="Making bitbucket work!">Version</strong>
                <p style="padding-left: 2em;">
                    - Device or operating system you are using. <br />
                    - Not Listed? Add '<em>Component - Your Device/OS Name</em>' in the description.
                </p>
                <p></p>
                <p>
                    <h3 class="center"><a href="https://bitbucket.org/rpetit3/staphopia-web/issues/new" 
                       title="Go to bitbucket.org to create a new issue." target="_blank" >Create A New Issue</a></h3>
                </p>
            </div>
        </div>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script>
            $(document).foundation();
        </script>
        <!-- End Javascript -->
        
    </body>
</html>
