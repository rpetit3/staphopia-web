<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }
    
    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (!isset($_SESSION['IsLoggedIn']) && !isset($_SESSION['IsLive'])) {
        header('Location: /index.php');
    }
    
    if (isset($_GET['q'])) {
        $_SESSION['u'] = $_GET['q'];   
    }

?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <!-- End Defaults -->
    <link rel="stylesheet" href="/css/ColVis.css">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" 
          type="text/css" rel="Stylesheet" />
    <link href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.3/css/jquery.dataTables_themeroller.css" 
          type="text/css" rel="stylesheet" >

    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <div class="row">
            <div class="small-12 columns">
                <p></p>
                <table id="genomesTable" class="small-12">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Owner</th>
                            <th>Public</th>
                            <th>Sample Name</th>
                            <th>Strain</th>
                            <th>Class</th>
                            <th>Sequencing Center</th>
                            <th>Q-Score</th>
                            <th>Coverage</th>
                            <th>GC Content</th>
                            <th>Average Read</th>
                            <th>ST</th>
                            <th>CC</th>
                            <th>SCCmec</th>
                            <th>Total Size</th>
                            <th>n50</th>
                            <th>Smallest Contig</th>
                            <th>Total Contigs</th>
                            <th>Resistance Hits</th>
                            <th>Virulence Hits</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-dataTables.php') ?>
        <script>
            $(document).foundation();
            $(document).ready(function () {       
                InitGenomeTable();
            });
        </script>
        <!-- End Javascript -->
        
    </body>
</html>
