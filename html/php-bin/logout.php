<?php session_start();
    if (isset($_COOKIE['SessionToken'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->DeleteToken( $_COOKIE['SessionToken'] );
        setcookie("SessionToken", "", time()-3600, "/");
    }

    session_unset();
    session_destroy();
    
    header('Location: /index.php');
?>
