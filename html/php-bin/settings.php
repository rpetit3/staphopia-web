<?php session_start();

    $commands = array(
        'saved' => True,
        'email' => True,
        'password' => True,
        'sessions' => True
    );
    
    if (!isset($_POST['Command'])) {
        // No direct access
        header('Location: /index.php');
    }
    else if (array_key_exists($_POST['Command'], $commands)) {
        require_once ('/var/www/staphopia/lib/HTML/Settings.php');
        $settings =  new Settings();
        
        require_once ('/var/www/staphopia/lib/Common/Validate.php');
        $v = new Validate();

        if($_POST['Command'] == 'password') {
            if (isset($_POST['Original']) && isset($_POST['Password']) && isset($_POST['Password1'])) {
                if ($v->IsValidPassword($_POST['Password']) && $_POST['Password'] == $_POST['Password1']) {
                    $settings->UpdatePassword($_POST['Original'], $_POST['Password']);
                } else {
                    $_SESSION['ErrorMessage'] = 'Passwords must match, and meet the stringency requirements.';
                }
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            }
        }
        else if ($_POST['Command'] == 'email') {
            if (isset($_POST['Email'])) {
                if (!$v->IsValidEmail($_POST['Email'])) {
                    $_SESSION['ErrorMessage'] = 'Please make sure the email given is a valid format.';
                } 
                else if ($_POST['Email'] == $_SESSION['Email']) {
                    $_SESSION['ErrorMessage'] = 'Nothing to do, new address is the same as your current address.';
                } else {
                    $settings->UpdateEmail( $_POST['Email'] );
                }
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            }            
        } 
        else if ($_POST['Command'] == 'saved') {
            // Add any new Tags
            $Tags = $settings->GetTags(False);
            foreach ($_POST as $Tag => $Value) {
                if ($Tag != 'Command') {
                    if (!array_key_exists($Tag, $Tags)){
                        $TagID = $settings->InsertTag( $Tag );
                        if ($TagID == 0) {
                            break;
                        } else {
                            $Tags[ $Tag ] = $TagID;   
                        }
                    } 
                }
            }
            
            $UserSettings = $settings->GetSavedSettings($_SESSION['UserID']);
            foreach ($Tags as $Tag => $TagID) {
                if (!empty($_POST[ $Tag ])) {
                    $Insert = True;
                    if (!empty($UserSettings[ $Tag ])) {
                        if ($UserSettings[ $Tag ] != $_POST[ $Tag ]) {
                            $Insert = False;
                        } else {
                            continue;
                        }
                    }
                    
                    if(!$settings->SaveSetting($_SESSION['UserID'], $Tags[ $Tag ], $_POST[ $Tag ], $Insert)) {
                        break;   
                    }
                } else {
                    if (!empty($UserSettings[ $Tag ])) {
                        if (!$settings->DeleteSetting($_SESSION['UserID'], $TagID)) {
                            break;   
                        }
                    }
                }
            }
        }
        else if ($_POST['Command'] == 'sessions') {
            if (count($_POST['Tokens']) > 0) {
                require_once ('/var/www/staphopia/lib/HTML/Login.php');
                $login =  new Login(False);
                foreach ($_POST['Tokens'] as $token) {
                    $login->DeleteToken( $token ) ;
                }
                $_SESSION['ErrorMessage'] = 'Successfully removed selected sessions.';
            } else {
                $_SESSION['ErrorMessage'] = 'Please select at least one session to remove.';  
            }
        }
        header('Location: /settings/'. $_POST['Command'] );
        
    } else {
        // Not a valid command
        header('Location: /index.php');
    }
?>    
