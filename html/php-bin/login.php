<?php session_start();
          
    if (!isset($_POST['UserName'])) {
        // No direct access
        header('Location: /index.php');
    } else {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login($_POST['RememberMe']);
        
        $isUser = $login->TestUserName( $_POST['UserName'] );
        $isValid = $login->TestPassword( $_POST['Password'] );
        
        
        if (strlen($login->ErrorMessage) > 0) {
            list( $TotalAttempts, $UserAttempts) = $login->LogAttempt( $_SERVER["REMOTE_ADDR"] );
            
            if ($isUser) {               
                if ($UserAttempts > 4 && !isset($_SESSION['IsLocked'])) {
                    $login->LockAccount();
                } 
            }
            
            if ($TotalAttempts > 9) {
                // To many attempts, temp ban
                session_unset();
                session_destroy();
                session_start();
                session_regenerate_id(True);
                $_SESSION['IsLockedOut'] = True;
                echo "1";
            }
            else if (isset($_POST['Ajax'])) {
                echo $login->ErrorMessage;
            } else {
                $_SESSION['ErrorMessage'] = $login->ErrorMessage;
                header('Location: /login/');
            }
        } else {
            // Successful login           
            if (isset($_POST['Ajax'])) {
                echo "1";
            } else {
                header('Location: /index.php');
            }
        }
    }

?>
