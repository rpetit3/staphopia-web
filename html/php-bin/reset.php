<?php session_start();
        
    if (!isset($_POST['Command'])) {
        // No direct access
        header('Location: /index.php');
    }
    else {

        if ($_POST['Command'] == 'password') {
            $redirect = '';
            // User is resetting the password with a given code
            if ($_POST['Type'] == 'reset') {
                $redirect = 'Location: /reset/'. $_POST['Command'] .'/'. $_POST['CodeID'];
                if (isset($_POST['Password']) && isset($_POST['Password1'])) {
                    if ($_POST['Password'] == $_POST['Password1']) {
                        require_once ('/var/www/staphopia/lib/Common/Validate.php');
                        $v = new Validate();
                        
                        if ($v->IsValidPassword( $_POST['Password'])) {
                            require_once ('/var/www/staphopia/lib/HTML/Settings.php');
                            $settings =  new Settings();
                            if ($settings->ResetPassword($_POST['CodeID'], $_POST['Password'])) {
                                $redirect = 'Location: /login/';
                            } 
                        } else {
                            $_SESSION['ErrorMessage']  = 'Your password does not meet our requirements. Passwords must be at least eight ';
                            $_SESSION['ErrorMessage'] .= 'characters in length and contain at least one character from the following ';
                            $_SESSION['ErrorMessage'] .= 'groups: lower-case letters, upper-case letters and numbers or special characters.';
                        }
                    } else {
                        $_SESSION['ErrorMessage'] = 'Passwords must match, please try again.';   
                    }                
                } else {
                    $_SESSION['ErrorMessage'] = 'Please do not leave any fields empty.';  
                } 
            } 
            // User is requesting a password change
            else if ($_POST['Type'] == 'request') {
                $redirect = 'Location: /reset/'. $_POST['Command'] .'/'. $_POST['Type'];
                if (isset($_POST['Email'])) {
                    require_once ('/var/www/staphopia/lib/Common/Validate.php');
                    $v = new Validate();
                    
                    if ($v->IsValidEmail( $_POST['Email'])) {
                        require_once ('/var/www/staphopia/lib/HTML/Settings.php');
                        $settings =  new Settings();
                        $settings->PasswordResetRequest($_POST['Email']); 
                    } else {
                        $_SESSION['ErrorMessage']  = 'Please give a valid email address.';
                    }
                } else {
                    $_SESSION['ErrorMessage'] = 'Please do not leave any fields empty.'; 
                }
            } else {
                $redirect = 'Location: /index.php';
            }
            header($redirect);
        } else {
            // Unknown request
            header('Location: /index.php');
        }
    }

?>
