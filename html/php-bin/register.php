<?php session_start();
    
    if (isset($_POST['CheckUser'])) {
        require_once ('/var/www/staphopia/lib/HTML/Register.php');
        $register = new Register();
        echo ($register->UserExists($_POST['CheckUser'])) ? '1' : '0';
    }
    else if (isset($_POST['CheckEmail'])) {
        require_once ('/var/www/staphopia/lib/HTML/Register.php');
        $register = new Register();
        echo ($register->EmailExists($_POST['CheckEmail'])) ? '1' : '0';
    }
    else if (!isset($_POST['FirstName'])) {
        // No direct access
        header('Location: /index.php');
    } else {
        require_once ('/var/www/staphopia/lib/HTML/Register.php');
        $register = new Register();
           
        if ($register->ValidInputs($_POST)) {
            // Valid input
            $EmailExists = $register->EmailExists($_POST['Email']);
            $UserExists = $register->UserExists($_POST['UserName']);
            if (!$EmailExists && !$UserExists && $register->CheckAccessCode()) {
                // Unused email, username, and valid access code
                $register->AddNewUser();
            }
        } 

        if (isset($_POST['Ajax'])) {
            if ($register->HasError) {
                echo $register->ErrorMessage;
                $register->EmailErrors();
            } else {
                echo "1";
            }
        } else {
            if ($register->HasError) {
                $_SESSION['ErrorMessage'] = $register->ErrorMessage;
                $register->EmailErrors();
                header('Location: /register/');
            } else {
                header('Location: /login/');
            }
        }
    }

?>
