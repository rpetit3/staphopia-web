<?php session_start();

    if (!isset($_POST['ContactName'])) {
        // No direct access
        header('Location: /index.php');
    } else {
        require_once ('/var/www/staphopia/lib/HTML/Submit.php');
        $submit = new Submit($_POST, $_FILES);
        
        if ($submit->CheckRequired()) {
            // Validate inputs
            if ($submit->Validate()) {
                $submit->SubmitSequence();
            }
        }
        
        if ($submit->IsError) {
            if ($_POST['Ajax'] == 1) {
                echo $submit->ErrorMessage;
            } else {
                $_SESSION['ErrorMessage'] = $submit->ErrorMessage;
                $_SESSION['POST'] = $_POST;
                header('Location: /submit/');
            }
        } else {
            if ($_POST['Ajax'] == 1) {
                echo $submit->MD5sum;
            } else {
                header('Location: /status/'. $submit->MD5sum);
            }
        }
    }
?>