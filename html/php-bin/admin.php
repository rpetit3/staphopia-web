<?php session_start();

    $commands = array(
        'lock' => True,
        'unlock' => True,
        'delete' => True,
        'grant' => True,
        'revoke' => True,
        'invite' => True
    );

    if (!isset($_POST['Command'])) {
        // No direct access
        header('Location: /index.php');
    } else if (array_key_exists($_POST['Command'], $commands)) {
        require_once ('/var/www/staphopia/lib/Common/Admin.php');
        $admin =  new Admin();
        $_SESSION['ErrorMessage'] = '';
        if ($_POST['Command'] == 'invite') {
            
            if (isset($_POST['FirstName']) && isset($_POST['LastName']) && isset($_POST['Email'])) {
                require_once ('/var/www/staphopia/lib/Common/Validate.php');
                $v = new Validate();
                
                if (!$v->IsValidName( $_POST['FirstName'] )) {
                    $_SESSION['ErrorMessage'] .= '"'. $_POST['FirstName'] .'" is an invalid first name. <br />';
                }
                
                if (!$v->IsValidName( $_POST['LastName'] )) {
                    $_SESSION['ErrorMessage'] .= '"'. $_POST['LastName'] .'" is an invalid last name. <br />';
                }
                
                $Email = $v->SanitizeEmail( $_POST['Email'] ); 
                if (!$v->IsValidEmail( $Email )) {
                    $_SESSION['ErrorMessage'] .= '"'. $Email  .'" is an invalid email. <br />';
                }
                
                if (empty($_SESSION['ErrorMessage'])) {
                    $success = $admin->InviteUser( array(
                                    'Email' => $Email,
                                    'FirstName' => $_POST['FirstName'],
                                    'LastName' => $_POST['LastName']
                                ));
                }
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            } 
        } else if ($_POST['Command'] == 'delete'){
            if (isset($_POST['UserName']) && isset($_POST['FirstName']) && isset($_POST['LastName']) 
                                          && isset($_POST['Email']) && isset($_POST['UserID'])) {
                
                $admin->DeleteAccount( array(
                    'UserName' => $_POST['UserName'],
                    'FirstName' => $_POST['FirstName'],
                    'LastName' => $_POST['LastName'],
                    'Email' => $_POST['Email'],
                    'UserID' => $_POST['UserID']
                ));
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            } 
        } else if (isset($_POST['UserName'])) {
            if (count($_POST['UserName']) > 0) {
                foreach ($_POST['UserName'] as $UserName) {
                    switch ($_POST['Command']) {
                        case 'lock':
                            $admin->LockUser( $UserName );
                            break;
                        case 'unlock':
                            $admin->UnlockUser( $UserName );
                            break;
                        case 'grant':
                            $admin->MakeAdmin( $UserName );
                            break;
                        case 'revoke':
                            $admin->RemoveAdmin( $UserName );
                            break;
                    }
                }                
            } else {
                $_SESSION['ErrorMessage'] = 'Please select atleast one user.';
            }
        }
        
        header('Location: /admin/'. $_POST['Command'] );
    } else {
        // Invalid command
        header('Location: /index.php');
    }

?>
