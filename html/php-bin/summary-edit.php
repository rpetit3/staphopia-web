<?php session_start();

    if (!isset($_POST['ContactName'])) {
        // No direct access
        header('Location: /index.php');
    } else {
        require_once ('/var/www/staphopia/lib/HTML/Edit.php');
        $edit = new Edit($_SESSION['SampleTag'], $_SESSION['UserID'], $_POST);
        $location = '/summary/'. $_SESSION['SampleTag'] .'/edit';
        unset($_SESSION['SampleTag']);

        if ($edit->CheckRequired()) {
            // Validate inputs
            if ($edit->Validate()) {
                // Add edits
                $edit->SubmitChanges();
            }
        }
        
        $_SESSION['ErrorMessage'] = $edit->ErrorMessage;
        header('Location: '. $location);
    }

?>