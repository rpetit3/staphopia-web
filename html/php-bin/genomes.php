<?php session_start();
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */
     
    /* Database credentials */
    require_once ('/var/www/staphopia/config/database.php');
    $DB = new DatabaseConfig();

    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
    $aColumns =   array( 
        'SampleID', 'UserName', 'Public', 'SampleTag', 'Strain', 'Class', 'SequencingCenter', 'Quality', 'Coverage', 
        'GC', 'MeanLength', 'SequenceType', 'ClonalComplex', 'SCCmec', 'TotalSize', 'N50', 'MinContig', 'TotalContigs', 
        'ResistanceHits', 'VirulenceHits'
    );
    $notPublished = array( 
        'sccmec' => 1,
        'resistance' => 1,
        'ResistanceHits' => 1,
        'virulence' => 1,
        'VirulenceHits' => 1        
    );
    
    $IsST = False;
    $IsCC = False;
    $MasterST = False;
    $MasterCC = False;
    
    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "SampleID";
    
    /* DB table to use */
    $sTable = "Summary";
    
    // SQL Statement
    $select = 'SELECT SQL_CALC_FOUND_ROWS `SampleID`, `SampleTag`, `UserName`, `BaseCount`, `ReadCount`, `MeanLength`, 
                                          `Quality`, `Coverage`, `N50`, `GC`, `TotalContigs`, `TotalSize`, `MinContig`,
                                          `SequenceType`, `ClonalComplex`, `ResistanceHits`, `VirulenceHits`, 
                                          `SequencingCenter`, `Strain`, `IsPublic` AS Public
                FROM `Summary`';

    /* Database connection information */
    $gaSql['user']       = $DB->db['select']['username'];
    $gaSql['port']       = $DB->db['port'];
    $gaSql['password']   = $DB->db['select']['password'];
    $gaSql['db']         = $DB->db['database'];
    $gaSql['server']     = $DB->db['host'];      

  
    // parse new querystring variable "o" to represent output_mode
    $output_mode = (isset($_GET["o"])) ? $_GET["o"] : "json";
    
    // Filter by username
    $user = '';
    if (isset($_SESSION['u'])) {
        $user = $_SESSION['u'];   
    }
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP server-side, there is
     * no need to edit below this line
     */
    
    /* 
     * Local functions
     */
    function fatal_error ( $sErrorMessage = '' ) {
        header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
        die( $sErrorMessage );
    }

    /* 
     * MySQL connection
     */
    if ( ! $gaSql['link'] = mysql_pconnect( $gaSql['server'].':'.$gaSql['port'], $gaSql['user'], $gaSql['password']  ) ) {
        fatal_error( 'Could not open connection to server' );
    }

    if ( ! mysql_select_db( $gaSql['db'], $gaSql['link'] ) ) {
        fatal_error( 'Could not select database ' );
    }

    /* 
     * Paging
     */
    $sLimit = "";
    if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' && $output_mode != "csv") {
        $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
        intval( $_GET['iDisplayLength'] );
    }
    
    
    /*
     * Ordering
     */
         error_log(print_r($_GET, True));
    $sOrder = "";
    if ( isset( $_GET['iSortCol_0'] ) ) {
        $sOrder = "ORDER BY  ";
        for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ) {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ) {
                $sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
                mysql_real_escape_string( $_GET['sSortDir_'.$i] ) .", ";
            }
        }
        
        $sOrder = substr_replace( $sOrder, "", -2 );
        if ( $sOrder == "ORDER BY" ) {
            $sOrder = "";
        }
    }
    
    /* 
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables, and MySQL's regex functionality is very limited
     */
    $sWhere = "";

    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
        $terms = preg_split('/ /', $_GET['sSearch']);
        
        $sWhere = "WHERE (";
        for ( $i=0 ; $i<count($terms) ; $i++ ) {
            if (preg_match("/^ST\d+$/", $terms[$i]) && !$IsST) {
                $IsST = True;
                $MasterST =  preg_replace("/ST/", "", $terms[$i]);
                $sWhere .= "`SequenceType`=".mysql_real_escape_string($MasterST)." AND ";
            } else if (preg_match("/^CC\d+$/", $terms[i]) && !$IsCC) {
                $IsCC = True; 
                $MasterCC = preg_replace("/CC/", "", $terms[$i]);
                $sWhere .= "`ClonalComplex`=".mysql_real_escape_string($MasterCC)." AND ";
            } else {
                if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" ) {
                    if(isset($_GET['bRegex_'.$i]) && $_GET['bRegex_'.$i] == "true") {
                        $sWhere .= "`MetaData` REGEXP '".mysql_real_escape_string($terms[$i])."' AND ";
                    } 
                    else {
                        $sWhere .= "`MetaData` LIKE '%".mysql_real_escape_string($terms[$i])."%' AND ";
                    }
                } 
            }
        }
        $sWhere = substr_replace( $sWhere, "", -4 );
        $sWhere .= ')';
    }
    
    
    /*
     * SQL queries
     * Get data to display
     */
    $sQuery = " $select
                $sWhere
                $sOrder
                $sLimit ";

    $rResult = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() );

    /* Data set length after filtering */
    $sQuery = " SELECT FOUND_ROWS() ";
    $rResultFilterTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() );
    $aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
    $iFilteredTotal = $aResultFilterTotal[0];
    
    /* Total data set length */
    $sQuery = " SELECT COUNT(`".$sIndexColumn."`)
                FROM $sTable";
    $rResultTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() );
    $aResultTotal = mysql_fetch_array($rResultTotal);
    $iTotal = $aResultTotal[0];
    
    
    /*
     * Output
     */
    $sEcho = (isset($_GET['sEcho'])) ? $_GET['sEcho'] : "";
    $output = array(
        "sEcho" => intval($sEcho),
        "iTotalRecords" => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData" => array()
    );
    error_log($IsST ." ". $MasterST);
    while ( $aRow = mysql_fetch_array( $rResult ) ) {
        $row = array();
        $published = 0;
        for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
            if ( $aColumns[$i] == "Class" ) {
                if ($aRow['MeanLength'] >= 75) {
                    if ($aRow['Coverage'] >= 45 && $aRow['Quality'] >= 30) {
                        $row[] = 'Gold';   
                    } 
                    else if ($aRow['Coverage'] >= 20 && $aRow['Quality'] >= 20) {
                        $row[] = 'Silver';
                    } else {
                        $row[] = 'Bronze';  
                    }
                } else {
                    $row[] = 'Bronze';
                }
            } 
            else if ( $aColumns[$i] == "SequenceType"  || $aColumns[$i] == "ClonalComplex" ) {
                if ($aRow[ $aColumns[$i] ] == 0 ) {
                    $row[] = "-";
                } 
                else {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
            }
            else if ( $aColumns[$i] == "Public" ) {
                $public = $aRow['Public'];
                if ($aRow['Public'] == 0) {
                    $row[] = 'No';
                } else {
                    $row[] = 'Yes';   
                }
            } 
            else if ( $aColumns[$i] == "UserName" ) {
                $owner = $aRow[ 'UserName' ];
                $row[] = $aRow[ 'UserName' ];
            } 
            else if ($aColumns[$i] == "Published") {
                if ($aRow[ $aColumns[$i] ] == 0 ) {
                    $row[] = "No";
                } 
                else {
                    $row[] = "Yes";
                    $published = 1;
                }
            } 
            else if ($aColumns[$i] == "SCCmec") {
                $row[] = "-";
            } 
            else if (isset($notPublished[$aColumns[$i]]) && $published == 0) {
                $row[] = "-";
            } 
            else if ( $aColumns[$i] != ' ' ) {
                /* General output */
                $row[] = $aRow[ $aColumns[$i] ];
            }
        }
        
        if (!empty($user)) {
            if ($aRow['UserName'] == $user) {
                array_push($output['aaData'], $row);
            }
        } else {
    
        //if ($public == 1 || $owner == $_SESSION['user']) {
            array_push($output['aaData'], $row);
        //}
        }

    }
    
    $output["iTotalDisplayRecords"]--;

    if ($output_mode == "csv") {
        header("Content-type: text/csv"); 
        header("Cache-Control: no-store, no-cache"); 
        header('Content-Disposition: attachment; filename="Staphopia.csv"');  // you can change the default file name here

    echo csv_encode( $output["aaData"], $aColumns );  // see function below
    } 
    else if ( $output_mode == "select" ) {
        // Return a elements to put in to the selection filter
        echo selection_encode( $output["aaData"], $_GET['column'] );
    } 
    else {
        // default is to output json
        echo json_encode( $output );
    }

    /*
    * csv_encode - encode data as csv, wrapping values in quotes
    */    
    function csv_encode($aaData, $aHeaders = NULL) {
        // output headers 
        $output = "";
        if ($aHeaders) $output .= '"' . implode('","', $aHeaders ) . '"'  . "\r\n";

        foreach ($aaData as $aRow) {
            $output .= '"' . implode('","', $aRow) . '"' . "\r\n";
        }

        return $output;
    }

    /*
    *  selection_encode - return only unique values of a specific column
    */
    function selection_encode( $aaData, $column ) {
        $unique_names = array();

        // Get the unique names.
        foreach ($aaData as $aRow) {
            $name = $aRow[ $column ];

            if (empty($unique_names[ $name ])) {
                $unique_names[ $name ] = $name;
            }
        }

        $output = "";
        sort($unique_names);
        foreach ($unique_names as $key => $value) {
            $output .= '<option value="'. $value .'">'. $value .'</option>';
        }

        return $output;
    }
?>
