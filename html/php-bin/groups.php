<?php session_start();

    $commands = array(
        'create' => True,
        'invite' => True,
        'leave' => True,
        'remove' => True,
        'permission' => True,
        'destroy' => True
    );
    
    if (!isset($_POST['Command'])) {
        // No direct access
        header('Location: /index.php');
    }
    else if (array_key_exists($_POST['Command'], $commands)) {
        require_once ('/var/www/staphopia/lib/HTML/Groups.php');
        $groups =  new Groups();
        
        require_once ('/var/www/staphopia/lib/Common/Validate.php');
        $v = new Validate();

        if($_POST['Command'] == 'create') {
            if (isset($_POST['GroupMessage']) && isset($_POST['GroupName'])) {
                $success = $groups->CreateGroup($_SESSION['UserID'], $_POST['GroupName'], $_POST['GroupMessage']);
                if ($success) {
                    $_POST['Command'] = 'invite';
                } 
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            }
        }
        elseif ($_POST['Command'] == 'invite') {
            
            if (isset($_POST['FirstName']) && isset($_POST['LastName']) && isset($_POST['Email'])) {
                require_once ('/var/www/staphopia/lib/Common/Validate.php');
                $v = new Validate();
                
                if (!$v->IsValidName( $_POST['FirstName'] )) {
                    $_SESSION['ErrorMessage'] .= '"'. $_POST['FirstName'] .'" is an invalid first name. <br />';
                }
                
                if (!$v->IsValidName( $_POST['LastName'] )) {
                    $_SESSION['ErrorMessage'] .= '"'. $_POST['LastName'] .'" is an invalid last name. <br />';
                }
                
                $Email = $v->SanitizeEmail( $_POST['Email'] ); 
                if (!$v->IsValidEmail( $Email )) {
                    $_SESSION['ErrorMessage'] .= '"'. $Email  .'" is an invalid email. <br />';
                }
                
                if (empty($_SESSION['ErrorMessage'])) {
                    $success = $groups->InviteUser( array(
                                    'GroupID' => $_POST['GroupID'],
                                    'Email' => $Email,
                                    'FirstName' => $_POST['FirstName'],
                                    'LastName' => $_POST['LastName']
                                ));
                }
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            } 
        }
        elseif ($_POST['Command'] == 'leave') {
            if (isset($_POST['UserID']) && isset($_POST['GroupID'])) {
                $OwnerID = $groups->GroupOwnerID( $_POST['GroupID'] );
                if ($OwnerID != $_POST['UserID']) {
                    $groups->RemoveUser( $_POST['GroupID'], $_POST['UserID'] );   
                } else {
                    $_SESSION['ErrorMessage'] = 'You cannot leave a group you created, you must delete the group.';
                }                 
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            } 
        }
        elseif ($_POST['Command'] == 'remove') {
            if (isset($_POST['UserID']) && isset($_POST['GroupID'])) {
                $Removee = $groups->UserGroup( $_POST['UserID'] , $_POST['GroupID'] );
                $Remover = $groups->UserGroup( $_SESSION['UserID'] , $_POST['GroupID'] );
                $OwnerID = $groups->GroupOwnerID( $_POST['GroupID'] );
                if ($OwnerID != $_POST['UserID'] && $Removee < $Remover ) {
                    $groups->RemoveUser( $_POST['GroupID'], $_POST['UserID'] );   
                } else {
                    $_SESSION['ErrorMessage'] = 'Only users of a lesser rank can be removed.';
                }                 
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            } 
        }
        elseif ($_POST['Command'] == 'permission') {
            if (!empty($_POST['UserID']) && !empty($_POST['GroupID']) && !empty($_POST['Permission'])) {
                $Changee = $groups->UserGroup( $_POST['UserID'] , $_POST['GroupID'] );
                $Changer = $groups->UserGroup( $_SESSION['UserID'] , $_POST['GroupID'] );
                $OwnerID = $groups->GroupOwnerID( $_POST['GroupID'] );
                if ($OwnerID != $_POST['UserID'] && $Changee < $Changer ) {
                    $groups->ChangeUserStatus( $_POST['Permission'], $_POST['UserID'] , $_POST['GroupID']); 
                } else {
                    $_SESSION['ErrorMessage'] = 'Only users of a lesser rank can be removed.';
                }                 
            } else {
                $_SESSION['ErrorMessage'] = 'Please complete all fields.';
            } 
        }
        elseif ($_POST['Command'] == 'destroy') {
            if (!empty($_POST['GroupID'])) {
                $OwnerID = $groups->GroupOwnerID( $_POST['GroupID'] );
                if ($OwnerID != $_POST['UserID']) {
                    $groups->DeleteGroup( $_POST['GroupID'] ); 
                } else {
                    $_SESSION['ErrorMessage'] = 'Only group owners can remove a group.';
                }                 
            }
            header('Location: /groups/');
            die();
        }

        if (isset($_POST['GroupID'])) {
            header('Location: /groups/'. $_POST['Command'] .'/'. $_POST['GroupID']);
        } else {
            header('Location: /groups/'. $_POST['Command'] .'/');
        }
        
    } else {
        // Not a valid command
        header('Location: /index.php');
    }
?>    
