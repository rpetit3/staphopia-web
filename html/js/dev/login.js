/**
 * login.js
 * 
 * All functions that should be associated with the login process (/login.php).
 * 
 * Author: Robert A Petit III
 * Date: 4/6/2013
 */
 
function Login() {
    this.init = 1;
}

Login.prototype.SubmitForm = function ( server ) {

    $( "#Submit" ).prop('disabled', true);
    $( "#login-modal p.response" ).html("We are attempting to log into your account now, please wait.").delay(1000);
    $('img.loading').show();
    $('a.reveal-link').trigger('click');
    
    setTimeout(function() {
        var q = 'Ajax=1';
        q += '&UserName=' + $('#UserName').val();
        q += '&Password=' + $('#Password').val();
        q += '&RememberMe=' + $('#RememberMe').prop('checked');
        
        $.ajax({
            type: "POST",
            url: "/php-bin/login.php",
            data: q,
            success: function(data) {
                $('img.loading').hide();
                $( "#Submit" ).prop('disabled', false);
                if (data == "1") {
                    window.location.replace(server);
                } 
                else {
                    $('img.loading').hide();
                    $( "#login-modal p.response" ).html("Log in attempt failed.<br /><br />Reason:<br/>" + data );
                }
            }
        });
    }, 2000);
};
