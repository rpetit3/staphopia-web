/**
 * register.js
 * 
 * All functions that should be associated with the registration process (/register.php).
 * 
 * Author: Robert A Petit III
 * Date: 4/3/2013
 */
 
function Register() {
    
    this.Validate = new Validate();
}

Register.prototype.SubmitForm = function () {
    
    $( "#register-modal p.response" ).html("We are registering your account now, please wait.");
    $('img.loading').show();
    $('a.reveal-link').trigger('click');
    
    setTimeout(function() {
        var q = 'Ajax=1';
        q += '&FirstName=' + $('#FirstName').val();
        q += '&LastName=' + $('#LastName').val();
        q += '&Email=' + $('#Email').val();
        q += '&UserName=' + $('#UserName').val();
        q += '&Password=' + $('#Password').val();
        q += '&AccessCode=' + $('#AccessCode').val();
        
        $.ajax({
            type: "POST",
            url: "/php-bin/register.php",
            data: q,
            success: function(data) {
                if (data == "1") {
                    window.location.replace("https://dev.staphopia.com/login/");
                }
                else {
                    $('img.loading').hide();
                    $( "#register-modal p.response" ).html("Whoops! An error has occurred during the registration process. If the problem persists, please report the issue.<br /><br /> Errors:<br/>" + data );
                }
            }
        });
    }, 2000);
};

Register.prototype.ValidateName = function( e ) {
    
    var Name = e.value;
    if (Name.length > 0) {
        if (this.Validate.Name(Name)) {
            e.setCustomValidity('');
        } else {
            e.setCustomValidity('Names can only contain alpha characters');
        }
    } else {
        e.setCustomValidity('Names can not be blank.');
    }
};

Register.prototype.ValidateUserName = function ( e ) {
    
    var UserName = e.value;
    if (UserName.length > 4) {
        if (this.Validate.RegEx( e.pattern, UserName )) {
            var exists = this.UserExists( UserName );
            exists.success( function (data) {
                if (data == "1") {
                    e.setCustomValidity('The username provided is already taken.');
                } else {
                    e.setCustomValidity('');
                }
            });
        } else {
            e.setCustomValidity('Usernames must start with a letter and may only contain letters, numbers and underscores.');
        }
    } else {
        e.setCustomValidity('Usernames must be at least 5 characters in length.');
    }
};

Register.prototype.UserExists = function ( value ) {
    return $.ajax({
                type: "POST",
                url: "/php-bin/register.php",
                data: 'CheckUser='+ value,
            });
};

Register.prototype.EmailExists = function ( e ) {
    
    return $.ajax({
                type: "POST",
                url: "/php-bin/register.php",
                data: 'CheckEmail='+ e.value,
                success: function(data) {
                    if (data == "1") {
                        e.setCustomValidity('An account is already registered under this email address.');
                    } else {
                        e.setCustomValidity('');
                    }
                }
            });
};

Register.prototype.ValidatePassword = function ( e ) {
    
    var p1 = e.value;
    if (p1.length > 7) {

        if (this.Validate.RegEx(e.pattern, p1)) {
            
            e.setCustomValidity('');     
        } else {
            e.setCustomValidity('Passwords must contain lower case, uppercase, and number/special characters.');
        }
    } else {
        e.setCustomValidity('Password must be at least 8 characters, containing lower case, uppercase, and number/special characters.');
    }
};

Register.prototype.ValidateSame = function ( parent ) {
  
    var e = $("#"+ parent +"2").get(0);
    if ($("#"+ parent).val() == $("#"+ parent +'2').val()) {
        e.setCustomValidity('');
    } else {
        e.setCustomValidity(parent +'s must match.');
    }       
    
};

Register.prototype.ValidateAccessCode = function ( e ) {
    
    if (this.Validate.RegEx(e.pattern, e.value)) {
        e.setCustomValidity('');
    } else {
        e.setCustomValidity('Please enter a valid access code.');
    }
};
