/**
 * submit.js
 * 
 * All functions that should be associated with submitting a new genome.
 * 
 * Author: Robert A Petit III
 * Date: 4/24/2013
 */
 
function Submit( id ) {
    this.BOXES = new Array('', 'project', 'organism', 'phenotype', 'submit');
    this.DRUGS = new Array('Vancomycin', 'Penicillin', 'Oxacillin', 'Clindamycin', 'Daptomycin',
                           'Levofloxacin', 'Linezolid', 'Rifampin', 'Tetracycline', 'Trimethoprim');
    this.CODES = [];
    this.index = 1;
    this.form = $('#SubmitForm');
    
    // Initialize a few things
    $(document).foundation().foundation('reveal', 'init');
    $(document).foundation().foundation('forms', 'init');
}

Submit.prototype.ShowBox = function(fieldset, i) {
    if (this.CheckValidity()) {
        this.FixDD(fieldset);
        this.FixArrow(i);
        this.index = i;
    }
};

Submit.prototype.SwitchBox = function(i) {
    if (this.CheckValidity()) {
        i = i + this.index;
        this.FixDD(this.BOXES[i]);
        this.FixArrow(i);
        this.index = i;
    }
};

Submit.prototype.CheckValidity = function() {
    if (!$('#SubmitForm')[0].checkValidity()) {
        $('#submit').click();
        return false;
    } else {
        return true;   
    }
};

Submit.prototype.FixDD = function(element) {
    $('.sub-nav dd.active').each( function() {
        $(this).removeClass('active');
    });
    $('.sub-nav dd.'+element+'-box').addClass('active');
    
    $('form fieldset.selected').each( function() {
        $(this).removeClass('selected');
    });
    $('form fieldset.'+element).addClass('selected');
};

Submit.prototype.FixArrow = function(i) {
    if (i == 1) {
        $('.nav-row .left').removeClass('active');
        $('.nav-row .right').addClass('active');
    }
    else if (i == 4) {
        this.CheckReqs();
        $('.nav-row .right').removeClass('active');
        $('.nav-row .left').addClass('active');
    }
    else {
        $('.nav-row .left').addClass('active');
        $('.nav-row .right').addClass('active');
    }
};

Submit.prototype.CheckReqs = function() {
    var FIELDS = new Array( 'ContactName', 'ContactEmail', 'SequencingCenter');
    for (var i = 0; i < FIELDS.length; i++) {
        if ($('input[name='+ FIELDS[i] +']').val() !== '') {
            $('fieldset.submit .'+ FIELDS[i]).removeClass('foundicon-error');
            $('fieldset.submit .'+ FIELDS[i]).addClass('foundicon-checkmark');
        }
        else {
            $('fieldset.submit .'+ FIELDS[i]).removeClass('foundicon-checkmark');
            $('fieldset.submit .'+ FIELDS[i]).addClass('foundicon-error');
        }
    }
};


Submit.prototype.Validate = function (formData, jqForm, options) {
    /* Disable the Submit button and Validate the input */
    $('.error .alert-box span').html('');
    $('.error .alert-box').css('visibility', 'hidden');
    $( "#SubmitForm input:submit" ).attr("disabled", true);
    /* Validate here */
};

Submit.prototype.UpdateStatus = function (event, position, total, percentComplete) {
    $('.progress .meter').css('width', percentComplete+'%');
};

Submit.prototype.Success = function (responseText, statusText, xhr, $form) {
    if (xhr.responseText.length == 32) {
        window.location.replace("/status/" + xhr.responseText);
    }
    else {
        $('.error .alert-box span').html(xhr.responseText);
        $('.error .alert-box').css('visibility', 'visible');
        $('.progress .meter').css('width', '0%');
        $( "#SubmitForm input:submit" ).attr("disabled", false);
    }  
};

Submit.prototype.Sliders = function() {
    for (var i=0; i < this.DRUGS.length; i++){
        var drug = this.DRUGS[i];
        this.Slider(drug);
    }
};

Submit.prototype.Slider = function( drug ) {
    $("#"+ drug +"-slider").slider({
        value: -10.3,
        min: -11.4,
        max: 32.0,
        step: 0.1,
        range: "min",
        slide: function( event, ui ) {
            Submit.prototype.UpdateSlider( drug, ui.value );
        }, 
        change: function( event, ui ) {
            Submit.prototype.UpdateSlider( drug, ui.value );
        }
    });
    
    $( "#"+ drug +"-value" ).val( 'Untested' );
    $( "#"+ drug +"-suffix" ).html( 'Untested' );
};

Submit.prototype.UpdateSlider = function( drug, value ) {
    if (value >= 0) {
        $( "#"+ drug +"-value" ).val( value );
        $( "#"+ drug +"-suffix" ).html( value + ' mg/ul MIC' );
    } else {
        if (value < -9.1) {
            $( "#"+ drug +"-value" ).val( 'Untested' );
            $( "#"+ drug +"-suffix" ).html( 'Untested' );
        }
        else if (value <= -6.7){
            $( "#"+ drug +"-value" ).val( 'Supseptible' );
            $( "#"+ drug +"-suffix" ).html( 'Supseptible' );
        }
        else if (value <= -4.3){
            $( "#"+ drug +"-value" ).val( 'Intermediate' );
            $( "#"+ drug +"-suffix" ).html( 'Intermediate' );
        }
        else{
            $( "#"+ drug +"-value" ).val( 'Resistant' );
            $( "#"+ drug +"-suffix" ).html( 'Resistant' );
        }
    }
};

Submit.prototype.SetPhenotype = function ( value ) {
    
    if (value === "Untested") {
        return -10.3;  
    } else if (value === "Supseptible") {
        return -7.9; 
    } else if (value === "Intermediate") {
        return -5.4; 
    } else if (value === "Resistant") {
        return -3; 
    } else {
        return value;   
    }
        
};

Submit.prototype.AutoComplete = function() {
    CODES = this.CODES;
    
    $( "#IsolationCity" ).autocomplete({
        source: function( request, response ) {
            $("#IsolationRegion").val( '' );
            $("#Longitude").val( '' );
            $("#Latitude").val( '' );
            $.ajax({
                beforeSend: function () {
                    $('#isolation_city img').addClass( "loading" );
                },
                complete: function () {
                    $('#isolation_city img').removeClass( "loading" ); 
                },
                url: "https://dev.staphopia.com/php-bin/geonames.php",
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    username: "staphopia",
                    style: "medium",
                    maxRows: 30,
                    country: CODES[$('#IsolationCountry').val()], 
                    name_startsWith: request.term
                },
                success: function( data ) {
                    response( $.map( data.geonames, function( item ) {
                        return {
                            region: (item.adminName1 ? item.adminName1 : ""),
                            lng: item.lng,
                            lat: item.lat,
                            label: item.name + (item.adminName1 ? ", " + item.adminName1 : ""),
                            value: item.name
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            $("#IsolationCity").val( ui.item.value );
            $("#IsolationRegion").val( ui.item.region );
            $("#Longitude").val( ui.item.lng );
            $("#Latitude").val( ui.item.lat );
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
};

Submit.prototype.HostName = function() {
    
    $( "#HostName" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                beforeSend: function () {
                    $('#host_name img').addClass( "loading" );
                },
                complete: function () {
                    $('#host_name img').removeClass( "loading" ); 
                },
                url: "https://dev.staphopia.com/php-bin/hostnames.php",
                dataType: "json",
                data: {
                    name: request.term
                },
                success: function( data ) {
                    response( $.map( data, function( item ) {
                        return {
                            name: item.Name,
                            taxid: item.TaxonID,
                            label: item.Name
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            $("#HostName").val( ui.item.name );
            $("#HostTaxonID").val( ui.item.taxid );
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
};