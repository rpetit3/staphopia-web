/**
 * validate.js
 * 
 * All functions that should be associated with sorts of validation (value is set, passwords, etc).
 * 
 * Author: Robert A Petit III
 * Date: 4/3/2013
 */
 
function Validate( id ) {
    this.id = id;
}

Validate.prototype.Name = function ( name ) {
    return (/^[a-zA-ZàáâäãåąćęèéêëìíîïłńòóôöõøùúûüÿýżźñçčšžÀÁÂÄÃÅĄĆĘÈÉÊËÌÍÎÏŁŃÒÓÔÖÕØÙÚÛÜŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'\-]{1,64}$/).test( name );
};

Validate.prototype.RegEx = function ( pattern, string ) {
    var re = new RegExp(pattern);
    return re.test( string );  
};



