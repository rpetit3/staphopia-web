/*----------------------------------------------------------------------
 * index.php
 *--------------------------------------------------------------------*/
function joinMailList() {
    $( "#email-modal p" ).html('');
    var email = $('input#email').val();
    if (email == "") {
        $( "#email-modal p" ).html('Please enter an email address and try again.');
    }
    else {
        $.ajax({
            type: "POST",
            url: "/php-bin/joinMailList.php",
            data: 'q='+email,
            success: function(data) {
                if (data == "1") {
                    $( "#email-modal p" ).html('You are already apart of the Staphopia mailing list.');
                }
                else if (data == "2") {
                    $( "#email-modal p" ).html('You have successfully been joined to Staphopia\'s mailing list. Thank you very much for your interest!.');
                }
                else if (data == "4") {
                    $( "#email-modal p" ).html('The email entered does not appear to be vaild, please check it and try again.');
                }
                else {
                    $( "#email-modal p" ).html('Please try submitting your request to join again.');
                }
            }
        });
    }
    $('a.reveal-link').trigger('click');
}

/* ---------------------------------------------------------------------
 * contact.php 
 * -------------------------------------------------------------------*/
function submitContact(){
    $( "form a.button" ).attr("disabled", true);
    $( "#contact-modal p" ).html('');
    var email = $('input#email').val();
    if (email == "") {
        $( "#contact-modal p" ).html('Please enter an email address and try again.');
    }
    else {
        $.ajax({
            type: "POST",
            url: "/php-bin/submitContact.php",
            data: $('#contactForm').formSerialize(),
            success: function(data) {
                if (data == "1") {
                    $( "#contact-modal p" ).html('Thank you for your response.  Some one will get in contact with you soon.');
                }
                else if (data == "4") {
                    $( "#contact-modal p" ).html('The email entered does not appear to be vaild, please check it and try again.');
                }
                else if (data == "6") {
                    $( "#contact-modal p" ).html('Make sure all the fields are completed, please try again.');
                }
                else {
                    $( "#contact-modal p" ).html('Please try submitting your request to join again.');
                }
            }
        });
    }
    setTimeout(function () {
       $('a.reveal-link').trigger('click');
    }, 500);
    $( "form a.button" ).attr("disabled", false);
}

/* ---------------------------------------------------------------------
 * genomes.php 
 * -------------------------------------------------------------------*/
 
function InitGenomeTable() {
        var oTable = $('#genomesTable').dataTable({
                "bServerSide": true,
                "bProcessing": true,
                "sScrollY": "700px",
                "sScrollX": "100%",
        "sScrollXInner": "250%",
                "sAjaxSource": "/php-bin/genomes.php",
        "bAutoWidth": false,
        "sDom": '<"H"CTf>tr<"F"ip>',
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayLength" : 50,
        "oColVis": { 
                        "iOverlayFade": 500, 
                        "sSize": "css" 
                },
                "fnInitComplete": function (){
            this.fnAdjustColumnSizing(true);
            this.fnDraw();
                },
                "fnDrawCallback": function (){
                        var rows = this.fnGetNodes();
            for(var i=0;i<rows.length;i++) {
                // Get HTML of 3rd column (for example)
                var seqname = $(rows[i]).find("td:eq(0)").html();
                var title = "View " + seqname + " details in another tab/window."; 
                $(rows[i]).prop('title', title);
            }
                }
        });
    
    // Make Rows Selectable
    $("#genomesTable tbody").click(function(event) {
        $(oTable.fnSettings().aoData).each(function (){
            $(this.nTr).removeClass('row_selected');
        });
        $(event.target.parentNode).addClass('row_selected');
        var aPos = oTable.fnGetPosition(event.target.parentNode);
        var id = oTable.fnGetData(aPos)[3];
        window.open('/summary/'+id+'/', '_blank');
        window.focus();
    });
    
}

/* ---------------------------------------------------------------------
 * summary.php 
 * -------------------------------------------------------------------*/
function InitSeqTables() {
    // MLST Related
    $( '#lociTable' ).dataTable({
                "bProcessing": false,
                "bJQueryUI": true,
                "aoColumnDefs": [{ 
                   "bSortable": false, 
                   "aTargets": [ 0,1,2,3,4 ] 
                }],
        "sDom": '<"H">t<"F">'
    });

    $( '#mlstRelated' ).dataTable({
        "sScrollY": "400",
        "bProcessing": true,
        "bJQueryUI": true,
        "bPaginate": true,
        "iDisplayLength" : 30,
        "sDom": '<"H"r>t<"F"p>',
        "fnDrawCallback": function (){
                        var rows = this.fnGetNodes();
            for(var i=0;i<rows.length;i++) {
                // Get HTML of 3rd column (for example)
                var seqname = $(rows[i]).find("td:eq(0)").html();
                var title = "View " + seqname + " details in another tab/window."; 
                $(rows[i]).prop('title', title);
            }
                }
    });
    
    // Make Rows Selectable
    $("#mlstRelated tbody").click(function(event) {
        oTable = $( '#mlstRelated' ).dataTable();
        $(oTable.fnSettings().aoData).each(function (){
            $(this.nTr).removeClass('row_selected');
        });
        $(event.target.parentNode).addClass('row_selected');
        var aPos = oTable.fnGetPosition(event.target.parentNode);
        var id = oTable.fnGetData(aPos)[0];
        window.open('/summary/'+id);
    });
    
    // Significant Protein Hit Tables
    $( '.proteinSTable' ).dataTable({
        "sScrollY": "300",
        "bProcessing": true,
        "bJQueryUI": true,
        "bPaginate": false,
        "sDom": '<"H"r>t<"F"p>'
    });   
    
    // All Protein Hit Tables 
    $( '.proteinTable' ).dataTable({
        "sScrollY": "500",
        "bProcessing": true,
        "bJQueryUI": true,
        "bPaginate": false,
        "sDom": '<"H"r>t<"F"p>'
    });

}

var TITLE = new Array('Summary Outline', 'Collected Meta-Data', 'Phenotype Information', 'Sequence Statistics', 'Assembly Statistics', 'MLST Outline',
                      'Related Samples', 'SCCmec Outline', 'Cassette Coverage', 'Protein Hits', 'Primer Hits', 'Resistance Outline', 'Virulence Outline');

function subCategory(category, title) {
    $('.category dd.active').each( function() {
        $(this).removeClass('active');
    });
    $('.category dd.'+category).addClass('active');

    $('.sub-category dl.selected').each( function() {
        $(this).removeClass('selected');
    });
    $('.sub-category dl.'+category).addClass('selected');
    $('.sub-category dl.selected dd.active').removeClass('active');
    $('.sub-category dl.selected dd.outline').addClass('active');
    
    $('.results .selected').each( function() {
        $(this).removeClass('selected');
    });
    $('.results .'+category+'-outline').addClass('selected');
    $('div.title-row .title h4').html(TITLE[title]);
    checkForTables(category+'-outline');
}

function showResults(sub, result, title) {
    $('.sub-category dl.selected dd.active').each( function() {
        $(this).removeClass('active');
    });
    $('.sub-category dl.selected dd.'+sub).addClass('active');
    
    $('.results .selected').each( function() {
        $(this).removeClass('selected');
    });
    $('.results .'+result+'-'+sub).addClass('selected');
    $('div.title-row .title h4').html(TITLE[title]);
    checkForTables(result+'-'+sub);
}


function showCoverage(type) {
    $('img.sccmec').each( function() {
       $(this).css('display', 'none') 
    });
    $('img.sccmec[name='+ type +']').css('display','block');
    $('ul.sccmecCov li.active').removeClass('active');
    $('ul.sccmecCov li.'+type).addClass('active');
}

// Correct column widths
function checkForTables(element) {
    switch(element) {
          case "mlst-outline":
            FixColumns($( '#lociTable' ).dataTable());
            break;
          case "mlst-related":
            FixColumns($( '#mlstRelated' ).dataTable());
            break;
          case "sccmec-outline":
            FixColumns($( '#mecSProtein' ).dataTable());
            FixColumns($( '#mecSPrimers' ).dataTable());
            break;
          case "sccmec-protein":
            FixColumns($( '#mecProtein' ).dataTable());
            break;
          case "sccmec-primer":
            FixColumns($( '#mecPrimers' ).dataTable());
            break;
          case "resistance-outline":
            FixColumns($( '#resSTable' ).dataTable());
            break;
          case "resistance-protein":
            FixColumns($( '#resTable' ).dataTable());
            break;
          case "virulence-outline":
            FixColumns($( '#virSTable' ).dataTable());
            break;
          case "virulence-protein":
            FixColumns($( '#virTable' ).dataTable());
            break;
          default:
            break;
    }
}

function FixColumns(oTable) {
    if ( oTable.length > 0 ) {
        oTable.fnAdjustColumnSizing();
    }
}

/* ---------------------------------------------------------------------
 * submit.php 
 * -------------------------------------------------------------------*/
var BOXES = new Array('', 'project', 'organism', 'phenotype', 'submit');
var index = 1;
function showBox(fieldset, i) {
    fixDD(fieldset);
    fixArrow(i);
    index = i;
}
 
function switchBox(i) {
    i = i + index;
    fixDD(BOXES[i]);
    fixArrow(i);
    index = i;
}

function fixDD(element){
    $('.sub-nav dd.active').each( function() {
        $(this).removeClass('active');
    });
    $('.sub-nav dd.'+element+'-box').addClass('active');
    
    $('form fieldset.selected').each( function() {
        $(this).removeClass('selected');
    });
    $('form fieldset.'+element).addClass('selected');
}

function fixArrow(i) {
    if (i == 1) {
        $('.nav-row .left').removeClass('active');
        $('.nav-row .right').addClass('active');
    }
    else if (i == 4) {
        checkReqs();
        $('.nav-row .right').removeClass('active');
        $('.nav-row .left').addClass('active');
    }
    else {
        $('.nav-row .left').addClass('active');
        $('.nav-row .right').addClass('active');
    }
}

function checkReqs() {
    var FIELDS = new Array('project_status', 'contact_name', 'contact_email', 'seq_status', 'seq_center');
    for (var i = 0; i < FIELDS.length; i++) {
        if ($('input[name='+ FIELDS[i] +']').val() != "") {
            $('fieldset.submit .'+ FIELDS[i]).removeClass('foundicon-error');
            $('fieldset.submit .'+ FIELDS[i]).addClass('foundicon-checkmark');
        }
        else {
            $('fieldset.submit .'+ FIELDS[i]).removeClass('foundicon-checkmark');
            $('fieldset.submit .'+ FIELDS[i]).addClass('foundicon-error');
        }
    }
}
