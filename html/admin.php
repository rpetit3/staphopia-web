<?php session_start();
    
    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }
    
    $Allow = False;
    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    } 
    else if (isset($_SESSION['IsLoggedIn'])) {
        if ($_SESSION['IsAdmin']) {
            $commands = array(
                'lock' => True,
                'unlock' => True,
                'delete' => True,
                'grant' => True,
                'revoke' => True,
                'invite' => True
                
            );
            if (empty($_GET['q'])) {
                $Allow = True;
                $command = 'summary';
            }
            else if (array_key_exists($_GET['q'], $commands)) {
                // Require password to use commands
                if ($_SESSION['IsValidated']) {
                    $Allow = True;
                    $command = $_GET['q'];
                } else {
                    $_SESSION['Redirect'] = 'Location: /admin/' . $_GET['q'];
                    header('Location: /login/validate');
                    die();
                }
            }     
        }
    } 

    if (!$Allow) {
        // Shouldn't be here!
        header('Location: /index.php');
    } else {
        require_once ('/var/www/staphopia/lib/Common/Admin.php');
        $admin =  new Admin();
        
        if (isset($_SESSION['ErrorMessage'])) {
            if (empty($_SESSION['ErrorMessage'])) {
                unset($_SESSION['ErrorMessage']);
            }
        }
    }

?>

<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <link rel="stylesheet" href="/css/general_foundicons.css">
    <!-- End Defaults -->
    <style>
        .side-nav li a.active {
            font-weight: bold;
        }
        div.callout {
            padding: 0.5em !important;
        }
        form fieldset {
            display: block !important;
        }
        img.loading {
            display: none;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <? include ('/var/www/staphopia/template/Admin/'. $command .'.php'); ?>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script>
            $(document).foundation();
        </script>
        <!-- End Javascript -->
        
    </body>
</html>
