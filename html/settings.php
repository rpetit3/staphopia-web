<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }
    
    $Allow = False;
    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    } 
    else if (isset($_SESSION['IsLoggedIn'])) {

        $commands = array(
            'email' => True,
            'password' => True,
            'sessions' => True,
            'saved' => True           
        );
        if (empty($_GET['q'])) {
            $Allow = True;
            $command = 'summary';
        }
        else if (array_key_exists($_GET['q'], $commands)) {
            // Require password to use commands
            if ($_SESSION['IsValidated']) {
                $Allow = True;
                $command = $_GET['q'];
            } else {
                $_SESSION['Redirect'] = 'Location: /settings/' . $_GET['q'];
                header('Location: /login/validate');
                die();
            }
        }     
    } 

    if (!$Allow) {
        // Shouldn't be here!
        header('Location: /index.php');
    } else {
        require_once ('/var/www/staphopia/lib/HTML/Settings.php');
        $settings =  new Settings();
        
        if (isset($_SESSION['ErrorMessage'])) {
            if (empty($_SESSION['ErrorMessage'])) {
                unset($_SESSION['ErrorMessage']);
            }
        }
    }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <link rel="stylesheet" href="/css/general_foundicons.css">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" type="text/css" rel="Stylesheet" />
    <!-- End Defaults -->
    <style>
        .side-nav li a.active {
            font-weight: bold;
        }
        div.callout {
            padding: 0.5em !important;
        }
        form fieldset {
            display: block !important;
        }
        img.loading {
            display: none;
            margin-left: auto;
            margin-right: auto;
        }
        
    <? if ($command == 'saved') : ?>
    
        .ui-widget-content {
            width: 325px !important;
        }
        .ui-autocomplete {
          padding: 0;
          list-style: none;
          background-color: #fff;
          width: 218px;
          border: 1px solid #B0BECA;
          max-height: 350px;
          overflow-y: scroll;
        }
        .ui-autocomplete .ui-menu-item a {
          border-top: 1px solid #B0BECA;
          display: block;
          padding: 4px 6px;
          color: #353D44;
          cursor: pointer;
        }
        .ui-autocomplete .ui-menu-item:first-child a {
          border-top: none;
        }
        .ui-autocomplete .ui-menu-item a.ui-state-hover {
          background-color: #D5E5F4;
          color: #161A1C;
        }
        #HideDropDown div.dropdown {
            display: none;
        }
        
        #IsolationCountry {
            display: block !important;
        }
        
        span.GeoNames {
            font-size: 0.7em;
            float: right;
            bottom: 0px;
            line-height: 1.51em;
        }
        
        .loading {
            display: inline !important;
        }
        
        .ui-helper-hidden-accessible {
            display: none !important;    
        }
        
    <? endif; ?>
        
    </style>
    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <? include ('/var/www/staphopia/template/Settings/'. $command .'.php'); ?>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script>
            $(document).foundation();
            
            
            
        </script>
        
        <? if ($command == 'saved') : ?>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <script src="/js/dev/submit.js"></script>
        <script src="/js/jquery.form.js"></script>
        <script src="/js/jquery.select-to-autocomplete.js"></script>
        <script>
            var s;
            $(document).ready(function () {
                s = new Submit();
                $('#country-selector').selectToAutocomplete();
                $("#country-selector > option").each(function() {
                    s.CODES[$(this).val()] = $(this).data('alternativeSpellings').substring(0,2);
                });
                s.AutoComplete();
                s.HostName()

                $('#IsolationCountry').bind("change paste keyup", function() {
                    if ($('#IsolationCountry').val() != '-' && $('#IsolationCountry').val() != '') {
                        $('#IsolationCity').prop('disabled', false);
                        $('#IsolationRegion').prop('disabled', false);
                    } else {
                        $('#IsolationCity').prop('disabled', true);
                        $('#IsolationRegion').prop('disabled', true);
                    }
                });
                
                <? if ($Tags['IsolationCountry'] != '') : ?>
                
                $('#IsolationCountry').val('<? echo $Tags['IsolationCountry']; ?>');
                $('#IsolationCity').prop('disabled', false);
                $('#IsolationRegion').prop('disabled', false);
                        
                <? endif; ?>
            });              
        </script>
            
        <? endif; ?>
        <!-- End Javascript -->
        
    </body>
</html>
