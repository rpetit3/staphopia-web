<?php session_start();
        
    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    } 
    else if ($_GET['q'] == 'password') {
        if (preg_match("/^[a-z0-9]{32}/", $_GET['f'])) {
            $command = 'reset';
        } else {
            $command = 'request';
        }
    } else {
        header('Location: /index.php');
    }

?>

<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <link rel="stylesheet" href="/css/general_foundicons.css">
    <!-- End Defaults -->
    <style>
        .side-nav li a.active {
            font-weight: bold;
        }
        div.callout {
            padding: 0.5em !important;
        }
        form fieldset {
            display: block !important;
        }
        img.loading {
            display: none;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <? include ('/var/www/staphopia/template/Reset/password-'. $command.'.php'); ?>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script>
            $(document).foundation();
        </script>
        <!-- End Javascript -->
        
    </body>
</html>