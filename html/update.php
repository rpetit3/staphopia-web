<? session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }

    if (empty($_GET['q']) || empty ($_GET['f']) || $_GET['q'] != 'email') {
        header('Location: /index.php');
    } 
    else if ($_SESSION['IsValidated']) {
        require_once ('/var/www/staphopia/lib/HTML/Settings.php');
        $settings =  new Settings();
        $settings->VerifyEmail( $_GET['f'] );
        header('Location: /settings/email');
    } else {
        $_SESSION['Redirect'] = 'Location: /update/email/' . $_GET['f'];
        header('Location: /login/validate');
        die();
    }    

?>
