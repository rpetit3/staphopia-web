<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (!isset($_SESSION['IsLoggedIn']) && !isset($_SESSION['IsLive'])) {
        header('Location: /index.php');
    }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <!-- End Defaults -->
    <style>
        form fieldset {
            display: block !important;
        }
    </style>
    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <div class="row">
            <div class="small-12 small-centered columns">
                <h3 class="center">Contact</h5>
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="small-6 small-centered columns">
                <p class="center" >Use the form to get in contact with the Staphopia team.</p>
            </div>
        </div>
        <div class="row">
            <div class="small-9 small-centered columns">
                <form id="contactForm" class="custom" onSubmit="submitContact(); return false;">
                    <fieldset>
                        <div class="row">
                            <div class="small-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <label>Name </label>
                                            <input name="contact_name" placeholder="Marvin" type="text" 
                                                   value="" required />
                                        </li>
                                        <li>
                                            <label>Email </label>
                                            <input name="contact_email" placeholder="my@email.com" type="email" 
                                                   value="" required />
                                        </li>
                                        <li>
                                            <label>Reason </label>
                                            <select name="contact_reason">
                                                <option selected value="General">General</option>
                                                <option value="Question">Question</option>
                                                <option value="Suggestion">Suggestion</option>
                                                <option value="Bug">Bug</option>
                                            </select>
                                        </li>
                                    </ul>
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <hr />
                           </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <div class="panel no-border">
                                    <label>Comments: </label>
                                    <textarea required style="height: 10em;" name="comments"
                                              placeholder="Ask questions, make comments/suggestions, etc...">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="small-3 small-offset-9 columns">
                                <div class="panel no-border">
                                    <input type="submit" class="button prefix" value="Contact">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <a href="#" data-reveal-id="contact-modal" class="reveal-link" style="display:none !important;"></a>
        <div id="contact-modal" class="reveal-modal small">
            <p class="response"></p>
            <a class="close-reveal-modal">&#215;</a>
        </div>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript.php') ?>
        <script src="/js/jquery.form.js"></script>
        <script>
            $(document).foundation();
            $(document).ready(function () {       
                $(document).foundation().foundation('reveal', 'init');
                $(document).foundation().foundation('forms', 'init');
            });
        </script>
        <!-- End Javascript -->
        
    </body>
</html>
