<?php session_start();

    // No direct access
    if (!isset($_GET['q'])) {
        header('Location: /index.php');
    } 
    else if (!preg_match("/^[a-z0-9]{32}/", $_GET['q'])) {
        header('Location: /index.php');
    } else {
        // Unlock account
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        
        if ($login->UnlockAccount( $_GET['q'] )) {
            $_SESSION['ErrorMessage']  = 'Your account has been unlocked, please take this time to';
            $_SESSION['ErrorMessage'] .= ' consider changing your password.';
        } else {
            $_SESSION['ErrorMessage'] = 'Invalid unlock code given.';
        }

        header('Location: /login/');
    }
    
?>
