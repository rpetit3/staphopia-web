<?php session_start();

    // No direct access
    if (!isset($_GET['q'])) {
        header('Location: /index.php');
    } 
    else if (!preg_match("/^[a-z0-9]{32}/", $_GET['q'])) {
        header('Location: /index.php');
    }
    
    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (isset($_SESSION['IsLoggedIn'])) {
        header('Location: /index.php');
    }

?>

<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <!-- End Defaults -->
    <style>
        div.callout {
            padding: 0.5em !important;
        }
        form fieldset {
            display: block !important;
        }
        img.loading {
            display: none;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <title>Staphopia - Registration</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Register</h4>
                <hr/>
            </div>
        </div>
        
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-9 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>        
        <div class="row">
            <div class="large-9 large-centered columns">
                <form id="RegisterForm" class="custom" method="post" action="/php-bin/register.php" 
                      onsubmit="return false;">
                    <fieldset>
                        <div class="row">
                            <div class="large-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <label>First Name </label>
                                            <input id="FirstName" name="FirstName" placeholder="Arthur" type="text" 
                                                   value="" title="Names may not contain numbers."
                                                   pattern="^[\D]{1,64}$" oninput="r.ValidateName(this);" required />
                                        </li>
                                        <li>
                                            <label>Last Name </label>
                                            <input id="LastName" name="LastName" placeholder="Dent" type="text" value="" 
                                                   title="Names may not contain numbers."
                                                   pattern="^[\D]{1,64}$" oninput="r.ValidateName(this);" required />
                                        </li>
                                        <li>
                                            <label>Email </label>
                                            <input id="Email" name="Email" placeholder="my@email.com" type="email" 
                                                   value="" autocomplete="off" required
                                                   oninput="r.EmailExists(this); r.ValidateSame('Email');"  />
                                        </li>
                                        <li>
                                            <label>Verify Email </label>
                                            <input id="Email2" name="Email2" placeholder="my@email.com" type="email" 
                                                   value="" autocomplete="off" oninput="r.ValidateSame('Email');" 
                                                   required />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="large-6 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <label>User Name </label>
                                            <input id="UserName" name="UserName" placeholder="adent" type="text" value="" 
                                                   title="May only contain letters, numbers and underscores."
                                                   pattern="^[a-zA-Z][a-zA-Z0-9_.]{4,14}$" required
                                                   oninput="r.ValidateUserName(this);" />
                                        </li>
                                        <li>
                                            <label>Password </label>
                                            <input id="Password" name="Password" type="password" value="" 
                                                   title="Must contain letters, numbers and special characters."
                                                   pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" 
                                                   oninput="r.ValidatePassword(this);r.ValidateSame('Password');" required />
                                        </li>
                                        <li>
                                            <label>Verify Password </label>
                                            <input id="Password2" name="Password2" type="password" value="" 
                                                   pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" 
                                                   oninput="r.ValidateSame('Password');" required />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="panel no-border">
                                    <ul>
                                        <li>
                                            <label>Access Code </label>
                                            <input id="AccessCode" name="AccessCode" type="text" value="<? echo $_GET['q']; ?>" 
                                                   pattern="^[a-z0-9]{32}$" oninput="r.ValidateAccessCode(this);" 
                                                   autocomplete="off" required />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-3 large-offset-9 columns">
                                <div class="panel no-border">
                                    <input type="submit" class="button prefix" value="Register">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        
        <a href="#" data-reveal-id="register-modal" class="reveal-link" style="display:none !important;"></a>
        <div id="register-modal" class="reveal-modal small">
            <p class="response center"></p>
            <img class="loading center" src="/img/loading.gif">
            <a class="close-reveal-modal">&#215;</a>
        </div>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script src="/js/dev/register.js"></script>
        <script src="/js/dev/validate.js"></script>
        <script>
            var r = new Register();
            $(document).foundation();
            $(document).ready(function () {       
                $(document).foundation().foundation('reveal', 'init');
                $(document).foundation().foundation('forms', 'init');
                $( "#RegisterForm" ).submit(function() { r.SubmitForm(); });
            });
            
        </script>
        <!-- End Javascript -->
        
    </body>
</html>
