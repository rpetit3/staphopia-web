<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (isset($_SESSION['IsLoggedIn']) && $_GET['q'] != "validate") {
        header('Location: /index.php');
    }
    
    require_once ('/var/www/staphopia/config/server.php');
    $server = new ServerConfig();
    
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <link rel="stylesheet" href="/css/accessibility_foundicons.css">
    <!-- End Defaults -->
    <style>
        div.callout {
            padding: 0.5em !important;
        }
        form fieldset {
            display: block !important;
        }
        img.loading {
            display: none;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <title>Staphopia - Login</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <div class="row">
            <div class="large-12 large-centered columns">
                <p></p>
                <h4 class="center">Log In</h4>
                <hr/>
            </div>
        </div>
        <? if (isset($_SESSION['ErrorMessage'])) : ?>
        
        <div class="row">
            <div class="large-6 large-centered columns">
                <div class="panel callout">
                    <p class="center">
                    
                    <? 
                        echo $_SESSION['ErrorMessage']; 
                        unset($_SESSION['ErrorMessage']);
                    ?>
                    
                    </p>
                </div>
            </div>
        </div>
        
        <? endif; ?>
        <div class="row">
            <div class="large-6 large-centered columns">
                <form id="LoginForm" class="custom" method="post" action="/php-bin/login.php" onsubmit="return false;">
                    <fieldset>
                        <div class="row">
                            <div class="large-12 columns">
                                <label>Email or username:</label>
                            </div>
                        </div>
                        <div class="row collapse">
                            <div class="small-3 large-2 columns">
                                <span class="prefix"><i class="foundicon-adult"></i></span>
                            </div>
                            <div class="small-9 large-10 columns">
                                <input tabindex="1" id="UserName" name="UserName" placeholder="my@email.com or myuser" 
                                       type="text" value="" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <label>Password:</label>
                            </div>
                            <div class="small-8 columns">
                                <label class="right">
                                    <a tabindex="5" href="/reset/password/request">Forgot your password?</a>
                                </label>
                            </div>
                        </div>
                        <div class="row collapse">
                            <div class="small-3 large-2 columns">
                                <span class="prefix"><i class="foundicon-key"></i></span>
                            </div>
                            <div class="small-9 large-10 columns">
                                <input tabindex="2 "id="Password" name="Password" placeholder="********" 
                                       type="password" value="" required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-8 columns">
                                <? if ($_GET['q'] != "validate") : ?>
                                <label for="RememberMe" title="Stay logged in for one week.">
                                    <input type="checkbox" id="RememberMe" name="RememberMe" style="display: none;">
                                    <span tabindex="3" class="custom checkbox"></span> 
                                    <em>Keep me logged in</em>
                                </label>
                                <? endif; ?>
                            </div>
                            <div class="large-3 columns">
                                <input id="Submit" tabindex="4" type="submit" class="button prefix" value="Login">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <!-- End Main Content-->
        
        
        <a href="#" data-reveal-id="login-modal" class="reveal-link" style="display:none !important;"></a>
        <div id="login-modal" class="reveal-modal small">
            <p class="response center"></p>
            <img class="loading" src="/img/loading.gif">
            <a class="close-reveal-modal">&#215;</a>
        </div>

        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script src="/js/dev/login.js"></script>
        <script>
            var l = new Login();
            $(document).foundation();
            $(document).ready(function () {       
                $(document).foundation().foundation('reveal', 'init');
                $(document).foundation().foundation('forms', 'init');
                $( "#LoginForm" ).submit(function() { l.SubmitForm( '<? echo $server->address; ?>' ); });
                $( '#UserName' ).focus();
            });
        </script>
        <!-- End Javascript -->
        
    </body>
</html>
