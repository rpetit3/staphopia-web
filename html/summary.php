<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (!isset($_SESSION['IsLoggedIn']) && !isset($_SESSION['IsLive']) || empty($_GET['q']) ) {
        // Must be logged in to access
        header('Location: /index.php');
    }
    
    require ('/var/www/staphopia/lib/HTML/Summary.php');
    $summary = new Summary($_GET['q']);

    if (!$summary->exists) {
        // seqname doesn't exist
        header('Location: /index.php');
    }
    
    if ($_GET['f'] == 'edit' || $_GET['f'] == 'history') {
        if (!$summary->fullView) {
            // Must have full view to access these
            header('Location: /index.php');
        } else {
            $_SESSION['SampleTag'] = $_GET['q'];   
        }
    } else {
        // Others
        // Assembly
        $assembly = new Assembly( $summary->db, $summary->sample_id );
        
        // MLST
        $mlst = new MLST( $summary->db, $summary->sample_id );
        
        // SCCmec
        $sccmec = new SCCmec( $summary->db, $summary->sample_id, $summary->accessions );
        
        // Resistance
        $resistance = new Resistance( $summary->db, $summary->sample_id, $summary->accessions, $summary->categories );
        
        // Virulence
        $virulence = new Virulence( $summary->db, $summary->sample_id, $summary->accessions, $summary->categories );
    }

?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <!-- Defaults -->
        <? include ('/var/www/staphopia/template/header.php') ?>
        <link rel="stylesheet" href="/css/general_foundicons.css">
        <!-- End Defaults -->
        <link rel="stylesheet" href="/css/ColVis.css">
        <link type="text/css" rel="Stylesheet" 
              href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" 
              href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.3/css/jquery.dataTables_themeroller.css">
        
        <? if ($_GET['f'] == 'edit') : ?>
            <? include ('/var/www/staphopia/template/Summary/edit-css.php') ?>
        <? elseif ($_GET['f'] == 'history') : ?>
            <? include ('/var/www/staphopia/template/Summary/history-css.php') ?>
        <? else : ?>
            <? include ('/var/www/staphopia/template/Summary/summary-css.php') ?>
        <? endif; ?>
        
        <title>Staphopia - <? echo $summary->seqname; ?></title>
    </head>
    <body>
    


        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav-private.php'); ?>
        <!-- End Top Bar -->

        <? if ($_GET['f'] == 'edit') : ?>
            <? include ('/var/www/staphopia/template/Summary/edit.php') ?>
        <? elseif ($_GET['f'] == 'history') : ?>
            <? include ('/var/www/staphopia/template/Summary/history.php') ?>
        <? else : ?>
            <? include ('/var/www/staphopia/template/Summary/summary.php') ?>
        <? endif; ?>

        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->

        <? if ($_GET['f'] == 'edit') : ?>
            <? include ('/var/www/staphopia/template/Summary/edit-js.php') ?>
        <? elseif ($_GET['f'] == 'history') : ?>
            <? include ('/var/www/staphopia/template/Summary/history-js.php') ?>
        <? else : ?>
            <? include ('/var/www/staphopia/template/Summary/summary-js.php') ?>
        <? endif; ?>
        
        <!-- End Javascript -->

    </body>
</html>
