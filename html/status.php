<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    }
    else if (empty($_GET['q'])) {
        header('Location: /index.php');
    }
    else if (strlen($_GET['q']) != 32) {
        header('Location: /index.php');
    }
    else if (!isset($_SESSION['IsLoggedIn'])) {
        // Must be logged in to access
        $_SESSION['Redirect'] = 'Location: /status/' . $_GET['q'];
        header('Location: /login/validate');
        die();
    } else {
        require_once ('/var/www/staphopia/lib/HTML/Status.php');
        $status = new Status($_GET['q']);
        if (!$status->exists) {
            // md5sum doesn't exist
            header('Location: /index.php');
        }
    }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <? if (!$status->complete) : ?>
    <!-- Refresh the page every 60 seconds -->
    <meta http-equiv="refresh" content="60" />
    <? endif; ?>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <link rel="stylesheet" href="/css/general_foundicons.css">

    <!-- End Defaults -->
    <style>
        #status i {
            padding-right: 0.5em;    
        }
    </style>
    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <? include ('/var/www/staphopia/template/status.php'); ?>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script>
            $(document).foundation();
        </script>
        <!-- End Javascript -->
        
    </body>
</html>