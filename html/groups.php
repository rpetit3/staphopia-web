<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }
    
    $Allow = False;
    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    } 
    else if (isset($_SESSION['IsLoggedIn'])) {

        $CodeID = False;
        if (!empty($_GET['q'])) {
            if (strlen($_GET['q']) == 32) {
                $CodeID = $_GET['q'];
                $_GET['q'] = 'join';
            }
        }
    
        $commands = array(
            'create' => True,
            'destroy' => True,
            'invite' => True,
            'join' => True,
            'leave' => True,
            'permission' => True,
            'remove' => True,
            'summary' => True,
            'view' => True
        );
        if (empty($_GET['q'])) {
            $Allow = True;
            $command = 'summary';
        }
        else if (array_key_exists($_GET['q'], $commands)) {
            // Require password to use commands
            if ($_SESSION['IsValidated']) {
                $Allow = True;
                $command = $_GET['q'];
            } else {
                if (!$CodeID) {
                    $_SESSION['Redirect'] = 'Location: /groups/'. $_GET['q'] .'/';
                } else {
                    $_SESSION['Redirect'] = 'Location: /groups/'. $CodeID .'/';
                }
                header('Location: /login/validate');
                die();
            }
        }     
    } 

    if (!$Allow) {
        // Shouldn't be here!
        header('Location: /index.php');
    } else {
        $permission = array(
            0 => 'Member',
            1 => 'Curator', 
            2 => 'Administrator',
            3 => 'Owner'
        );
        require_once ('/var/www/staphopia/lib/HTML/Groups.php');
        $groups = new Groups();
        
        if ($command == 'destroy') {
            if (empty($_GET['f'])) {
                header('Location: /groups/');
                die();
            } else {
                if ($_SESSION['UserID'] != $groups->GroupOwnerID( $_GET['f'] )){
                    $_SESSION['ErrorMessage'] = 'You must be the owner of a group in order to delete a group.';
                    header('Location: /groups/');
                    die();
                }
            }
        }        
        elseif ($command == 'view' || $command == 'remove' || $command == 'permission') {
            if (empty($_GET['f'])) {
                header('Location: /groups/');
                die();
            }
            
            $groups->GroupInfo( $_GET['f'], $_SESSION['UserID'] );
            $rank = $groups->UserGroup( $_SESSION['UserID'] , $_GET['f'] );
            
            if ($command == 'remove' || $command == 'permission') {
                if ($groups->CanInvite) {
                    $groups->TestRemovables( $rank, $_GET['f'] );
                } else {
                    header('Location: /groups/');
                    die();
                }
            }
            
            if (!$groups->HasGroups) {
                header('Location: /groups/');
                die();
            }
            
        } else {
            // Join the group before getting a list of groups.
            if ($command == 'join') {
                 $groups->JoinGroup( $_SESSION['UserID'], $CodeID );
                 $command = 'summary';
            }
            
            // Get list of groups.
            $groups->UserGroups( $_SESSION['UserID'] );
        }
        
        if (isset($_SESSION['ErrorMessage'])) {
            if (empty($_SESSION['ErrorMessage'])) {
                unset($_SESSION['ErrorMessage']);
            }
        }
    }
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <link rel="stylesheet" href="/css/general_foundicons.css">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css" type="text/css" rel="Stylesheet" />
    <!-- End Defaults -->
    <style>
        div.callout {
            padding: 0.5em !important;
        }
        form fieldset {
            display: block !important;
        }
        
        .right {
            text-align: right;
        }
        .italic {
            font-style: italic;
        }
    </style>
    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->
        <? include ('/var/www/staphopia/template/Groups/'. $command .'.php'); ?>
        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script>
            $(document).foundation();
        </script>
        <!-- End Javascript -->
        
    </body>