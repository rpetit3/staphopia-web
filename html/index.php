<?php session_start();

    // If cookie is set use, set session.
    if (isset($_COOKIE['SessionToken']) && !isset($_SESSION['IsLoggedIn'])) {
        require_once ('/var/www/staphopia/lib/HTML/Login.php');
        $login = new Login( False );
        $login->TestSessionToken( $_COOKIE['SessionToken'] );
    }

    if (isset($_SESSION['IsLockedOut'])) {
        header('Location: /locked/');
    } 
    else if (isset($_SESSION['Redirect'])) {
        // User validated password,take them back where they came from.
        $location = $_SESSION['Redirect'];
        unset($_SESSION['Redirect']);
        header($location);
    }
?>

<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <!-- Defaults -->
    <? include ('/var/www/staphopia/template/header.php') ?>
    <!-- End Defaults -->

    <title>Staphopia - Development</title>
</head>
    <body>

        <!-- Top Bar -->
        <? include ('/var/www/staphopia/template/top-nav.php'); ?>
        <!-- End Top Bar -->


        <!-- Main Page Content-->

            <? if (isset($_SESSION['IsLive'])) : ?>
                <? include ('/var/www/staphopia/template/index-live.php') ?>
            <? else: ?>
                <? include ('/var/www/staphopia/template/index-dev.php') ?>
            <? endif; ?>

        <!-- End Main Content-->


        <!-- Footer -->
        <? include ('/var/www/staphopia/template/footer.php') ?>
        <!-- End Footer -->


        <!-- Javascipt -->
        <? include ('/var/www/staphopia/template/javascript-min.php') ?>
        <script>
            $(document).foundation();
        </script>
        <!-- End Javascript -->
        
    </body>
</html>
